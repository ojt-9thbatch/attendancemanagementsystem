﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class ChangePassword : MenuBar
    {
        //declare sql connection
        SqlConnection conn;
        public void connection()
        {
            //connection method to connect with DB including server name and database name
            conn = new SqlConnection(@"data source=TMPC-051; Database=AttendanceDB; integrated security = true");
        }

        //variable declaration
        private string encryptionKey = "abcdefghijkmnopq";
        string currentPwd = "";

        public ChangePassword()
        {
            InitializeComponent();
            btnChangePassword.BackColor = Color.FromArgb(151, 242, 0);
        }
            
        /// <summary>
        /// this method is for update password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangePass_Click(object sender, EventArgs e)
        {
            //variable declarations
            string currentPassword = txtCurrentPass.Text.Trim();
            string newPassword = txtNewPass.Text.Trim();
            string confirmPassword = txtConfirmPass.Text.Trim();

            string currentEncrypt = Encrypt(currentPassword);

            //check current password is valid or not
            if (!string.IsNullOrWhiteSpace(currentPassword))
            {
                if (currentEncrypt != currentPwd)
                {
                    MessageBox.Show("Current Password is not valid!");
                    txtCurrentPass.Clear();
                    txtCurrentPass.Focus();
                    return;
                }
            }
            else
            {
                // Check if current password field empty
                if (string.IsNullOrWhiteSpace(currentPassword))
                {
                    //show alert message
                    MessageBox.Show("Please fill current password.");
                    txtCurrentPass.Focus();
                    return;
                }
            }

            // Check if new password field empty
            if (string.IsNullOrWhiteSpace(newPassword))
            {
                //show alert message
                MessageBox.Show("Please fill new password.");
                txtNewPass.Focus();
                return;
            }

            // Check if confirm password field empty
            if (string.IsNullOrWhiteSpace(confirmPassword))
            {
                //show alert message
                MessageBox.Show("Please fill confirm password.");
                txtConfirmPass.Focus();
                return;
            }

                //check newpassword and confirm password match or not
                if (newPassword != confirmPassword)
                {
                    MessageBox.Show("New Password and Confirm Password do not match!");
                    txtNewPass.Clear();
                    txtConfirmPass.Clear();
                    txtNewPass.Focus();
                    return;
                }

                string encryptedPassword = Encrypt(newPassword);
                //query for update password
                string updatePassword = "update t_emp_basic_info set emp_password = @newPassword where emp_id = @id";
                SqlCommand cmdUpdate = new SqlCommand(updatePassword, conn);
                cmdUpdate.Parameters.AddWithValue("@newPassword", encryptedPassword);
                cmdUpdate.Parameters.AddWithValue("@id", Login.employeeId);
                int rowsAffected = cmdUpdate.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    MessageBox.Show("Password changed successfully!");
                }
                else
                {
                    MessageBox.Show("Failed to update password!");
                }
            
            ClearTextBoxes();
        }

        //clear textboxes
        private void ClearTextBoxes()
        {
            txtCurrentPass.Clear();
            txtNewPass.Clear();
            txtConfirmPass.Clear();
        }

        // This function takes a string `text` as input parameters.
        // It encrypts the input `text` using AES (Advanced Encryption Standard) algorithm with the provided `key`.
        private string Encrypt(string text)
        {
            // Creating an instance of AES algorithm
            using (Aes aesAlg = Aes.Create())
            {
                // Setting the key for AES encryption
                aesAlg.Key = Encoding.UTF8.GetBytes(encryptionKey);
                aesAlg.IV = new byte[16]; // Initialization Vector (IV) should be unique, but for simplicity, we use a static IV
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Using a MemoryStream to store the encrypted data
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // Using CryptoStream to perform the encryption
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        // Using StreamWriter to write the plaintext `text` to the CryptoStream
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }
                    }
                    // Converting the encrypted data to Base64 string and returning it
                    return Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
        }

        /// <summary>
        /// form loading => get emp_password(current password) from db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangePassword_Load(object sender, EventArgs e)
        {
            try
            {
                connection();
                conn.Open();
                string selectCurrentPass = "select emp_password from t_emp_basic_info where emp_id = @id";
                SqlCommand CurrentPassCmd = new SqlCommand(selectCurrentPass, conn);
                CurrentPassCmd.Parameters.AddWithValue("@id", Login.employeeId);
                currentPwd = CurrentPassCmd.ExecuteScalar() as string;
                
            }
            catch (Exception ex) 
            {
                MessageBox.Show("An error occurred. Details: " + ex.Message);
            }
            
        }
    }
}
