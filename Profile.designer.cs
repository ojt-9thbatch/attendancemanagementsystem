﻿namespace Attendance_Management_System
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.containerPanel = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.gbWorkExperience = new System.Windows.Forms.GroupBox();
            this.workExperiencePanel = new System.Windows.Forms.Panel();
            this.lblWorkExperience = new System.Windows.Forms.Label();
            this.lblWorkPosition = new System.Windows.Forms.Label();
            this.lblNameLocation = new System.Windows.Forms.Label();
            this.lblWorkExperienceDate = new System.Windows.Forms.Label();
            this.addExperiencePanel = new System.Windows.Forms.Panel();
            this.txtWorkPosition = new System.Windows.Forms.TextBox();
            this.txtCompanyLocation = new System.Windows.Forms.TextBox();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.btnAddExperience = new System.Windows.Forms.Button();
            this.dtpExperienceDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblExperienceDateTo = new System.Windows.Forms.Label();
            this.dtpExperienceDateTo = new System.Windows.Forms.DateTimePicker();
            this.lblExperienceDateFrom = new System.Windows.Forms.Label();
            this.gbEducation = new System.Windows.Forms.GroupBox();
            this.certificatePanel = new System.Windows.Forms.Panel();
            this.lblCertificate = new System.Windows.Forms.Label();
            this.lblCertificateDate = new System.Windows.Forms.Label();
            this.lblCertificateName = new System.Windows.Forms.Label();
            this.jpCertificatePanel = new System.Windows.Forms.Panel();
            this.lblJPLevelName = new System.Windows.Forms.Label();
            this.lblJPLangLevel = new System.Windows.Forms.Label();
            this.lblJPBandScore = new System.Windows.Forms.Label();
            this.lblJPCertificateDate = new System.Windows.Forms.Label();
            this.rdoSetDefault1 = new System.Windows.Forms.RadioButton();
            this.addCertificatePanel = new System.Windows.Forms.Panel();
            this.txtCertificateName = new System.Windows.Forms.TextBox();
            this.lblCertificateDateFrom = new System.Windows.Forms.Label();
            this.btnAddCertificate = new System.Windows.Forms.Button();
            this.dtpCertificateDateFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpCertificateDateTo = new System.Windows.Forms.DateTimePicker();
            this.lblCertificateDateTo = new System.Windows.Forms.Label();
            this.addJPSkillPanel = new System.Windows.Forms.Panel();
            this.btnAddJPCertificate = new System.Windows.Forms.Button();
            this.txtJPLevelName = new System.Windows.Forms.TextBox();
            this.lblAddBandScore = new System.Windows.Forms.Label();
            this.txtJPBandScore = new System.Windows.Forms.TextBox();
            this.lblJPYear = new System.Windows.Forms.Label();
            this.dtpJPYear = new System.Windows.Forms.DateTimePicker();
            this.txtEducation = new System.Windows.Forms.TextBox();
            this.lblEducation = new System.Windows.Forms.Label();
            this.gbContactInfo = new System.Windows.Forms.GroupBox();
            this.rtxtTempAddr = new System.Windows.Forms.RichTextBox();
            this.rtxtPermAddr = new System.Windows.Forms.RichTextBox();
            this.txtEmgContactRelation = new System.Windows.Forms.TextBox();
            this.lblEmgContactRelation = new System.Windows.Forms.Label();
            this.txtEmgContactNo = new System.Windows.Forms.TextBox();
            this.lblContactNo = new System.Windows.Forms.Label();
            this.txtEmgContactName = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmgContact = new System.Windows.Forms.Label();
            this.lblTempAddr = new System.Windows.Forms.Label();
            this.lblPermAddr = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.gbPersonalInfo = new System.Windows.Forms.GroupBox();
            this.imgBox = new System.Windows.Forms.PictureBox();
            this.lblGenderName = new System.Windows.Forms.Label();
            this.lblDisplayDOB = new System.Windows.Forms.Label();
            this.lblRollName = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.rdoFemale = new System.Windows.Forms.RadioButton();
            this.rdoMale = new System.Windows.Forms.RadioButton();
            this.lblNRC = new System.Windows.Forms.Label();
            this.txtNRC = new System.Windows.Forms.TextBox();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.comboRole = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtEmployeeName = new System.Windows.Forms.TextBox();
            this.lblEmployeeID = new System.Windows.Forms.Label();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.StaffPanel.SuspendLayout();
            this.containerPanel.SuspendLayout();
            this.gbWorkExperience.SuspendLayout();
            this.workExperiencePanel.SuspendLayout();
            this.addExperiencePanel.SuspendLayout();
            this.gbEducation.SuspendLayout();
            this.certificatePanel.SuspendLayout();
            this.jpCertificatePanel.SuspendLayout();
            this.addCertificatePanel.SuspendLayout();
            this.addJPSkillPanel.SuspendLayout();
            this.gbContactInfo.SuspendLayout();
            this.gbPersonalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBox)).BeginInit();
            this.SuspendLayout();
            // 
            // containerPanel
            // 
            this.containerPanel.BackColor = System.Drawing.SystemColors.Window;
            this.containerPanel.CausesValidation = false;
            this.containerPanel.Controls.Add(this.btnEdit);
            this.containerPanel.Controls.Add(this.gbWorkExperience);
            this.containerPanel.Controls.Add(this.gbEducation);
            this.containerPanel.Controls.Add(this.gbContactInfo);
            this.containerPanel.Controls.Add(this.gbPersonalInfo);
            this.containerPanel.Location = new System.Drawing.Point(4, 179);
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size(1556, 1943);
            this.containerPanel.TabIndex = 0;
            // 
            // btnEdit
            // 
            this.btnEdit.AutoSize = true;
            this.btnEdit.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.SystemColors.Window;
            this.btnEdit.Location = new System.Drawing.Point(720, 1842);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(170, 46);
            this.btnEdit.TabIndex = 32;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // gbWorkExperience
            // 
            this.gbWorkExperience.Controls.Add(this.workExperiencePanel);
            this.gbWorkExperience.Controls.Add(this.addExperiencePanel);
            this.gbWorkExperience.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbWorkExperience.Location = new System.Drawing.Point(88, 1468);
            this.gbWorkExperience.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.gbWorkExperience.Name = "gbWorkExperience";
            this.gbWorkExperience.Size = new System.Drawing.Size(1382, 323);
            this.gbWorkExperience.TabIndex = 31;
            this.gbWorkExperience.TabStop = false;
            this.gbWorkExperience.Text = "Work Experience";
            // 
            // workExperiencePanel
            // 
            this.workExperiencePanel.Controls.Add(this.lblWorkExperience);
            this.workExperiencePanel.Controls.Add(this.lblWorkPosition);
            this.workExperiencePanel.Controls.Add(this.lblNameLocation);
            this.workExperiencePanel.Controls.Add(this.lblWorkExperienceDate);
            this.workExperiencePanel.Location = new System.Drawing.Point(40, 58);
            this.workExperiencePanel.Name = "workExperiencePanel";
            this.workExperiencePanel.Size = new System.Drawing.Size(933, 48);
            this.workExperiencePanel.TabIndex = 32;
            // 
            // lblWorkExperience
            // 
            this.lblWorkExperience.AutoSize = true;
            this.lblWorkExperience.Location = new System.Drawing.Point(3, 11);
            this.lblWorkExperience.Name = "lblWorkExperience";
            this.lblWorkExperience.Size = new System.Drawing.Size(166, 27);
            this.lblWorkExperience.TabIndex = 0;
            this.lblWorkExperience.Text = "Work Experience";
            // 
            // lblWorkPosition
            // 
            this.lblWorkPosition.AutoSize = true;
            this.lblWorkPosition.Location = new System.Drawing.Point(272, 11);
            this.lblWorkPosition.Name = "lblWorkPosition";
            this.lblWorkPosition.Size = new System.Drawing.Size(127, 27);
            this.lblWorkPosition.TabIndex = 0;
            this.lblWorkPosition.Text = "Programmer";
            // 
            // lblNameLocation
            // 
            this.lblNameLocation.AutoSize = true;
            this.lblNameLocation.Location = new System.Drawing.Point(675, 11);
            this.lblNameLocation.Margin = new System.Windows.Forms.Padding(40, 0, 3, 0);
            this.lblNameLocation.Name = "lblNameLocation";
            this.lblNameLocation.Size = new System.Drawing.Size(220, 27);
            this.lblNameLocation.TabIndex = 0;
            this.lblNameLocation.Text = "Tosco Myanmar Co.Ltd";
            // 
            // lblWorkExperienceDate
            // 
            this.lblWorkExperienceDate.AutoSize = true;
            this.lblWorkExperienceDate.Location = new System.Drawing.Point(459, 11);
            this.lblWorkExperienceDate.Margin = new System.Windows.Forms.Padding(40, 0, 3, 0);
            this.lblWorkExperienceDate.Name = "lblWorkExperienceDate";
            this.lblWorkExperienceDate.Size = new System.Drawing.Size(151, 27);
            this.lblWorkExperienceDate.TabIndex = 0;
            this.lblWorkExperienceDate.Text = "2024/1~2024/2";
            // 
            // addExperiencePanel
            // 
            this.addExperiencePanel.Controls.Add(this.txtWorkPosition);
            this.addExperiencePanel.Controls.Add(this.txtCompanyLocation);
            this.addExperiencePanel.Controls.Add(this.txtCompanyName);
            this.addExperiencePanel.Controls.Add(this.btnAddExperience);
            this.addExperiencePanel.Controls.Add(this.dtpExperienceDateFrom);
            this.addExperiencePanel.Controls.Add(this.lblExperienceDateTo);
            this.addExperiencePanel.Controls.Add(this.dtpExperienceDateTo);
            this.addExperiencePanel.Controls.Add(this.lblExperienceDateFrom);
            this.addExperiencePanel.Location = new System.Drawing.Point(286, 145);
            this.addExperiencePanel.Name = "addExperiencePanel";
            this.addExperiencePanel.Size = new System.Drawing.Size(1065, 154);
            this.addExperiencePanel.TabIndex = 31;
            // 
            // txtWorkPosition
            // 
            this.txtWorkPosition.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWorkPosition.Location = new System.Drawing.Point(25, 19);
            this.txtWorkPosition.Name = "txtWorkPosition";
            this.txtWorkPosition.Size = new System.Drawing.Size(266, 34);
            this.txtWorkPosition.TabIndex = 26;
            this.txtWorkPosition.Text = "Experience";
            // 
            // txtCompanyLocation
            // 
            this.txtCompanyLocation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyLocation.Location = new System.Drawing.Point(479, 100);
            this.txtCompanyLocation.Name = "txtCompanyLocation";
            this.txtCompanyLocation.Size = new System.Drawing.Size(266, 34);
            this.txtCompanyLocation.TabIndex = 30;
            this.txtCompanyLocation.Text = "Location";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyName.Location = new System.Drawing.Point(25, 100);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(266, 34);
            this.txtCompanyName.TabIndex = 29;
            this.txtCompanyName.Text = "Company Name";
            // 
            // btnAddExperience
            // 
            this.btnAddExperience.AutoSize = true;
            this.btnAddExperience.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddExperience.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddExperience.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddExperience.ForeColor = System.Drawing.SystemColors.Window;
            this.btnAddExperience.Location = new System.Drawing.Point(880, 12);
            this.btnAddExperience.Name = "btnAddExperience";
            this.btnAddExperience.Size = new System.Drawing.Size(179, 46);
            this.btnAddExperience.TabIndex = 31;
            this.btnAddExperience.Text = "Add Experience";
            this.btnAddExperience.UseVisualStyleBackColor = false;
            this.btnAddExperience.Click += new System.EventHandler(this.btnAddExperience_Click);
            // 
            // dtpExperienceDateFrom
            // 
            this.dtpExperienceDateFrom.CustomFormat = "yyyy/MM";
            this.dtpExperienceDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExperienceDateFrom.Location = new System.Drawing.Point(480, 19);
            this.dtpExperienceDateFrom.Name = "dtpExperienceDateFrom";
            this.dtpExperienceDateFrom.Size = new System.Drawing.Size(150, 34);
            this.dtpExperienceDateFrom.TabIndex = 27;
            // 
            // lblExperienceDateTo
            // 
            this.lblExperienceDateTo.AutoSize = true;
            this.lblExperienceDateTo.Location = new System.Drawing.Point(665, 22);
            this.lblExperienceDateTo.Name = "lblExperienceDateTo";
            this.lblExperienceDateTo.Size = new System.Drawing.Size(33, 27);
            this.lblExperienceDateTo.TabIndex = 28;
            this.lblExperienceDateTo.Text = "To";
            // 
            // dtpExperienceDateTo
            // 
            this.dtpExperienceDateTo.CustomFormat = "yyyy/MM";
            this.dtpExperienceDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExperienceDateTo.Location = new System.Drawing.Point(705, 19);
            this.dtpExperienceDateTo.Name = "dtpExperienceDateTo";
            this.dtpExperienceDateTo.Size = new System.Drawing.Size(150, 34);
            this.dtpExperienceDateTo.TabIndex = 28;
            // 
            // lblExperienceDateFrom
            // 
            this.lblExperienceDateFrom.AutoSize = true;
            this.lblExperienceDateFrom.Location = new System.Drawing.Point(413, 25);
            this.lblExperienceDateFrom.Name = "lblExperienceDateFrom";
            this.lblExperienceDateFrom.Size = new System.Drawing.Size(60, 27);
            this.lblExperienceDateFrom.TabIndex = 27;
            this.lblExperienceDateFrom.Text = "From";
            // 
            // gbEducation
            // 
            this.gbEducation.Controls.Add(this.certificatePanel);
            this.gbEducation.Controls.Add(this.jpCertificatePanel);
            this.gbEducation.Controls.Add(this.addCertificatePanel);
            this.gbEducation.Controls.Add(this.addJPSkillPanel);
            this.gbEducation.Controls.Add(this.txtEducation);
            this.gbEducation.Controls.Add(this.lblEducation);
            this.gbEducation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEducation.Location = new System.Drawing.Point(88, 998);
            this.gbEducation.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.gbEducation.Name = "gbEducation";
            this.gbEducation.Size = new System.Drawing.Size(1382, 447);
            this.gbEducation.TabIndex = 30;
            this.gbEducation.TabStop = false;
            this.gbEducation.Text = "Education Background";
            // 
            // certificatePanel
            // 
            this.certificatePanel.AutoSize = true;
            this.certificatePanel.Controls.Add(this.lblCertificate);
            this.certificatePanel.Controls.Add(this.lblCertificateDate);
            this.certificatePanel.Controls.Add(this.lblCertificateName);
            this.certificatePanel.Location = new System.Drawing.Point(40, 271);
            this.certificatePanel.Name = "certificatePanel";
            this.certificatePanel.Size = new System.Drawing.Size(779, 48);
            this.certificatePanel.TabIndex = 30;
            // 
            // lblCertificate
            // 
            this.lblCertificate.AutoSize = true;
            this.lblCertificate.Location = new System.Drawing.Point(3, 10);
            this.lblCertificate.Name = "lblCertificate";
            this.lblCertificate.Size = new System.Drawing.Size(123, 27);
            this.lblCertificate.TabIndex = 0;
            this.lblCertificate.Text = "Certification";
            // 
            // lblCertificateDate
            // 
            this.lblCertificateDate.AutoSize = true;
            this.lblCertificateDate.Location = new System.Drawing.Point(409, 10);
            this.lblCertificateDate.Margin = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblCertificateDate.Name = "lblCertificateDate";
            this.lblCertificateDate.Size = new System.Drawing.Size(173, 27);
            this.lblCertificateDate.TabIndex = 0;
            this.lblCertificateDate.Text = "2023/01~2024/01";
            // 
            // lblCertificateName
            // 
            this.lblCertificateName.AutoSize = true;
            this.lblCertificateName.Location = new System.Drawing.Point(272, 10);
            this.lblCertificateName.Margin = new System.Windows.Forms.Padding(0, 0, 40, 0);
            this.lblCertificateName.Name = "lblCertificateName";
            this.lblCertificateName.Size = new System.Drawing.Size(51, 27);
            this.lblCertificateName.TabIndex = 0;
            this.lblCertificateName.Text = "Java";
            // 
            // jpCertificatePanel
            // 
            this.jpCertificatePanel.AutoSize = true;
            this.jpCertificatePanel.Controls.Add(this.lblJPLevelName);
            this.jpCertificatePanel.Controls.Add(this.lblJPLangLevel);
            this.jpCertificatePanel.Controls.Add(this.lblJPBandScore);
            this.jpCertificatePanel.Controls.Add(this.lblJPCertificateDate);
            this.jpCertificatePanel.Controls.Add(this.rdoSetDefault1);
            this.jpCertificatePanel.Location = new System.Drawing.Point(41, 115);
            this.jpCertificatePanel.Name = "jpCertificatePanel";
            this.jpCertificatePanel.Size = new System.Drawing.Size(854, 48);
            this.jpCertificatePanel.TabIndex = 29;
            // 
            // lblJPLevelName
            // 
            this.lblJPLevelName.AutoSize = true;
            this.lblJPLevelName.Location = new System.Drawing.Point(271, 10);
            this.lblJPLevelName.Margin = new System.Windows.Forms.Padding(0);
            this.lblJPLevelName.Name = "lblJPLevelName";
            this.lblJPLevelName.Size = new System.Drawing.Size(80, 27);
            this.lblJPLevelName.TabIndex = 0;
            this.lblJPLevelName.Text = "JLPT N5";
            this.lblJPLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJPLangLevel
            // 
            this.lblJPLangLevel.AutoSize = true;
            this.lblJPLangLevel.Location = new System.Drawing.Point(4, 10);
            this.lblJPLangLevel.Name = "lblJPLangLevel";
            this.lblJPLangLevel.Size = new System.Drawing.Size(238, 27);
            this.lblJPLangLevel.TabIndex = 0;
            this.lblJPLangLevel.Text = "Japanese Language Level";
            // 
            // lblJPBandScore
            // 
            this.lblJPBandScore.AutoSize = true;
            this.lblJPBandScore.Location = new System.Drawing.Point(408, 10);
            this.lblJPBandScore.Margin = new System.Windows.Forms.Padding(0);
            this.lblJPBandScore.Name = "lblJPBandScore";
            this.lblJPBandScore.Size = new System.Drawing.Size(45, 27);
            this.lblJPBandScore.TabIndex = 0;
            this.lblJPBandScore.Text = "100";
            this.lblJPBandScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJPCertificateDate
            // 
            this.lblJPCertificateDate.AutoSize = true;
            this.lblJPCertificateDate.Location = new System.Drawing.Point(522, 10);
            this.lblJPCertificateDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblJPCertificateDate.Name = "lblJPCertificateDate";
            this.lblJPCertificateDate.Size = new System.Drawing.Size(87, 27);
            this.lblJPCertificateDate.TabIndex = 0;
            this.lblJPCertificateDate.Text = "2020/10";
            this.lblJPCertificateDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rdoSetDefault1
            // 
            this.rdoSetDefault1.AutoSize = true;
            this.rdoSetDefault1.Location = new System.Drawing.Point(679, 10);
            this.rdoSetDefault1.Margin = new System.Windows.Forms.Padding(0);
            this.rdoSetDefault1.Name = "rdoSetDefault1";
            this.rdoSetDefault1.Size = new System.Drawing.Size(158, 31);
            this.rdoSetDefault1.TabIndex = 0;
            this.rdoSetDefault1.TabStop = true;
            this.rdoSetDefault1.Text = "Set as Default";
            this.rdoSetDefault1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdoSetDefault1.UseVisualStyleBackColor = true;
            // 
            // addCertificatePanel
            // 
            this.addCertificatePanel.Controls.Add(this.txtCertificateName);
            this.addCertificatePanel.Controls.Add(this.lblCertificateDateFrom);
            this.addCertificatePanel.Controls.Add(this.btnAddCertificate);
            this.addCertificatePanel.Controls.Add(this.dtpCertificateDateFrom);
            this.addCertificatePanel.Controls.Add(this.dtpCertificateDateTo);
            this.addCertificatePanel.Controls.Add(this.lblCertificateDateTo);
            this.addCertificatePanel.Location = new System.Drawing.Point(296, 342);
            this.addCertificatePanel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.addCertificatePanel.Name = "addCertificatePanel";
            this.addCertificatePanel.Size = new System.Drawing.Size(1055, 70);
            this.addCertificatePanel.TabIndex = 28;
            // 
            // txtCertificateName
            // 
            this.txtCertificateName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCertificateName.Location = new System.Drawing.Point(16, 21);
            this.txtCertificateName.Name = "txtCertificateName";
            this.txtCertificateName.Size = new System.Drawing.Size(266, 34);
            this.txtCertificateName.TabIndex = 22;
            this.txtCertificateName.Text = "Certificate";
            // 
            // lblCertificateDateFrom
            // 
            this.lblCertificateDateFrom.AutoSize = true;
            this.lblCertificateDateFrom.Location = new System.Drawing.Point(391, 24);
            this.lblCertificateDateFrom.Name = "lblCertificateDateFrom";
            this.lblCertificateDateFrom.Size = new System.Drawing.Size(60, 27);
            this.lblCertificateDateFrom.TabIndex = 24;
            this.lblCertificateDateFrom.Text = "From";
            // 
            // btnAddCertificate
            // 
            this.btnAddCertificate.AutoSize = true;
            this.btnAddCertificate.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddCertificate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCertificate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCertificate.ForeColor = System.Drawing.SystemColors.Window;
            this.btnAddCertificate.Location = new System.Drawing.Point(870, 14);
            this.btnAddCertificate.Name = "btnAddCertificate";
            this.btnAddCertificate.Size = new System.Drawing.Size(179, 46);
            this.btnAddCertificate.TabIndex = 25;
            this.btnAddCertificate.Text = "Add Certification";
            this.btnAddCertificate.UseVisualStyleBackColor = false;
            this.btnAddCertificate.Click += new System.EventHandler(this.btnAddCertificate_Click);
            // 
            // dtpCertificateDateFrom
            // 
            this.dtpCertificateDateFrom.CustomFormat = "yyyy/MM";
            this.dtpCertificateDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCertificateDateFrom.Location = new System.Drawing.Point(457, 19);
            this.dtpCertificateDateFrom.Name = "dtpCertificateDateFrom";
            this.dtpCertificateDateFrom.Size = new System.Drawing.Size(150, 34);
            this.dtpCertificateDateFrom.TabIndex = 23;
            // 
            // dtpCertificateDateTo
            // 
            this.dtpCertificateDateTo.CustomFormat = "yyyy/MM";
            this.dtpCertificateDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCertificateDateTo.Location = new System.Drawing.Point(694, 19);
            this.dtpCertificateDateTo.Name = "dtpCertificateDateTo";
            this.dtpCertificateDateTo.Size = new System.Drawing.Size(150, 34);
            this.dtpCertificateDateTo.TabIndex = 24;
            // 
            // lblCertificateDateTo
            // 
            this.lblCertificateDateTo.AutoSize = true;
            this.lblCertificateDateTo.Location = new System.Drawing.Point(656, 24);
            this.lblCertificateDateTo.Name = "lblCertificateDateTo";
            this.lblCertificateDateTo.Size = new System.Drawing.Size(33, 27);
            this.lblCertificateDateTo.TabIndex = 25;
            this.lblCertificateDateTo.Text = "To";
            // 
            // addJPSkillPanel
            // 
            this.addJPSkillPanel.AutoSize = true;
            this.addJPSkillPanel.Controls.Add(this.btnAddJPCertificate);
            this.addJPSkillPanel.Controls.Add(this.txtJPLevelName);
            this.addJPSkillPanel.Controls.Add(this.lblAddBandScore);
            this.addJPSkillPanel.Controls.Add(this.txtJPBandScore);
            this.addJPSkillPanel.Controls.Add(this.lblJPYear);
            this.addJPSkillPanel.Controls.Add(this.dtpJPYear);
            this.addJPSkillPanel.Location = new System.Drawing.Point(297, 186);
            this.addJPSkillPanel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.addJPSkillPanel.Name = "addJPSkillPanel";
            this.addJPSkillPanel.Size = new System.Drawing.Size(1054, 70);
            this.addJPSkillPanel.TabIndex = 27;
            // 
            // btnAddJPCertificate
            // 
            this.btnAddJPCertificate.AutoSize = true;
            this.btnAddJPCertificate.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddJPCertificate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddJPCertificate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddJPCertificate.ForeColor = System.Drawing.SystemColors.Window;
            this.btnAddJPCertificate.Location = new System.Drawing.Point(869, 14);
            this.btnAddJPCertificate.Name = "btnAddJPCertificate";
            this.btnAddJPCertificate.Size = new System.Drawing.Size(179, 46);
            this.btnAddJPCertificate.TabIndex = 21;
            this.btnAddJPCertificate.Text = "Add Certification";
            this.btnAddJPCertificate.UseVisualStyleBackColor = false;
            this.btnAddJPCertificate.Click += new System.EventHandler(this.btnAddJPCertificate_Click);
            // 
            // txtJPLevelName
            // 
            this.txtJPLevelName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJPLevelName.Location = new System.Drawing.Point(15, 18);
            this.txtJPLevelName.Name = "txtJPLevelName";
            this.txtJPLevelName.Size = new System.Drawing.Size(266, 34);
            this.txtJPLevelName.TabIndex = 18;
            this.txtJPLevelName.Text = "Japanese Level";
            // 
            // lblAddBandScore
            // 
            this.lblAddBandScore.AutoSize = true;
            this.lblAddBandScore.Location = new System.Drawing.Point(332, 21);
            this.lblAddBandScore.Name = "lblAddBandScore";
            this.lblAddBandScore.Size = new System.Drawing.Size(118, 27);
            this.lblAddBandScore.TabIndex = 11;
            this.lblAddBandScore.Text = "Score/Band";
            // 
            // txtJPBandScore
            // 
            this.txtJPBandScore.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJPBandScore.Location = new System.Drawing.Point(456, 21);
            this.txtJPBandScore.Name = "txtJPBandScore";
            this.txtJPBandScore.Size = new System.Drawing.Size(150, 34);
            this.txtJPBandScore.TabIndex = 19;
            // 
            // lblJPYear
            // 
            this.lblJPYear.AutoSize = true;
            this.lblJPYear.Location = new System.Drawing.Point(637, 21);
            this.lblJPYear.Name = "lblJPYear";
            this.lblJPYear.Size = new System.Drawing.Size(51, 27);
            this.lblJPYear.TabIndex = 15;
            this.lblJPYear.Text = "Year";
            // 
            // dtpJPYear
            // 
            this.dtpJPYear.CustomFormat = "yyyy/MM";
            this.dtpJPYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJPYear.Location = new System.Drawing.Point(694, 18);
            this.dtpJPYear.Name = "dtpJPYear";
            this.dtpJPYear.Size = new System.Drawing.Size(150, 34);
            this.dtpJPYear.TabIndex = 20;
            // 
            // txtEducation
            // 
            this.txtEducation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducation.Location = new System.Drawing.Point(312, 59);
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.Size = new System.Drawing.Size(266, 34);
            this.txtEducation.TabIndex = 17;
            this.txtEducation.Text = "B.C.Sc (Computer Science)";
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Location = new System.Drawing.Point(45, 62);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(103, 27);
            this.lblEducation.TabIndex = 0;
            this.lblEducation.Text = "Education";
            // 
            // gbContactInfo
            // 
            this.gbContactInfo.Controls.Add(this.rtxtTempAddr);
            this.gbContactInfo.Controls.Add(this.rtxtPermAddr);
            this.gbContactInfo.Controls.Add(this.txtEmgContactRelation);
            this.gbContactInfo.Controls.Add(this.lblEmgContactRelation);
            this.gbContactInfo.Controls.Add(this.txtEmgContactNo);
            this.gbContactInfo.Controls.Add(this.lblContactNo);
            this.gbContactInfo.Controls.Add(this.txtEmgContactName);
            this.gbContactInfo.Controls.Add(this.txtEmail);
            this.gbContactInfo.Controls.Add(this.lblEmgContact);
            this.gbContactInfo.Controls.Add(this.lblTempAddr);
            this.gbContactInfo.Controls.Add(this.lblPermAddr);
            this.gbContactInfo.Controls.Add(this.lblEmail);
            this.gbContactInfo.Controls.Add(this.txtPhone);
            this.gbContactInfo.Controls.Add(this.lblPhone);
            this.gbContactInfo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbContactInfo.Location = new System.Drawing.Point(88, 524);
            this.gbContactInfo.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.gbContactInfo.Name = "gbContactInfo";
            this.gbContactInfo.Size = new System.Drawing.Size(1382, 451);
            this.gbContactInfo.TabIndex = 25;
            this.gbContactInfo.TabStop = false;
            this.gbContactInfo.Text = "Contact Information";
            // 
            // rtxtTempAddr
            // 
            this.rtxtTempAddr.Location = new System.Drawing.Point(309, 266);
            this.rtxtTempAddr.Name = "rtxtTempAddr";
            this.rtxtTempAddr.Size = new System.Drawing.Size(266, 80);
            this.rtxtTempAddr.TabIndex = 13;
            this.rtxtTempAddr.Text = "Lathar 17 street, Yangon";
            // 
            // rtxtPermAddr
            // 
            this.rtxtPermAddr.Location = new System.Drawing.Point(309, 176);
            this.rtxtPermAddr.Name = "rtxtPermAddr";
            this.rtxtPermAddr.Size = new System.Drawing.Size(266, 80);
            this.rtxtPermAddr.TabIndex = 12;
            this.rtxtPermAddr.Text = "Lathar 17 street, Yangon";
            // 
            // txtEmgContactRelation
            // 
            this.txtEmgContactRelation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmgContactRelation.Location = new System.Drawing.Point(1146, 369);
            this.txtEmgContactRelation.Name = "txtEmgContactRelation";
            this.txtEmgContactRelation.Size = new System.Drawing.Size(150, 34);
            this.txtEmgContactRelation.TabIndex = 16;
            this.txtEmgContactRelation.Text = "Mother";
            // 
            // lblEmgContactRelation
            // 
            this.lblEmgContactRelation.AutoSize = true;
            this.lblEmgContactRelation.Location = new System.Drawing.Point(1053, 369);
            this.lblEmgContactRelation.Name = "lblEmgContactRelation";
            this.lblEmgContactRelation.Size = new System.Drawing.Size(87, 27);
            this.lblEmgContactRelation.TabIndex = 0;
            this.lblEmgContactRelation.Text = "Relation";
            // 
            // txtEmgContactNo
            // 
            this.txtEmgContactNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmgContactNo.Location = new System.Drawing.Point(851, 369);
            this.txtEmgContactNo.Name = "txtEmgContactNo";
            this.txtEmgContactNo.Size = new System.Drawing.Size(152, 34);
            this.txtEmgContactNo.TabIndex = 15;
            this.txtEmgContactNo.Text = "09796266500";
            // 
            // lblContactNo
            // 
            this.lblContactNo.AutoSize = true;
            this.lblContactNo.Location = new System.Drawing.Point(627, 369);
            this.lblContactNo.Name = "lblContactNo";
            this.lblContactNo.Size = new System.Drawing.Size(218, 27);
            this.lblContactNo.TabIndex = 0;
            this.lblContactNo.Text = "Emergency Contact No";
            // 
            // txtEmgContactName
            // 
            this.txtEmgContactName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmgContactName.Location = new System.Drawing.Point(309, 369);
            this.txtEmgContactName.Name = "txtEmgContactName";
            this.txtEmgContactName.Size = new System.Drawing.Size(266, 34);
            this.txtEmgContactName.TabIndex = 14;
            this.txtEmgContactName.Text = "Daw Khin Khin Cho";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(309, 119);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(266, 34);
            this.txtEmail.TabIndex = 11;
            this.txtEmail.Text = "dev.kaungthant@gmail.com";
            // 
            // lblEmgContact
            // 
            this.lblEmgContact.AutoSize = true;
            this.lblEmgContact.Location = new System.Drawing.Point(42, 369);
            this.lblEmgContact.Name = "lblEmgContact";
            this.lblEmgContact.Size = new System.Drawing.Size(246, 27);
            this.lblEmgContact.TabIndex = 0;
            this.lblEmgContact.Text = "Emergency Contact Name";
            // 
            // lblTempAddr
            // 
            this.lblTempAddr.AutoSize = true;
            this.lblTempAddr.Location = new System.Drawing.Point(42, 266);
            this.lblTempAddr.Name = "lblTempAddr";
            this.lblTempAddr.Size = new System.Drawing.Size(190, 27);
            this.lblTempAddr.TabIndex = 0;
            this.lblTempAddr.Text = "Temporary Address";
            // 
            // lblPermAddr
            // 
            this.lblPermAddr.AutoSize = true;
            this.lblPermAddr.Location = new System.Drawing.Point(42, 176);
            this.lblPermAddr.Name = "lblPermAddr";
            this.lblPermAddr.Size = new System.Drawing.Size(192, 27);
            this.lblPermAddr.TabIndex = 0;
            this.lblPermAddr.Text = "Permanent Address";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(43, 119);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(62, 27);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "Email";
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone.Location = new System.Drawing.Point(309, 62);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(266, 34);
            this.txtPhone.TabIndex = 10;
            this.txtPhone.Text = "09674225695";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(43, 62);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 27);
            this.lblPhone.TabIndex = 0;
            this.lblPhone.Text = "Phone";
            // 
            // gbPersonalInfo
            // 
            this.gbPersonalInfo.Controls.Add(this.imgBox);
            this.gbPersonalInfo.Controls.Add(this.lblGenderName);
            this.gbPersonalInfo.Controls.Add(this.lblDisplayDOB);
            this.gbPersonalInfo.Controls.Add(this.lblRollName);
            this.gbPersonalInfo.Controls.Add(this.dtpDOB);
            this.gbPersonalInfo.Controls.Add(this.btnUpload);
            this.gbPersonalInfo.Controls.Add(this.txtFatherName);
            this.gbPersonalInfo.Controls.Add(this.lblFatherName);
            this.gbPersonalInfo.Controls.Add(this.lblGender);
            this.gbPersonalInfo.Controls.Add(this.rdoFemale);
            this.gbPersonalInfo.Controls.Add(this.rdoMale);
            this.gbPersonalInfo.Controls.Add(this.lblNRC);
            this.gbPersonalInfo.Controls.Add(this.txtNRC);
            this.gbPersonalInfo.Controls.Add(this.lblDOB);
            this.gbPersonalInfo.Controls.Add(this.lblRole);
            this.gbPersonalInfo.Controls.Add(this.comboRole);
            this.gbPersonalInfo.Controls.Add(this.lblName);
            this.gbPersonalInfo.Controls.Add(this.txtEmployeeName);
            this.gbPersonalInfo.Controls.Add(this.lblEmployeeID);
            this.gbPersonalInfo.Controls.Add(this.txtEmployeeID);
            this.gbPersonalInfo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPersonalInfo.Location = new System.Drawing.Point(88, 27);
            this.gbPersonalInfo.Name = "gbPersonalInfo";
            this.gbPersonalInfo.Size = new System.Drawing.Size(1382, 474);
            this.gbPersonalInfo.TabIndex = 23;
            this.gbPersonalInfo.TabStop = false;
            this.gbPersonalInfo.Text = "Personal Information";
            // 
            // imgBox
            // 
            this.imgBox.BackColor = System.Drawing.Color.White;
            this.imgBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgBox.Location = new System.Drawing.Point(660, 65);
            this.imgBox.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.imgBox.Name = "imgBox";
            this.imgBox.Size = new System.Drawing.Size(186, 177);
            this.imgBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgBox.TabIndex = 21;
            this.imgBox.TabStop = false;
            // 
            // lblGenderName
            // 
            this.lblGenderName.AutoSize = true;
            this.lblGenderName.Location = new System.Drawing.Point(305, 345);
            this.lblGenderName.Name = "lblGenderName";
            this.lblGenderName.Size = new System.Drawing.Size(156, 27);
            this.lblGenderName.TabIndex = 0;
            this.lblGenderName.Text = "lblGenderName";
            // 
            // lblDisplayDOB
            // 
            this.lblDisplayDOB.AutoSize = true;
            this.lblDisplayDOB.Location = new System.Drawing.Point(305, 237);
            this.lblDisplayDOB.Name = "lblDisplayDOB";
            this.lblDisplayDOB.Size = new System.Drawing.Size(141, 27);
            this.lblDisplayDOB.TabIndex = 0;
            this.lblDisplayDOB.Text = "lblDisplayDOB";
            // 
            // lblRollName
            // 
            this.lblRollName.AutoSize = true;
            this.lblRollName.Location = new System.Drawing.Point(305, 178);
            this.lblRollName.Name = "lblRollName";
            this.lblRollName.Size = new System.Drawing.Size(122, 27);
            this.lblRollName.TabIndex = 0;
            this.lblRollName.Text = "lblRollName";
            // 
            // dtpDOB
            // 
            this.dtpDOB.CalendarFont = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.CustomFormat = "yyyy/MM/dd";
            this.dtpDOB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Location = new System.Drawing.Point(308, 237);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(266, 34);
            this.dtpDOB.TabIndex = 4;
            this.dtpDOB.Value = new System.DateTime(2024, 2, 13, 14, 8, 23, 0);
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged);
            // 
            // btnUpload
            // 
            this.btnUpload.AutoSize = true;
            this.btnUpload.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpload.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpload.ForeColor = System.Drawing.SystemColors.Window;
            this.btnUpload.Location = new System.Drawing.Point(668, 248);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(170, 47);
            this.btnUpload.TabIndex = 9;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // txtFatherName
            // 
            this.txtFatherName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFatherName.Location = new System.Drawing.Point(309, 393);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(266, 34);
            this.txtFatherName.TabIndex = 8;
            this.txtFatherName.Text = "U Hla";
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFatherName.Location = new System.Drawing.Point(43, 393);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(129, 27);
            this.lblFatherName.TabIndex = 0;
            this.lblFatherName.Text = "Father Name";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(42, 345);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(80, 27);
            this.lblGender.TabIndex = 0;
            this.lblGender.Text = "Gender";
            // 
            // rdoFemale
            // 
            this.rdoFemale.AutoSize = true;
            this.rdoFemale.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoFemale.Location = new System.Drawing.Point(461, 345);
            this.rdoFemale.Name = "rdoFemale";
            this.rdoFemale.Size = new System.Drawing.Size(99, 31);
            this.rdoFemale.TabIndex = 7;
            this.rdoFemale.Text = "Female";
            this.rdoFemale.UseVisualStyleBackColor = true;
            // 
            // rdoMale
            // 
            this.rdoMale.AutoSize = true;
            this.rdoMale.Checked = true;
            this.rdoMale.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoMale.Location = new System.Drawing.Point(348, 345);
            this.rdoMale.Name = "rdoMale";
            this.rdoMale.Size = new System.Drawing.Size(79, 31);
            this.rdoMale.TabIndex = 6;
            this.rdoMale.TabStop = true;
            this.rdoMale.Text = "Male";
            this.rdoMale.UseVisualStyleBackColor = true;
            // 
            // lblNRC
            // 
            this.lblNRC.AutoSize = true;
            this.lblNRC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNRC.Location = new System.Drawing.Point(43, 291);
            this.lblNRC.Name = "lblNRC";
            this.lblNRC.Size = new System.Drawing.Size(81, 27);
            this.lblNRC.TabIndex = 0;
            this.lblNRC.Text = "NRC No";
            // 
            // txtNRC
            // 
            this.txtNRC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNRC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNRC.Location = new System.Drawing.Point(308, 291);
            this.txtNRC.Name = "txtNRC";
            this.txtNRC.Size = new System.Drawing.Size(266, 34);
            this.txtNRC.TabIndex = 5;
            this.txtNRC.Text = "12/MABANA(N)182003";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.Location = new System.Drawing.Point(43, 237);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(128, 27);
            this.lblDOB.TabIndex = 0;
            this.lblDOB.Text = "Date of Birth";
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.Location = new System.Drawing.Point(43, 178);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 27);
            this.lblRole.TabIndex = 0;
            this.lblRole.Text = "Role";
            // 
            // comboRole
            // 
            this.comboRole.BackColor = System.Drawing.SystemColors.Window;
            this.comboRole.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRole.FormattingEnabled = true;
            this.comboRole.IntegralHeight = false;
            this.comboRole.Items.AddRange(new object[] {
            "Administrator",
            "Staff"});
            this.comboRole.Location = new System.Drawing.Point(308, 178);
            this.comboRole.MaxDropDownItems = 2;
            this.comboRole.Name = "comboRole";
            this.comboRole.Size = new System.Drawing.Size(266, 35);
            this.comboRole.Sorted = true;
            this.comboRole.TabIndex = 3;
            this.comboRole.Text = "Staff";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(42, 121);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(66, 27);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeName.Location = new System.Drawing.Point(308, 121);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Size = new System.Drawing.Size(266, 34);
            this.txtEmployeeName.TabIndex = 2;
            this.txtEmployeeName.Text = "Kaung Thant";
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeID.Location = new System.Drawing.Point(43, 65);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(127, 27);
            this.lblEmployeeID.TabIndex = 0;
            this.lblEmployeeID.Text = "Employee ID";
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeID.Location = new System.Drawing.Point(308, 65);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Size = new System.Drawing.Size(266, 34);
            this.txtEmployeeID.TabIndex = 1;
            this.txtEmployeeID.Text = "T2023006";
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1582, 1018);
            this.Controls.Add(this.containerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "Profile";
            this.Text = "User Profile";
            this.Load += new System.EventHandler(this.profileForm_Load);
            this.Controls.SetChildIndex(this.StaffPanel, 0);
            this.Controls.SetChildIndex(this.containerPanel, 0);
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            this.containerPanel.ResumeLayout(false);
            this.containerPanel.PerformLayout();
            this.gbWorkExperience.ResumeLayout(false);
            this.workExperiencePanel.ResumeLayout(false);
            this.workExperiencePanel.PerformLayout();
            this.addExperiencePanel.ResumeLayout(false);
            this.addExperiencePanel.PerformLayout();
            this.gbEducation.ResumeLayout(false);
            this.gbEducation.PerformLayout();
            this.certificatePanel.ResumeLayout(false);
            this.certificatePanel.PerformLayout();
            this.jpCertificatePanel.ResumeLayout(false);
            this.jpCertificatePanel.PerformLayout();
            this.addCertificatePanel.ResumeLayout(false);
            this.addCertificatePanel.PerformLayout();
            this.addJPSkillPanel.ResumeLayout(false);
            this.addJPSkillPanel.PerformLayout();
            this.gbContactInfo.ResumeLayout(false);
            this.gbContactInfo.PerformLayout();
            this.gbPersonalInfo.ResumeLayout(false);
            this.gbPersonalInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.GroupBox gbContactInfo;
        private System.Windows.Forms.GroupBox gbPersonalInfo;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.RadioButton rdoFemale;
        private System.Windows.Forms.RadioButton rdoMale;
        private System.Windows.Forms.Label lblNRC;
        private System.Windows.Forms.TextBox txtNRC;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.ComboBox comboRole;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtEmployeeName;
        private System.Windows.Forms.Label lblEmployeeID;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtEmgContactName;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmgContact;
        private System.Windows.Forms.Label lblTempAddr;
        private System.Windows.Forms.Label lblPermAddr;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmgContactNo;
        private System.Windows.Forms.Label lblContactNo;
        private System.Windows.Forms.TextBox txtEmgContactRelation;
        private System.Windows.Forms.Label lblEmgContactRelation;
        private System.Windows.Forms.GroupBox gbEducation;
        private System.Windows.Forms.TextBox txtJPLevelName;
        private System.Windows.Forms.Label lblJPLangLevel;
        private System.Windows.Forms.TextBox txtEducation;
        private System.Windows.Forms.Label lblEducation;
        private System.Windows.Forms.RichTextBox rtxtTempAddr;
        private System.Windows.Forms.RichTextBox rtxtPermAddr;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label lblJPCertificateDate;
        private System.Windows.Forms.Label lblJPBandScore;
        private System.Windows.Forms.Button btnAddJPCertificate;
        private System.Windows.Forms.DateTimePicker dtpJPYear;
        private System.Windows.Forms.Label lblJPYear;
        private System.Windows.Forms.TextBox txtJPBandScore;
        private System.Windows.Forms.Label lblAddBandScore;
        private System.Windows.Forms.TextBox txtCertificateName;
        private System.Windows.Forms.Label lblCertificateDate;
        private System.Windows.Forms.Label lblCertificateName;
        private System.Windows.Forms.Label lblCertificate;
        private System.Windows.Forms.Label lblCertificateDateTo;
        private System.Windows.Forms.Label lblCertificateDateFrom;
        private System.Windows.Forms.DateTimePicker dtpCertificateDateTo;
        private System.Windows.Forms.DateTimePicker dtpCertificateDateFrom;
        private System.Windows.Forms.GroupBox gbWorkExperience;
        private System.Windows.Forms.Label lblNameLocation;
        private System.Windows.Forms.Label lblWorkExperienceDate;
        private System.Windows.Forms.Label lblWorkPosition;
        private System.Windows.Forms.Label lblWorkExperience;
        private System.Windows.Forms.Button btnAddCertificate;
        private System.Windows.Forms.Button btnAddExperience;
        private System.Windows.Forms.Label lblExperienceDateTo;
        private System.Windows.Forms.Label lblExperienceDateFrom;
        private System.Windows.Forms.DateTimePicker dtpExperienceDateTo;
        private System.Windows.Forms.DateTimePicker dtpExperienceDateFrom;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtWorkPosition;
        private System.Windows.Forms.TextBox txtCompanyLocation;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Panel addJPSkillPanel;
        private System.Windows.Forms.Panel addCertificatePanel;
        private System.Windows.Forms.Panel addExperiencePanel;
        private System.Windows.Forms.Label lblRollName;
        private System.Windows.Forms.Label lblGenderName;
        private System.Windows.Forms.Label lblDisplayDOB;
        private System.Windows.Forms.Label lblJPLevelName;
        private System.Windows.Forms.RadioButton rdoSetDefault1;
        private System.Windows.Forms.Panel jpCertificatePanel;
        private System.Windows.Forms.Panel certificatePanel;
        private System.Windows.Forms.Panel workExperiencePanel;
        private System.Windows.Forms.PictureBox imgBox;
    }
}

