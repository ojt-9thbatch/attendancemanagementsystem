﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;



namespace Attendance_Management_System
{
    /// <summary>
    /// Login Form Design by KT
    /// </summary>
    public partial class Login : Form
    {
        //variable declaration
        public static SqlConnection conn = null;
        string encryptionKey; // for encryption
        public static string employeeName;
        public static int role;
        public static string employeeId;
        public void connection()
        {
            conn = new SqlConnection(@"data source=TMPC-057; Database=AttendanceDB; integrated security = true");
        }

        public Login()
        {

            InitializeComponent();
            this.BackColor = Color.White;

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string employeeID = txtEmployeeID.Text;
            string password = txtPassword.Text;

            // Check if username is empty
            if (string.IsNullOrWhiteSpace(employeeID))
            {
                MessageBox.Show("Please fill employee id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //Err035
                txtEmployeeID.Focus();
                return;
            }

            // Check if password is empty
            if (string.IsNullOrWhiteSpace(password))
            {
                MessageBox.Show("Please fill your password.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);//Err069
                txtPassword.Focus();
                return;
            }

            // Validate username format 
            if (!IsValidUsernameFormat(employeeID))
            {
                MessageBox.Show("Employee ID format is not correct (Eg.TXXX)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmployeeID.Text = ""; // Clear the text box
                txtPassword.Text = password;   // Set focus to the Employee ID text box
                return;
            }
            else
            {
                SqlDataReader reader = null;
                try
                {
                    connection();
                    conn.Open();

                    string query = "select emp_id, emp_password, emp_role, emp_name from t_emp_basic_info where emp_id = @EmployeeID";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@EmployeeID", employeeID);

                    reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        string empPassword = reader["emp_password"].ToString();

                        string encryptedText = Encrypt(txtPassword.Text, encryptionKey);
                        int empRole = Convert.ToInt32(reader["emp_role"]);

                        // Check if the password matches
                        if (empPassword == encryptedText)
                        {
                            MessageBox.Show("Login successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);//msg003

                            clearTextBoxes();

                            role = empRole;
                            employeeName = reader["emp_name"].ToString() + " !";
                            employeeId = reader["emp_id"].ToString();

                            MenuBar menu = new MenuBar();
                            menu.Show();
                            this.Hide();


                        }
                        else
                        {
                            // Password doesn't match
                            MessageBox.Show("Password is incorrect", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);//Err050
                            txtPassword.Text = "";
                            txtPassword.Focus();
                        }
                    }
                    else
                    {
                        // Employee ID not found
                        MessageBox.Show("Employee Id doesn't exit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);//Err049
                        txtEmployeeID.Text = "";
                        txtEmployeeID.Focus();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid login: " + ex.Message);
                    clearTextBoxes();
                    txtEmployeeID.Focus();
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (conn != null && conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                }
            }
        }

        //check username format correct or not
        private bool IsValidUsernameFormat(string username)
        {
            //check if the username starts with a capital letter "T"
            if (username.Length > 0 && username[0] == 'T')
            {
                return true;
                
            }
            clearTextBoxes();
            txtEmployeeID.Focus();
            return false;
        }

        private void chkShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            // Toggle password visibility based on checkbox state
            txtPassword.UseSystemPasswordChar = !chkShowPassword.Checked;
            txtPassword.Focus();
        }

        // This function takes a string `text` and a string `key` as input parameters.
        // It encrypts the input `text` using AES (Advanced Encryption Standard) algorithm with the provided `key`.

        public static string Encrypt(String text, String key)
        {
            // Creating an instance of AES algorithm
            using (Aes aesAlg = Aes.Create())
            {
                // Setting the key for AES encryption
                aesAlg.Key = Encoding.UTF8.GetBytes(key);

                // Initialization Vector (IV) should be unique for each encryption, 
                // but for simplicity, a static IV is being used here.
                aesAlg.IV = new byte[16];

                // Creating an encryptor object for AES encryption with the provided key and IV
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Using a MemoryStream to store the encrypted data
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // Using CryptoStream to perform the encryption
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        // Using StreamWriter to write the plaintext `text` to the CryptoStream
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }
                    }

                    // Converting the encrypted data to Base64 string and returning it
                    return Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
        }


        private void Login_Load(object sender, EventArgs e)
        {
            txtPassword.UseSystemPasswordChar = true; // Set to true to hide password by default

            encryptionKey = "abcdefghijkmnopq";// character can change whatever you want 16 Characters

            chkShowPassword.CheckedChanged += chkShowPassword_CheckedChanged;

        }

        public void clearTextBoxes()
        {
            txtEmployeeID.Text = "";
            txtPassword.Text = "";
        }
    }
}
