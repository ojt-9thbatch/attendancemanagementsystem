﻿using Attendance_Management_System;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace AttendanceManagementSystem
{
    public partial class EmployeeRegistrationForm : MenuBar
    {
        SqlConnection conn; 
        string call_Status;
        string empID = "";

        DataTable dtCertification = new DataTable();
        DataTable dtJpskill = new DataTable();
        DataTable dtWorkExp = new DataTable();

        bool jpskillClicked = false;
        bool certificationClicked = false;
        bool workexpClicked = false;
        string encryptionKey = "abcdefghijkmnopq";

        // retrieve parameter from function call page and use to set button text;
        public string _btnParameters
        {
            set { call_Status = value; }
        }
        public void Connection()
        {
            conn = new SqlConnection(@"data source=TMPC-056; Database=AttendanceDB; integrated security = true;");
        }
        public EmployeeRegistrationForm(string emp_id)
        {
            InitializeComponent();
            btnEmployee.BackColor = Color.FromArgb(151, 242, 0); // set button color on which page is loaded
            // get emp_id parameter from employee list form
            this.empID = emp_id;
        }
        // declare database connection statement
        public void register_allBlank()
        {
            foreach (var txtBox in Controls)
            {
                if (txtBox is System.Windows.Forms.TextBox)
                {
                    ((System.Windows.Forms.TextBox)txtBox).Text = string.Empty;
                }
            }
            txtPermanentAddr.Text = string.Empty;
            txtTemporaryAddr.Text = string.Empty;

            // set all datetimpicker current date and format "Year/Month";
            dtpDob.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd"); // set sample value D.O.B for initial display
            dtpJpYear.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
            dtpCertificateFrm.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
            dtpCertificateTo.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
            dtpWorkExpFrm.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
            dtpWorkExpTo.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");

            ddRole.Text = "Staff"; //set default value for role in combo box
        }
        // image validation checker
        // image file size must be 1 MB
        public bool Check_imgsize(string filepath, long maxSize)
        {
            long length = new FileInfo(filepath).Length / 1024;

            if (length < maxSize)
            {
                return true; // return true if image size is bigger than 1Mb.
            }
            else
            {
                return false;
            }
        }
        // Email Format checking
        public bool IsValid(string emailaddress)
        {
            //format only gmail address e.g "youremail@gmail.com"
            string emailPattern = @"^[a-z0-9._%+-]+@gmail\.com$";

            Regex regex = new Regex(emailPattern);
            return regex.IsMatch(emailaddress);
        }
        private bool rdoBtnCheckOrNot(Control container)
        {
            // set default value to false (radio button unchecked)
            bool rdoChecked = false;
            // check radio button is checked or not?
            foreach (Control ctrl in container.Controls)
            {
                if (ctrl is RadioButton)
                {
                    RadioButton radio = ctrl as RadioButton;
                    if (radio.Checked)
                    {
                        // if radio button is checked, return checked
                        rdoChecked = true;
                        break;
                    }
                }
            }
            return rdoChecked;
        }
        /// <summary>
        /// check the require field for textboxes and date time
        /// return true if error exists
        /// return false if there is no error
        /// </summary>
        public bool errorChecking()
            {
                if (!string.IsNullOrEmpty(txtEmpId.Text.Trim()))
                {
                    if (!txtEmpId.Text.StartsWith("T"))
                    {
                        MessageBox.Show("Employee Id format is not correct (Eg.TXXX)");
                        txtEmpId.Clear();
                        txtEmpId.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill employee Id");
                    txtEmpId.Focus();
                    return true;
                }
                if (!string.IsNullOrEmpty(txtEmpName.Text.Trim()))
                {
                    if (Regex.IsMatch(txtEmpName.Text, "[0-9]"))
                    {
                        MessageBox.Show("Name must not be integer");
                        txtEmpName.Clear();
                        txtEmpName.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill name");
                    txtEmpName.Focus();
                    return true;
                }
                if (dtpDob.Value > DateTime.Now)
                {
                    MessageBox.Show("Date of birth must be less than current date");
                    return true;
                }
                if (string.IsNullOrEmpty(txtNrcNo.Text.Trim()))
                {
                    MessageBox.Show("Please fill  NRC number");
                    txtNrcNo.Focus();
                    return true;
                }
                if ((rdoFemale.Checked == false) && (rdoMale.Checked == false))
                {
                    MessageBox.Show("Please choose gender");
                    rdoMale.Focus();
                    return true;
                }
           
                if (!string.IsNullOrEmpty(txtFatherName.Text.Trim()))
                {
                    if (Regex.IsMatch(txtFatherName.Text, "[0-9]"))
                    {
                        MessageBox.Show("Father Name must not be integer");
                        txtFatherName.Clear();
                        txtFatherName.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill father name");
                    txtFatherName.Focus();
                    return true;
                }
            
                if (!string.IsNullOrEmpty(txtPhone.Text.Trim()))
                {
                    if (txtPhone.Text.StartsWith("09") == true)
                    {
                        if (!(txtPhone.Text.Length == 9 || txtPhone.Text.Length == 11))
                        {
                             MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                             txtPhone.Clear();
                             txtPhone.Focus();
                             return true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                        txtPhone.Clear();
                        txtPhone.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill phone number using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                    txtPhone.Focus();
                    return true;
                }
            
                if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
                {
                    // Email format check
                    if (!IsValid(txtEmail.Text.Trim()))
                    {
                        MessageBox.Show("Email format is invalid, format is \"youremailaddress@gmail.com\"");
                        txtEmail.Clear();
                        txtEmail.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill email address");
                    txtEmail.Focus();
                    return true;
                }
                if (string.IsNullOrEmpty(txtPermanentAddr.Text.Trim()))
                {
                    MessageBox.Show("Please fill permanent address");
                    txtPermanentAddr.Focus();
                    return true;
                }
                if (string.IsNullOrEmpty(txtTemporaryAddr.Text.Trim()))
                {
                    MessageBox.Show("Please fill temporary address");
                    txtTemporaryAddr.Focus();
                    return true;
                }
                if (!string.IsNullOrEmpty(txtEmergencyContactName.Text.Trim()))
                {
                    if (Regex.IsMatch(txtEmergencyContactName.Text, "[0-9]"))
                    {
                        MessageBox.Show("Contact Name must not be integer");
                        txtEmergencyContactName.Clear();
                        txtEmergencyContactName.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill emergency contact name");
                    txtEmergencyContactName.Focus();
                    return true;
                }

                if (!string.IsNullOrEmpty(txtEmergencyContactNo.Text.Trim()))
                {
                    if (txtPhone.Text.StartsWith("09") == true)
                    {
                        if (!(txtEmergencyContactNo.Text.Length == 9 || txtEmergencyContactNo.Text.Length == 11))
                        {
                                MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                                txtEmergencyContactNo.Clear();
                                txtEmergencyContactNo.Focus();
                                return true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                        txtEmergencyContactNo.Clear();
                        txtEmergencyContactNo.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill phone number using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                    txtEmergencyContactNo.Focus();
                    return true;
                }
                if(!string.IsNullOrEmpty(txtRelation.Text.Trim()))
                {
                    if (Regex.IsMatch(txtRelation.Text, "[0-9]"))
                    {
                        MessageBox.Show("Contact Relation must not be integer");
                        txtRelation.Clear();
                        txtRelation.Focus();
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Please fill relation");
                    txtRelation.Focus();
                    return true;
                }
                if (string.IsNullOrEmpty(txtEducation.Text.Trim()))
                {
                    MessageBox.Show("Please fill education");
                    txtEducation.Focus();
                    return true;
                }
                if (imgBox.Image == null)
                {
                    MessageBox.Show("Please attach image");
                    btnUploadImg.Focus();
                    return true;
                }
                if (rdoBtnCheckOrNot(pJpLevelList) == false)
                {
                    MessageBox.Show("Pls choose max level beside japanese level");
                    return true;
                }
            return false; // return false if there's no error in registration process
        }
        public static string Encrypt(string text, string key)
        {
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(key);
                aesAlg.IV = new byte[16];// InitializationEventAttribute Vector (IV) should be unique,but for simplicity,we use a static Iv
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }
                    }
                    return Convert.ToBase64String(msEncrypt.ToArray()); // return the encrypted password
                }
            }
        }

        /// <summary>
        /// This method set all date time picker display in format " Year/Month/day "
        /// </summary>
        public void dateTimeFormat()
        {
            // format all datetimepicker as year,month except D.O.B;
            dtpDob.Format = DateTimePickerFormat.Custom;
            dtpDob.CustomFormat = "yyyy/MM/dd";

            dtpWorkExpFrm.Format = DateTimePickerFormat.Custom;
            dtpWorkExpFrm.CustomFormat = "yyyy/MM";

            dtpWorkExpTo.Format = DateTimePickerFormat.Custom;
            dtpWorkExpTo.CustomFormat = "yyyy/MM";

            dtpCertificateFrm.Format = DateTimePickerFormat.Custom;
            dtpCertificateFrm.CustomFormat = "yyyy/MM";

            dtpCertificateTo.Format = DateTimePickerFormat.Custom;
            dtpCertificateTo.CustomFormat = "yyyy/MM";

            dtpJpYear.Format = DateTimePickerFormat.Custom;
            dtpJpYear.CustomFormat = "yyyy/MM";

        }
        private void updateJPMaxLevel(int newMax) // parameter rdoCount
        {
            Connection();

            // set all max level to '0'
            foreach (DataRow r in dtJpskill.Rows)
            {
                r[4] = 0;
            }

            // set selected specific row's max level to '1'
            DataRow dr = dtJpskill.Rows[newMax - 1];
            dr[4] = 1;

            string jpCertificateName = dr[0].ToString();


            int zero = 0;

            string setZeroQuery = @"update t_emp_jpkill set emp_jpskill_maxlevel = '" + zero + "' where emp_id = '" + txtEmpId.Text.Trim().ToString() + "'";

            SqlCommand zeroCmd = new SqlCommand(setZeroQuery, conn);

            conn.Open();

            zeroCmd.ExecuteNonQuery();

            conn.Close();

            string updateMaxLevelQuery = @"update t_emp_jpkill Set emp_jpskill_maxlevel = @newMax where emp_jpskill_level_name = '" + jpCertificateName + "'and emp_id = '" + txtEmpId.Text.Trim().ToString() + "'";

            SqlCommand cmd = new SqlCommand(updateMaxLevelQuery, conn);

            cmd.Parameters.AddWithValue("@newMax", 1);
            conn.Open();

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        /// <summary>
        /// get all employee info for update section
        /// </summary>
        public void load_BasicInfo()
        {
            string selectEmpData = @"select * from t_emp_basic_info where emp_id=@employee_id";
            conn.Open();
            SqlCommand cmd = new SqlCommand(selectEmpData, conn);
            cmd.Parameters.AddWithValue("@employee_id", empID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                // fill data in textbox for related employee ID
                txtEmpId.Text = empID;
                txtEmpId.Enabled = false;

                txtEmpName.Text = reader.GetValue(2).ToString().Trim();
                // role 1 for administrator, 2 for staff
                var role = reader.GetValue(4).ToString().Trim();
                if (role == "1")
                {
                    ddRole.Text = "Administrator";
                }
                else
                {
                    ddRole.Text = "Staff";
                }
                dtpDob.Text = Convert.ToDateTime(reader.GetValue(5)).ToString("yyyy/MM/dd");
                txtNrcNo.Text = reader.GetValue(6).ToString().Trim();
                // set radio button default 
                // male default value 1
                // female default value 2
                if (reader.GetValue(7).ToString() == "1")
                {
                    rdoMale.Checked = true;
                }
                else if (reader.GetValue(7).ToString() == "2")
                {
                    rdoFemale.Checked = true;
                }

                txtFatherName.Text = reader.GetValue(8).ToString().Trim();
                txtPhone.Text = "09" + reader.GetValue(9);
                txtEmail.Text = reader.GetValue(10).ToString().Trim();
                txtPermanentAddr.Text = reader.GetValue(11).ToString().Trim();
                txtTemporaryAddr.Text = reader.GetValue(12).ToString().Trim();
                txtEmergencyContactName.Text = reader.GetValue(13).ToString().Trim();
                txtEmergencyContactNo.Text = "09" + reader.GetValue(14);
                txtRelation.Text = reader.GetValue(15).ToString().Trim();
                txtEducation.Text = reader.GetValue(16).ToString().Trim();

                string imageName = txtEmpId.Text.Trim().ToString();
                // create a local path where profile pictures are stored
                string localPath = @"C:\Users\T2023007\Desktop\Attendance Management System\temp\";
                
                string[] profilePic = Directory.GetFiles(localPath, $"{imageName}.*");

                imgBox.Image = new Bitmap(profilePic[0]);

            }
            reader.Close();
            conn.Close();
        }
        public void LoadJpSkillData()
        {
            // Education background groupBox
            // Showing result for JP Level Skill
            if (btnRegister.Text == "Register") //create only data table columns for register form
            {
                dtJpskill.Columns.Add("Japan Level Data");
                dtJpskill.Columns.Add("Score");
                dtJpskill.Columns.Add("Year");
                dtJpskill.Columns.Add("From"); // From column define the source of data ( data from database or new added from button)
                dtJpskill.Columns.Add("Max Level", typeof(int));
            }
            if (btnRegister.Text == "Update")
            {
                dtJpskill.Columns.Add("Japan Level Data");
                dtJpskill.Columns.Add("Score");
                dtJpskill.Columns.Add("Year");
                dtJpskill.Columns.Add("From");
                dtJpskill.Columns.Add("Max Level", typeof(int));

                string selectJpLevelSkill = "select * from t_emp_jpkill where emp_id=@employee_id";
                conn.Open();
                SqlCommand Jpkillcmd = new SqlCommand(selectJpLevelSkill, conn);
                Jpkillcmd.Parameters.AddWithValue("@employee_id", empID);
                SqlDataReader Jpkillreader = Jpkillcmd.ExecuteReader();
                // set location to labels on first row
                int top = 17;
            
                while (Jpkillreader.Read())
                {
                    // display JP skill data on pJpLevelList
                    dtJpskill.Rows.Add(Jpkillreader[2].ToString(), Jpkillreader[3].ToString(), Jpkillreader[4].ToString(), "Database", Jpkillreader[5]);
                }
                Jpkillreader.Close();
                conn.Close();

                foreach (DataRow row in dtJpskill.Rows)
                {
                    Label lblJpLevelData = new Label();
                    Label lblJpScore = new Label();
                    Label lblJpYear = new Label();
                    RadioButton rdoJpDefaultData = new RadioButton();

                    lblJpLevelData.AutoSize = true;
                    lblJpLevelData.Location = new System.Drawing.Point(7, top);
                    lblJpLevelData.Name = "lblJpLevelData";
                    lblJpLevelData.Size = new System.Drawing.Size(54, 22);
                    lblJpLevelData.TabIndex = 38;
                    lblJpLevelData.Text = row[0].ToString();

                    lblJpScore.AutoSize = true;
                    lblJpScore.Location = new System.Drawing.Point(lblJpLevelData.Right + 130, top);
                    lblJpScore.Name = "lblJpScore";
                    lblJpScore.Size = new System.Drawing.Size(54, 22);
                    lblJpScore.TabIndex = 39;
                    lblJpScore.Text = row[1].ToString();

                    lblJpYear.AutoSize = true;
                    lblJpYear.Location = new System.Drawing.Point(lblJpScore.Right + 100, top);
                    lblJpYear.Name = "lblJpYear";
                    lblJpYear.Size = new System.Drawing.Size(54, 22);
                    lblJpYear.TabIndex = 39;
                    lblJpYear.Text = Convert.ToDateTime(row[2]).ToString("yyyy/MM");

                    if (Convert.ToInt32(row[4]) == 1)
                    {
                        rdoJpDefaultData.Checked = true;
                    }
                    else if (Convert.ToInt32(row[4]) == 0)
                    {
                        rdoJpDefaultData.Checked = false;
                    }
                    rdoJpDefaultData.AutoSize = true;
                    rdoJpDefaultData.Location = new System.Drawing.Point(lblJpYear.Right + 120, top);
                    rdoJpDefaultData.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
                    rdoJpDefaultData.Name = "rdoJpDefaultData";
                    rdoJpDefaultData.Size = new System.Drawing.Size(129, 26);
                    rdoJpDefaultData.TabIndex = 37;
                    rdoJpDefaultData.TabStop = true;
                    rdoJpDefaultData.Text = "set as default";
                    rdoJpDefaultData.UseVisualStyleBackColor = true;

                    pJpLevelList.Controls.Add(lblJpLevelData);
                    pJpLevelList.Controls.Add(lblJpScore);
                    pJpLevelList.Controls.Add(lblJpYear);
                    pJpLevelList.Controls.Add(rdoJpDefaultData);
                    top = lblJpLevelData.Bottom + 10;
                }
                pJpLevelList.AutoSize = true;
                pJpLevelList.AutoSizeMode = AutoSizeMode.GrowOnly;

                pAddEducation.Top = pJpLevelList.Bottom + 10;
                pCertificationList.Top = pAddEducation.Bottom + 10;
                gpBoxEduBackground.AutoSize = true;
                gpBoxEduBackground.AutoSizeMode = AutoSizeMode.GrowOnly;

                gpBoxWorkExp.Top = gpBoxEduBackground.Bottom + 20;

            }

        }
        public void LoadCertification_List()
        {
            if (btnRegister.Text == "Register")
            {
                //pCertificationList.Controls.Clear();

                dtCertification.Columns.Add("Certificate");
                dtCertification.Columns.Add("CertificateFrom");
                dtCertification.Columns.Add("CertificateTo");
                dtCertification.Columns.Add("From");
            }
            else if (btnRegister.Text == "Update")
            {
                dtCertification.Columns.Add("Certificate");
                dtCertification.Columns.Add("CertificateFrom");
                dtCertification.Columns.Add("CertificateTo");
                dtCertification.Columns.Add("From");

                // retrieve certification data from t_emp_certification
                String selectCertificate = "select * from t_emp_certification where emp_id = @employee_id";
                conn.Open();
                SqlCommand Cert_cmd = new SqlCommand(selectCertificate, conn);
                Cert_cmd.Parameters.AddWithValue("@employee_id", empID);
                SqlDataReader Cert_reader = Cert_cmd.ExecuteReader();
                int top = 10;
                while (Cert_reader.Read())
                {
                    dtCertification.Rows.Add(Cert_reader[2].ToString(), Convert.ToDateTime(Cert_reader[3]).ToString("yyyy/MM"), Convert.ToDateTime(Cert_reader[4]).ToString("yyyy/MM"), "Database");
                }
                Cert_reader.Close();
                conn.Close();

                foreach (DataRow row in dtCertification.Rows)
                {
                    Label lblCertificateData = new Label();
                    Label lblCertificateYearData = new Label();
                    string certificateFrom = row[1].ToString();
                    string certificateTo = row[2].ToString();

                    lblCertificateData.AutoSize = true;
                    lblCertificateData.Location = new System.Drawing.Point(185, top);
                    lblCertificateData.Name = "lblCertificateData";
                    lblCertificateData.Size = new System.Drawing.Size(54, 22);
                    lblCertificateData.Text = row[0].ToString();

                    lblCertificateYearData.AutoSize = true;
                    lblCertificateYearData.Location = new System.Drawing.Point(lblCertificateData.Right + 180, top);
                    lblCertificateYearData.Name = "lblCertificateYearData";
                    lblCertificateYearData.Size = new System.Drawing.Size(54, 22);
                    lblCertificateYearData.Text = Convert.ToDateTime(certificateFrom).ToString("yyyy/MM") + " - " + Convert.ToDateTime(certificateTo).ToString("yyyy/MM");

                    pCertificationList.Controls.Add(lblCertificateData);
                    pCertificationList.Controls.Add(lblCertificateYearData);
                    top = lblCertificateData.Bottom + 10;
                }
                pCertificationList.AutoSize = true;
                pCertificationList.AutoSizeMode = AutoSizeMode.GrowOnly;
                pCertificationList.Top = pAddEducation.Bottom + 10;
                pAddCertification.Top = pCertificationList.Bottom + 10;
                
                gpBoxWorkExp.Top = gpBoxEduBackground.Bottom + 20;
            }
        }
        public void LoadWorkExpData()
        {
            if (btnRegister.Text == "Register")
            {
                //pWorkExp.Controls.Clear();
                dtWorkExp.Columns.Add("Work Experience Name", typeof(string));
                dtWorkExp.Columns.Add("Work Exp Year From", typeof(DateTime));
                dtWorkExp.Columns.Add("Work Exp Year To", typeof(DateTime));
                dtWorkExp.Columns.Add("Company Name", typeof(string));
                dtWorkExp.Columns.Add("Location", typeof(string));
                dtWorkExp.Columns.Add("From", typeof(string));
            }
            else if (btnRegister.Text == "Update")
            {
                dtWorkExp.Columns.Add("Work Experience Name", typeof(string));
                dtWorkExp.Columns.Add("Work Exp Year From", typeof(DateTime));
                dtWorkExp.Columns.Add("Work Exp Year To", typeof(DateTime));
                dtWorkExp.Columns.Add("Company Name", typeof(string));
                dtWorkExp.Columns.Add("Location", typeof(string));
                dtWorkExp.Columns.Add("From", typeof(string));

                // retrieve certification data from t_emp_certification
                String selectWorkExp = "select * from t_emp_work_experience where emp_id = @employee_id";
                conn.Open();
                SqlCommand WorkExp_cmd = new SqlCommand(selectWorkExp, conn);
                WorkExp_cmd.Parameters.AddWithValue("@employee_id", empID);
                SqlDataReader WorkExp_reader = WorkExp_cmd.ExecuteReader();
                int top = 30;
                while (WorkExp_reader.Read())
                {
                    dtWorkExp.Rows.Add(WorkExp_reader[2].ToString(), Convert.ToDateTime(WorkExp_reader[3]).ToString("yyyy/MM"), Convert.ToDateTime(WorkExp_reader[4]).ToString("yyyy/MM"), WorkExp_reader[5].ToString(), WorkExp_reader[6].ToString(), "Database");
                }
                WorkExp_reader.Close();
                conn.Close();

                foreach (DataRow row in dtWorkExp.Rows)
                {
                    Label lblWorkExpNameData = new Label();
                    Label lblWorkExpYearData = new Label();
                    Label lblWorkExpCompanyAddrData = new Label();
                    string WorkExpYearFrm = row[1].ToString();
                    string WorkExpYearTo = row[2].ToString();

                    lblWorkExpNameData.AutoSize = true;
                    lblWorkExpNameData.Location = new System.Drawing.Point(lblWorkExp.Right + 65, top);
                    lblWorkExpNameData.Name = "lblWorkExpNameData";
                    lblWorkExpNameData.Size = new System.Drawing.Size(133, 22);
                    lblWorkExpNameData.Text = row[0].ToString();

                    lblWorkExpYearData.AutoSize = true;
                    lblWorkExpYearData.Location = new System.Drawing.Point(lblWorkExpNameData.Right + 105, top);
                    lblWorkExpYearData.Name = "lblCertificateYearData";
                    lblWorkExpYearData.Size = new System.Drawing.Size(54, 22);
                    lblWorkExpYearData.Text = Convert.ToDateTime(WorkExpYearFrm).ToString("yyyy/MM") + " - " + Convert.ToDateTime(WorkExpYearTo).ToString("yyyy/MM");

                    lblWorkExpCompanyAddrData.AutoSize = true;
                    lblWorkExpCompanyAddrData.Location = new System.Drawing.Point(lblWorkExpYearData.Right + 200, top);
                    lblWorkExpCompanyAddrData.Name = "lblCertificateYearData";
                    lblWorkExpCompanyAddrData.Size = new System.Drawing.Size(54, 22);
                    lblWorkExpCompanyAddrData.Text = row[3].ToString() + ", " + row[4].ToString();

                    pWorkExp.Controls.Add(lblWorkExpNameData);
                    pWorkExp.Controls.Add(lblWorkExpYearData);
                    pWorkExp.Controls.Add(lblWorkExpCompanyAddrData);
                    top = lblWorkExpNameData.Bottom + 10;
                }
                pWorkExp.AutoSize = true;
                pWorkExp.AutoSizeMode = AutoSizeMode.GrowOnly;
                pAddWorkExp.Top = pWorkExp.Bottom + 10;
                gpBoxWorkExp.Top = gpBoxEduBackground.Bottom + 20;
                btnRegister.Top = gpBoxWorkExp.Bottom + 20;

            }

        }
        public void update_EmployeeInfo()
        {
            int genderInsert = 0;

            string updateEmpInfo = @"update t_emp_basic_info set emp_name=@emp_name, emp_role=@emp_role, emp_dob=@emp_dob, emp_nrc=@emp_nrc, emp_gender=@emp_gender,
                                   emp_phone=@emp_phone ,emp_email=@emp_email, emp_father=@emp_father, emp_perm_addr=@emp_perm_addr, emp_temp_addr=@emp_temp_addr,
                                   emp_emg_name=@emp_emg_name, emp_emg_contact=@emp_emg_contact, emp_emg_contact_relation=@emp_emg_contact_relation,emp_education=@emp_education,
                                   emp_photo_name=@emp_photo_name where emp_id=@emp_id";
            conn.Open();
            SqlCommand updateEmpInfoCmd = new SqlCommand(updateEmpInfo, conn);
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_name", txtEmpName.Text.Trim());

            if (ddRole.Items[ddRole.SelectedIndex].ToString().Equals("Administrator"))
            {
                updateEmpInfoCmd.Parameters.AddWithValue("@emp_role", 1);
            }
            else if (ddRole.Items[ddRole.SelectedIndex].ToString().Equals("Staff"))
            {
                updateEmpInfoCmd.Parameters.AddWithValue("@emp_role", 2);
            }
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_dob", dtpDob.Text);
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_nrc", txtNrcNo.Text.Trim());
            if (rdoMale.Checked == true)
            {
                genderInsert = 1;
            }
            else if (rdoFemale.Checked == true)
            {
                genderInsert = 2;
            }
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_gender", genderInsert);
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_father", txtFatherName.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_phone", int.Parse(txtPhone.Text.Substring(2))); //remove first "09" to insert to database
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_email", txtEmail.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_perm_addr", txtPermanentAddr.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_temp_addr", txtTemporaryAddr.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_emg_name", txtEmergencyContactName.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_emg_contact", int.Parse(txtEmergencyContactNo.Text.Substring(2)));//remove first "09" to insert to database
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_emg_contact_relation", txtRelation.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_education", txtEducation.Text.Trim());
            updateEmpInfoCmd.Parameters.AddWithValue("@emp_photo_name", txtEmpId.Text.Trim());
            updateEmpInfoCmd.ExecuteNonQuery();
            conn.Close();
        }
        public void update_Certification()
        {
            if (certificationClicked == true)
            {
                conn.Open();
                foreach (DataRow row in dtCertification.Rows)
                {
                    if (row[3].Equals("btnClicked"))
                    {
                        string updateEmpCertificate = "insert into t_emp_certification (emp_id, emp_cert_name, emp_cert_from ,emp_cert_to) values (@emp_id, @emp_cert_name, @emp_cert_from ,@emp_cert_to)";
                        SqlCommand cmd = new SqlCommand(updateEmpCertificate, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_cert_name", row[0].ToString());
                        cmd.Parameters.AddWithValue("@emp_cert_from", Convert.ToDateTime(row[1]));
                        cmd.Parameters.AddWithValue("@emp_cert_to", Convert.ToDateTime(row[2]));
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
        }
        public void update_JpskillData()
        {
            if (jpskillClicked == true)
            {
                conn.Open();
                foreach (DataRow row in dtJpskill.Rows)
                {
                    if (row[3].Equals("btnClicked"))
                    {
                        string updateEmpJpSkill = "insert into t_emp_jpkill (emp_id,emp_jpskill_level_name,emp_jpskill_bandscore,emp_jpskill_certified_date,emp_jpskill_maxlevel) " +
                                                  "values (@emp_id,@emp_jplevel,@emp_jpscore,@emp_jpdate,@emp_jpmax) ";

                        SqlCommand cmd = new SqlCommand(updateEmpJpSkill, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jplevel", row[0].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jpscore", row[1].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jpdate", Convert.ToDateTime(row[2]));
                        cmd.Parameters.AddWithValue("@emp_jpmax", 0);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
        }
        public void updateWorkExpData()
        {
            if (workexpClicked == true)
            {
                conn.Open();
                foreach (DataRow row in dtWorkExp.Rows)
                {
                    if (row[5].Equals("btnClicked"))
                    {
                        string updateEmpJpSkill = "insert into t_emp_work_experience(emp_id,emp_position,emp_comp_from,emp_comp_to,emp_comp_name,emp_comp_location) " +
                                                  "values (@emp_id,@emp_position,@emp_from,@emp_to,@emp_compname,@emp_location) ";

                        SqlCommand cmd = new SqlCommand(updateEmpJpSkill, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_position", row[0].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_from", Convert.ToDateTime(row[1]));
                        cmd.Parameters.AddWithValue("@emp_to", Convert.ToDateTime(row[2]));
                        cmd.Parameters.AddWithValue("@emp_compname", row[3].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_location", row[4].ToString().Trim());
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
        }
        public void insert_EmployeeInfo()
        {
            try
            {
                int genderState = 0;
                int role = 0;
                string selectedRole = ddRole.SelectedItem.ToString();
                string encryptedText = Encrypt("password1234", encryptionKey);

                string registerEmpInfo = "insert into t_emp_basic_info (emp_id,emp_name,emp_password,emp_role,emp_dob,emp_nrc,emp_gender,emp_father,emp_phone,emp_email," +
                                         "emp_perm_addr,emp_temp_addr,emp_emg_name,emp_emg_contact,emp_emg_contact_relation,emp_education,emp_photo_name) " +
                                         "values (@emp_id,@emp_name,@emp_password,@emp_role,@emp_dob,@emp_nrc,@emp_gender,@emp_father,@emp_phone,@emp_email," +
                                         "@emp_perm_addr,@emp_temp_addr,@emp_eng_name,@emp_eng_contact,@emp_eng_contact_relation,@emp_education,@emp_photo_name)";
                conn.Open();
                SqlCommand cmd = new SqlCommand(registerEmpInfo, conn);
                cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_name", txtEmpName.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_password", encryptedText);

                if (selectedRole.Equals("Administrator"))
                {
                    role = 1;
                }
                else if (selectedRole.Equals("Staff"))
                {
                    role = 2;
                }
                cmd.Parameters.AddWithValue("@emp_role", role);
                cmd.Parameters.AddWithValue("@emp_dob", Convert.ToDateTime(dtpDob.Text));
                cmd.Parameters.AddWithValue("@emp_nrc", txtNrcNo.Text.ToString().Trim());

                if (rdoMale.Checked == true)
                {
                    genderState = 1;
                }
                else if (rdoFemale.Checked == true)
                {
                    genderState = 2;
                }
                cmd.Parameters.AddWithValue("@emp_gender", genderState);
                cmd.Parameters.AddWithValue("@emp_father", txtFatherName.Text.ToString().Trim());
             
                // remove the first two digit '09' from phone number
                cmd.Parameters.AddWithValue("@emp_phone", int.Parse(txtPhone.Text.Substring(2)));
                cmd.Parameters.AddWithValue("@emp_email", txtEmail.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_perm_addr", txtPermanentAddr.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_temp_addr", txtTemporaryAddr.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_eng_name", txtEmergencyContactName.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_eng_contact", int.Parse(txtEmergencyContactNo.Text.Substring(2)));
                cmd.Parameters.AddWithValue("@emp_eng_contact_relation", txtRelation.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_education", txtEducation.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@emp_photo_name", txtEmpId.Text.ToString().Trim());

                cmd.ExecuteNonQuery();
                conn.Close();
            }catch(Exception ex)
            {
                MessageBox.Show("Exception Error: " + ex.Message);
            }
            
        }
        public void insert_Jpskill()
        {
            try
            {
                conn.Open();
                foreach (DataRow row in dtJpskill.Rows)
                {
                    if (row[3].Equals("btnClicked"))
                    {
                        string updateEmpJpSkill = "insert into t_emp_jpkill (emp_id,emp_jpskill_level_name,emp_jpskill_bandscore,emp_jpskill_certified_date,emp_jpskill_maxlevel) " +
                                                  "values (@emp_id,@emp_jplevel,@emp_jpscore,@emp_jpdate,@emp_jpmax) ";

                        SqlCommand cmd = new SqlCommand(updateEmpJpSkill, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jplevel", row[0].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jpscore", row[1].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_jpdate", Convert.ToDateTime(row[2]));

                        // insert japan max level as 0 first time
                        cmd.Parameters.AddWithValue("@emp_jpmax", 0);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }catch (Exception ex)
            {
                MessageBox.Show("Exception Error: " + ex.Message);
            }
            
        }
        public void insert_CertificateList()
        {
            try
            {
                conn.Open();
                foreach (DataRow row in dtCertification.Rows)
                {
                    if (row[3].Equals("btnClicked"))
                    {
                        string updateEmpCertificate = "insert into t_emp_certification (emp_id, emp_cert_name, emp_cert_from ,emp_cert_to) values (@emp_id, @emp_cert_name, @emp_cert_from ,@emp_cert_to)";
                        SqlCommand cmd = new SqlCommand(updateEmpCertificate, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_cert_name", row[0].ToString());
                        cmd.Parameters.AddWithValue("@emp_cert_from", Convert.ToDateTime(row[1]));
                        cmd.Parameters.AddWithValue("@emp_cert_to", Convert.ToDateTime(row[2]));
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }catch(Exception ex)
            {
                MessageBox.Show("Exception Error: " + ex.Message);
            }
            
        }
        public void insert_WorkExp()
        {
            try
            {
                conn.Open();
                foreach (DataRow row in dtWorkExp.Rows)
                {
                    if (row[5].Equals("btnClicked"))
                    {
                        string updateEmpJpSkill = "insert into t_emp_work_experience(emp_id,emp_position,emp_comp_from,emp_comp_to,emp_comp_name,emp_comp_location) " +
                                                  "values (@emp_id,@emp_position,@emp_from,@emp_to,@emp_compname,@emp_location) ";

                        SqlCommand cmd = new SqlCommand(updateEmpJpSkill, conn);
                        cmd.Parameters.AddWithValue("@emp_id", txtEmpId.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_position", row[0].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_from", Convert.ToDateTime(row[1]));
                        cmd.Parameters.AddWithValue("@emp_to", Convert.ToDateTime(row[2]));
                        cmd.Parameters.AddWithValue("@emp_compname", row[3].ToString().Trim());
                        cmd.Parameters.AddWithValue("@emp_location", row[4].ToString().Trim());
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }catch(Exception ex)
            {
                MessageBox.Show("Exception Error: " + ex.Message);
            }
            
        }
        /// <summary>
        /// checkRadioBtn method checks which radio button on pJpLevelList is selected
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        private int checkRadioBtn(Control container)
        {
            // checked radio button
            int rdoBtnCount = 0;

            // check which radio button is checked ?
            foreach (Control ctrl in container.Controls)
            {
                if (ctrl is RadioButton)
                {
                    RadioButton radio = ctrl as RadioButton;

                    if (!radio.Checked)
                    {
                        rdoBtnCount += 1;
                    }
                    else
                    {
                        rdoBtnCount += 1;
                        break;
                    }
                }
            } // end of forloop
            return rdoBtnCount;
        }
        private void btnAddJpCertificate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtJpLevel.Text.ToString()))
                {
                    MessageBox.Show("Please fill japanese level");
                    txtJpLevel.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtJpScore.Text.ToString()))
                {
                    MessageBox.Show("Please fill japanese language level score/band");
                    txtJpScore.Focus();
                    return;
                }
                if (dtpJpYear.Value > DateTime.Now)
                {
                    MessageBox.Show("Japanese certificate year must be less than current date");
                    return;
                }

                int top = 17;
                int sameName = 0;

                foreach (DataRow row in dtJpskill.Rows)
                {
                    if (row[0].Equals(txtJpLevel.Text.ToString().Trim()))
                    {
                        sameName++;
                        break;
                    }
                }
                if (sameName > 0)
                {
                    MessageBox.Show("This level already exist");
                }
                else
                {
                    jpskillClicked = true;
                    pJpLevelList.Controls.Clear();
                    dtJpskill.Rows.Add(txtJpLevel.Text.ToString().Trim(), txtJpScore.Text.ToString().Trim(), Convert.ToDateTime(dtpJpYear.Value).ToString("yyyy/MM"), "btnClicked", 0);
                    foreach (DataRow row in dtJpskill.Rows)
                    {
                        // declare label names
                        Label lblJpLevelData = new Label();
                        Label lblJpScore = new Label();
                        Label lblJpYear = new Label();
                        RadioButton rdoJpDefaultData = new RadioButton();
                        // properties for lblJpLevelData
                        lblJpLevelData.AutoSize = true;
                        lblJpLevelData.Location = new System.Drawing.Point(7, top);
                        lblJpLevelData.Name = "lblJpLevelData";
                        lblJpLevelData.Size = new System.Drawing.Size(54, 22);
                        lblJpLevelData.Text = row[0].ToString();
                        // properties for Japan Score
                        lblJpScore.AutoSize = true;
                        lblJpScore.Location = new System.Drawing.Point(lblJpLevelData.Right + 130, top);
                        lblJpScore.Name = "lblJpScore";
                        lblJpScore.Size = new System.Drawing.Size(54, 22);
                        lblJpScore.Text = row[1].ToString();
                        // properties for Jp Year
                        lblJpYear.AutoSize = true;
                        lblJpYear.Location = new System.Drawing.Point(lblJpScore.Right + 100, top);
                        lblJpYear.Name = "lblJpYear";
                        lblJpYear.Size = new System.Drawing.Size(54, 22);
                        lblJpYear.Text = Convert.ToDateTime(row[2]).ToString("yyyy/MM");

                        if (Convert.ToInt32(row[4]) == 1)
                        {
                            rdoJpDefaultData.Checked = true;
                        }
                        else if (Convert.ToInt32(row[4]) == 0)
                        {
                            rdoJpDefaultData.Checked = false;
                        }
                        // properties for radio button max level
                        rdoJpDefaultData.AutoSize = true;
                        rdoJpDefaultData.Location = new System.Drawing.Point(lblJpYear.Right + 120, top);
                        rdoJpDefaultData.Name = "rdoJpDefaultData";
                        rdoJpDefaultData.Size = new System.Drawing.Size(129, 26);                     
                        rdoJpDefaultData.Text = "set as default";
                        rdoJpDefaultData.UseVisualStyleBackColor = true;


                        pJpLevelList.Controls.Add(lblJpLevelData);
                        pJpLevelList.Controls.Add(lblJpScore);
                        pJpLevelList.Controls.Add(lblJpYear);
                        pJpLevelList.Controls.Add(rdoJpDefaultData);
                        top = lblJpLevelData.Bottom + 10;
                    }
                    // set all textboxes clear and datetimepicker to today date after every button clicked
                    txtJpLevel.Clear();
                    txtJpScore.Clear();
                    dtpJpYear.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");

                    pJpLevelList.AutoSize = true;
                    pAddEducation.Top = pJpLevelList.Bottom + 10;

                    pCertificationList.Top = pAddEducation.Bottom + 10;
                    pAddCertification.Top = pCertificationList.Bottom + 10;
                    gpBoxWorkExp.Top = gpBoxEduBackground.Bottom + 20;
                    btnRegister.Top = gpBoxWorkExp.Bottom + 20;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnAddCertificate_Click(object sender, EventArgs e)
        {
            try
            {
                int top = 20;
                int sameName = 0;

                if (string.IsNullOrEmpty(txtCertificate.Text))
                {
                    MessageBox.Show("Please fill certification");
                    txtCertificate.Focus();
                    return;
                }
                if (dtpCertificateFrm.Value >= dtpCertificateTo.Value)
                {
                    MessageBox.Show("Certificate date from must be less than certificate date to");
                    return;
                }
                if (dtpCertificateFrm.Value >= DateTime.Now)
                {
                    MessageBox.Show("Certificate date from must be less than or equal current date");
                    return;
                }
                if (dtpCertificateTo.Value >= DateTime.Now)
                {
                    MessageBox.Show("Certificate date to must be less than or equal current date");
                    return;
                }

                foreach (DataRow row in dtCertification.Rows)
                {
                    if (row[0].Equals(txtCertificate.Text.ToString().Trim()))
                    {
                        sameName = 1;
                    }
                }
                if (sameName > 0)
                {
                    MessageBox.Show("This certificate already exists!");
                }
                else
                {
                    certificationClicked = true;
                    pCertificationList.Controls.Clear();
                    dtCertification.Rows.Add(txtCertificate.Text.ToString().Trim(), Convert.ToDateTime(dtpCertificateFrm.Value).ToString("yyyy/MM"), 
                                             Convert.ToDateTime(dtpCertificateTo.Value).ToString("yyyy/MM"), "btnClicked");

                    foreach (DataRow row in dtCertification.Rows)
                    {
                        Label lblCertificateData = new Label();
                        Label lblCertificateYearData = new Label();
                        // properties for label certificate name
                        lblCertificateData.AutoSize = true;
                        lblCertificateData.Location = new System.Drawing.Point(185, top);
                        lblCertificateData.Name = "lblCertificateData";
                        lblCertificateData.Size = new System.Drawing.Size(54, 22);
                        lblCertificateData.Text = row[0].ToString();
                        // properties for label certificate year range
                        lblCertificateYearData.AutoSize = true;
                        lblCertificateYearData.Location = new System.Drawing.Point(lblCertificateData.Right + 180, top);
                        lblCertificateYearData.Name = "lblCertificateYearData";
                        lblCertificateYearData.Size = new System.Drawing.Size(54, 22);
                        lblCertificateYearData.Text = (row[1].ToString() + " - " + row[2].ToString());

                        pCertificationList.Controls.Add(lblCertificateData);
                        pCertificationList.Controls.Add(lblCertificateYearData);
                        top = lblCertificateData.Bottom + 10;
                    }
                    txtCertificate.Clear(); //clear textbox after input
                    dtpCertificateFrm.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
                    dtpCertificateTo.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
                    // Create label Certification after panel clear
                    Label lblCertificate = new Label();
                    lblCertificate.AutoSize = true;
                    lblCertificate.Location = new System.Drawing.Point(4, 18);
                    lblCertificate.Name = "lblCertificate";
                    lblCertificate.Size = new System.Drawing.Size(101, 22);
                    lblCertificate.Text = "Certification";

                    pCertificationList.Controls.Add(lblCertificate);

                    pCertificationList.AutoSize = true;
                    pCertificationList.AutoSizeMode = AutoSizeMode.GrowOnly;
                    pAddCertification.Top = pCertificationList.Bottom + 10;
                    gpBoxWorkExp.Top = gpBoxEduBackground.Bottom + 20;
                    btnRegister.Top = gpBoxWorkExp.Bottom + 20;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void btnAddExp_Click(object sender, EventArgs e)
        {
            try
            {
                workexpClicked = true;
                DateTime dWorkFrm = dtpWorkExpFrm.Value;
                DateTime dWorkTo = dtpWorkExpTo.Value;
                // require checked and error message
                if (String.IsNullOrEmpty(txtWorkExpName.Text))
                {
                    MessageBox.Show(" Please fill work experience");
                    txtWorkExpName.Focus();
                    return;
                }
                if (dWorkFrm >= dWorkTo)
                {
                    MessageBox.Show("Experience date from must be less than experience date to");
                    return;
                }
                if (dtpWorkExpTo.Value > DateTime.Now)
                {
                    MessageBox.Show("Experience date to must be less than or equal current date");
                    return;
                }
                if (string.IsNullOrEmpty(txtWorkExpCompanyName.Text))
                {
                    MessageBox.Show(" Please fill company name");
                    txtWorkExpCompanyName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtWorkExpLocation.Text))
                {
                    MessageBox.Show(" Please fill location");
                    txtWorkExpLocation.Focus();
                    return;
                }
                pWorkExp.Controls.Clear();
                dtWorkExp.Rows.Add(txtWorkExpName.Text.ToString(), Convert.ToDateTime(dtpWorkExpFrm.Value).ToString("yyyy/MM"),
                                   Convert.ToDateTime(dtpWorkExpTo.Value).ToString("yyyy/MM"), txtWorkExpCompanyName.Text.ToString().Trim(),
                                   txtWorkExpLocation.Text.ToString().Trim(), "btnClicked");
                int top = 30;
                foreach (DataRow row in dtWorkExp.Rows)
                {
                    Label lblWorkExpNameData = new Label();
                    Label lblWorkExpYearData = new Label();
                    Label lblWorkExpCompanyAddrData = new Label();
                    string WorkExpYearFrm = row[1].ToString();
                    string WorkExpYearTo = row[2].ToString();

                    // properties for label Work Experience Name
                    lblWorkExpNameData.AutoSize = true;
                    lblWorkExpNameData.Location = new System.Drawing.Point(lblWorkExp.Right + 65, top);
                    lblWorkExpNameData.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
                    lblWorkExpNameData.Name = "lblWorkExpNameData";
                    lblWorkExpNameData.Size = new System.Drawing.Size(133, 22);
                    lblWorkExpNameData.Text = row[0].ToString();

                    // properties for label Work Exp Year Range
                    lblWorkExpYearData.AutoSize = true;
                    lblWorkExpYearData.Location = new System.Drawing.Point(lblWorkExpNameData.Right + 105, top);
                    lblWorkExpYearData.Name = "lblCertificateYearData";
                    lblWorkExpYearData.Size = new System.Drawing.Size(54, 22);
                    lblWorkExpYearData.Text = Convert.ToDateTime(WorkExpYearFrm).ToString("yyyy/MM") + " - " + Convert.ToDateTime(WorkExpYearTo).ToString("yyyy/MM");
                    
                    // properties for label Company location and name
                    lblWorkExpCompanyAddrData.AutoSize = true;
                    lblWorkExpCompanyAddrData.Location = new System.Drawing.Point(lblWorkExpYearData.Right + 200, top);
                    lblWorkExpCompanyAddrData.Name = "lblCertificateYearData";
                    lblWorkExpCompanyAddrData.Size = new System.Drawing.Size(54, 22);
                    lblWorkExpCompanyAddrData.Text = row[3].ToString() + ", " + row[4].ToString();

                    pWorkExp.Controls.Add(lblWorkExpNameData);
                    pWorkExp.Controls.Add(lblWorkExpYearData);
                    pWorkExp.Controls.Add(lblWorkExpCompanyAddrData);
                    top = lblWorkExpNameData.Bottom + 10;
                }
                txtWorkExpCompanyName.Clear();
                txtWorkExpLocation.Clear();
                txtWorkExpName.Clear();
                dtpWorkExpFrm.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
                dtpWorkExpTo.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM");
                
                // add label control to WorkExp panel
                pWorkExp.Controls.Add(lblWorkExp);
                pWorkExp.AutoSize = true;
                pWorkExp.AutoSizeMode = AutoSizeMode.GrowOnly;
                pAddWorkExp.Top = pWorkExp.Bottom + 10;
                btnRegister.Top = gpBoxWorkExp.Bottom + 20;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnUploadImg_Click(object sender, EventArgs e)
        {
            // clear picture box and add new profile picture
            if (imgBox.Image != null)
            {
                imgBox.Image.Dispose();
            }

            // open file dialog
            OpenFileDialog fileDialog = new OpenFileDialog();

            // initial directory you want to see when open file dialog
            fileDialog.InitialDirectory = "C:\\Users\\T2023007\\Pictures";

            // filter to show only these types of images
            fileDialog.Filter = "Image Only(*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                // getting (Selected Image Path)
                string imagePath = fileDialog.FileName;

                // getting (Image Name With ID)
                string imageName = txtEmpId.Text.Trim().ToString();

                // getting (Selected Image Info)
                FileInfo imageInfo = new FileInfo(imagePath);

                // getting (Selected Image Extension)
                string imageExtension = imageInfo.Extension;

                // create a local path where profile pictures are stored
                string localPath = @"C:\Users\T2023007\Desktop\Attendance Management System\temp\";
                
                // get every images with same name with employee ID from local path
                string[] imageWithSameName = Directory.GetFiles(localPath, $"{imageName}.*");

                // check image size is less than 1 mb or not
                if (Check_imgsize(imagePath, 1024) == false)
                {
                    MessageBox.Show("Image size must be less than 1mb");
                    return;
                }
                else
                {
                    // if image with same name exist, delete them and add new incoming image
                    if (imageWithSameName.Length > 0)
                    {
                        try
                        {
                            // deleting image with same name
                            File.Delete(imageWithSameName[0]);

                            // adding new selected image to folder
                            File.Copy(imagePath, localPath + imageName + imageExtension, true);

                            // set creation time to image
                            File.SetCreationTimeUtc(localPath + imageName + imageExtension, DateTime.Now);

                            // adding image to picture box
                            imgBox.Image = new Bitmap(imagePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        // adding new selected image to folder
                        File.Copy(imagePath, localPath + imageName + imageExtension, true);

                        // set creation time to image
                        File.SetCreationTimeUtc(localPath + imageName + imageExtension, DateTime.Now);

                        // adding image to picture box
                        imgBox.Image = new Bitmap(imagePath);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please attach image");
            }

        }
        private void btnRegister_Click(object sender, EventArgs e)
        {
            
            // check which radio button is checked in Japan skill panel
            int rdoCount = checkRadioBtn(pJpLevelList);
            // check button name for process Update or Register
            // Employee registration process
            if (btnRegister.Text == "Register")
            {
                Connection();
                string selectEmpData = @"select * from t_emp_basic_info where emp_id = @employee_id";
                conn.Open();
                SqlCommand cmd = new SqlCommand(selectEmpData, conn);
                cmd.Parameters.AddWithValue("@employee_id", txtEmpId.Text.Trim());
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var employee_ID = reader[1].ToString();

                    if (employee_ID.Equals(txtEmpId.Text.ToString().Trim()))
                    {
                        MessageBox.Show("Employee Id already exists");
                        txtEmpId.Clear();
                        txtEmpId.Focus();
                        return;
                    }
                }
                reader.Close();
                conn.Close();

                if (errorChecking() == true)
                {
                    return;
                }
                else
                {
                    insert_EmployeeInfo();
                    if (dtJpskill.Rows.Count > 0)
                    {
                        insert_Jpskill();
                        if (rdoCount != 0)
                        {
                            updateJPMaxLevel(rdoCount);
                        }
                    }
                    if (dtCertification.Rows.Count > 0)
                    {
                        insert_CertificateList();
                    }
                    if (dtWorkExp.Rows.Count > 0)
                    {
                        insert_WorkExp();
                    }
                    MessageBox.Show("New employee registration successfully", "Successful Registration", MessageBoxButtons.OK);
                    EmployeeList EmpList = new EmployeeList();
                    EmpList.Show();
                    this.Close();
                }
            }
            else if (btnRegister.Text == "Update")
            {
                if (errorChecking() == true)
                {
                    return;
                }
                else
                {
                    update_EmployeeInfo();
                    update_JpskillData();
                    if (rdoCount != 0)
                    {
                        updateJPMaxLevel(rdoCount);
                    }
                    update_Certification();
                    updateWorkExpData();

                    MessageBox.Show("Employee data is updated successfully", "Successful Update", MessageBoxButtons.OK);

                    // refresh database with updated data
                    EmployeeList EmpList = new EmployeeList();
                    EmpList.Show();
                    this.Close();
                }
            }

        }
        private void EmployeeRegistrationForm_Load(object sender, EventArgs e)
        {
            Connection();
            dateTimeFormat();
            // panel for the whole form to fix the size in runtime
            pForAll.AutoSize = true;
            pForAll.AutoSizeMode = AutoSizeMode.GrowOnly;

            // if (callFrom == "edit_Employee") --> form load with database data for employee id.
            // if (callFrom == "add_Employee") --> form load with blank input box.
            if(call_Status.Equals("Edit"))
            {

                btnRegister.Text = "Update";
                try
                {
                    load_BasicInfo();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    LoadJpSkillData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    LoadCertification_List();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    LoadWorkExpData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (call_Status.Equals("Add Employee"))
            {
                btnRegister.Text = "Register";

                register_allBlank();
                try
                {
                    LoadJpSkillData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    LoadCertification_List();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    LoadWorkExpData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}

