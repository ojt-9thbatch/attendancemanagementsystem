﻿namespace Attendance_Management_System
{
    partial class LeaveTypeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvLeaveTypeList = new System.Windows.Forms.DataGridView();
            this.btnAddLeaveType = new System.Windows.Forms.Button();
            this.lblNodata = new System.Windows.Forms.Label();
            this.leavetype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.days = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leaveID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StaffPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveTypeList)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLeaveTypeList
            // 
            this.dgvLeaveTypeList.AllowUserToAddRows = false;
            this.dgvLeaveTypeList.AllowUserToDeleteRows = false;
            this.dgvLeaveTypeList.AllowUserToResizeColumns = false;
            this.dgvLeaveTypeList.AllowUserToResizeRows = false;
            this.dgvLeaveTypeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLeaveTypeList.BackgroundColor = System.Drawing.Color.White;
            this.dgvLeaveTypeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLeaveTypeList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLeaveTypeList.ColumnHeadersHeight = 29;
            this.dgvLeaveTypeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.leavetype,
            this.days,
            this.Desc,
            this.leaveID});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLeaveTypeList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLeaveTypeList.EnableHeadersVisualStyles = false;
            this.dgvLeaveTypeList.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvLeaveTypeList.Location = new System.Drawing.Point(92, 287);
            this.dgvLeaveTypeList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 70);
            this.dgvLeaveTypeList.Name = "dgvLeaveTypeList";
            this.dgvLeaveTypeList.ReadOnly = true;
            this.dgvLeaveTypeList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvLeaveTypeList.RowHeadersVisible = false;
            this.dgvLeaveTypeList.RowHeadersWidth = 51;
            this.dgvLeaveTypeList.RowTemplate.Height = 35;
            this.dgvLeaveTypeList.RowTemplate.ReadOnly = true;
            this.dgvLeaveTypeList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLeaveTypeList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvLeaveTypeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvLeaveTypeList.Size = new System.Drawing.Size(1257, 309);
            this.dgvLeaveTypeList.TabIndex = 0;
            this.dgvLeaveTypeList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeaveTypeList_CellContentClick);
            this.dgvLeaveTypeList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvLeaveTypeList_CellPainting);
            this.dgvLeaveTypeList.Paint += new System.Windows.Forms.PaintEventHandler(this.dgvLeaveTypeList_Paint);
            // 
            // btnAddLeaveType
            // 
            this.btnAddLeaveType.BackColor = System.Drawing.Color.DeepPink;
            this.btnAddLeaveType.FlatAppearance.BorderSize = 0;
            this.btnAddLeaveType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddLeaveType.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddLeaveType.ForeColor = System.Drawing.Color.White;
            this.btnAddLeaveType.Location = new System.Drawing.Point(1176, 202);
            this.btnAddLeaveType.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddLeaveType.Name = "btnAddLeaveType";
            this.btnAddLeaveType.Size = new System.Drawing.Size(173, 46);
            this.btnAddLeaveType.TabIndex = 2;
            this.btnAddLeaveType.Text = "+ Add Leave Type";
            this.btnAddLeaveType.UseVisualStyleBackColor = false;
            this.btnAddLeaveType.Click += new System.EventHandler(this.btnAddLeaveType_Click);
            // 
            // lblNodata
            // 
            this.lblNodata.AutoSize = true;
            this.lblNodata.Location = new System.Drawing.Point(556, 348);
            this.lblNodata.Name = "lblNodata";
            this.lblNodata.Size = new System.Drawing.Size(180, 16);
            this.lblNodata.TabIndex = 8;
            this.lblNodata.Text = "No leave type data available";
            // 
            // leavetype
            // 
            this.leavetype.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.leavetype.HeaderText = "Leave Type";
            this.leavetype.MinimumWidth = 6;
            this.leavetype.Name = "leavetype";
            this.leavetype.ReadOnly = true;
            this.leavetype.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.leavetype.Width = 200;
            // 
            // days
            // 
            this.days.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.days.HeaderText = "Days";
            this.days.MinimumWidth = 6;
            this.days.Name = "days";
            this.days.ReadOnly = true;
            this.days.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.days.Width = 125;
            // 
            // Desc
            // 
            this.Desc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Desc.HeaderText = "Description";
            this.Desc.MinimumWidth = 6;
            this.Desc.Name = "Desc";
            this.Desc.ReadOnly = true;
            this.Desc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Desc.Width = 400;
            // 
            // leaveID
            // 
            this.leaveID.HeaderText = "Leave ID";
            this.leaveID.MinimumWidth = 6;
            this.leaveID.Name = "leaveID";
            this.leaveID.ReadOnly = true;
            this.leaveID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.leaveID.Visible = false;
            // 
            // LeaveTypeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1481, 667);
            this.Controls.Add(this.dgvLeaveTypeList);
            this.Controls.Add(this.lblNodata);
            this.Controls.Add(this.btnAddLeaveType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.Name = "LeaveTypeList";
            this.Text = "LeaveTypeList";
            this.Load += new System.EventHandler(this.LeaveTypeList_Load);
            this.Controls.SetChildIndex(this.StaffPanel, 0);
            this.Controls.SetChildIndex(this.btnAddLeaveType, 0);
            this.Controls.SetChildIndex(this.lblNodata, 0);
            this.Controls.SetChildIndex(this.dgvLeaveTypeList, 0);
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveTypeList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLeaveTypeList;
        private System.Windows.Forms.Button btnAddLeaveType;
        private System.Windows.Forms.Label lblNodata;
        private System.Windows.Forms.DataGridViewTextBoxColumn leavetype;
        private System.Windows.Forms.DataGridViewTextBoxColumn days;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn leaveID;
    }
}