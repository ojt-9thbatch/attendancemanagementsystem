﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class MenuBar : Form
    {
        int role = 0;
        private Button btn = null;
        public MenuBar()
        {
            InitializeComponent();
        }
        protected void BtnSetting(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackColor = Color.FromArgb(151, 242, 0);
            if (btn != null)
            {
                btn.BackColor = Color.LightSkyBlue;
            }
            btn = button;
        }
        private void btnHoliday_Click(object sender, EventArgs e)
        {
            BtnSetting(btnHoliday, null);
            btnHoliday.BackColor = Color.FromArgb(151, 242, 0);
            HolidayList Holiday = new HolidayList();
            Holiday.Show();
            this.Close();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            BtnSetting(btnEmployee, null);
            btnEmployee.BackColor = Color.FromArgb(151, 242, 0);
            EmployeeList Employee = new EmployeeList();
            Employee.Show();
            this.Close();
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            BtnSetting (btnLeave, null);
            btnLeave.BackColor = Color.FromArgb(151, 242, 0);
            LeaveTypeList LeaveList = new LeaveTypeList();
            LeaveList.Show();
            this.Close();
        }
        private void btnMyAccount_Click(object sender, EventArgs e)
        {
            BtnSetting(btnMyAccount, null);
            btnMyAccount.BackColor = Color.FromArgb(151, 242, 0);
            Profile AdminProfile = new Profile();
            AdminProfile.Show();
            this.Close();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            BtnSetting(btnChangePassword, null);
            btnChangePassword.BackColor = Color.FromArgb(151, 242, 0);
            ChangePassword AdminPassword = new ChangePassword();
            AdminPassword.Show();
            this.Close();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            BtnSetting(btnLogOut, null);
            btnLogOut.BackColor = Color.FromArgb(151, 242, 0);
            Login LoginForm = new Login();
            LoginForm.Show();
            this.Close();
        }
        private void MenuBar_Load(object sender, EventArgs e)
        {   
            role = Login.role;
            if (role == 1)
            {
                lblAdminName.Text = Login.employeeName;
                AdminPanel.BringToFront();
                StaffPanel.Hide();
            }
            else if (role == 2)
            {
                lblStaffName.Text = Login.employeeName;
                StaffPanel.BringToFront();
                AdminPanel.Hide();
            }
        }

        private void btnStaffProfile_Click(object sender, EventArgs e)
        {
            BtnSetting(btnStaffProfile, null);
            btnStaffProfile.BackColor = Color.FromArgb(151, 242, 0);
            Profile ProfileForm = new Profile();
            ProfileForm.Show();
            this.Close();
        }

        private void btnStaffPass_Click(object sender, EventArgs e)
        {
            BtnSetting(btnStaffPass, null);
            btnStaffPass.BackColor = Color.FromArgb(151, 242, 0);
            ChangePassword StaffPass = new ChangePassword();
            StaffPass.Show();
            this.Close();
        }

        private void btnStaffLog_Click(object sender, EventArgs e)
        {
            BtnSetting(btnStaffLog, null);
            btnStaffLog.BackColor = Color.FromArgb(151, 242, 0);
            Login LoginForm = new Login();
            LoginForm.Show();
            this.Close();
        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
            BtnSetting(btnAttendance, null);
            btnAttendance.BackColor = Color.FromArgb(151, 242, 0);
            AttendanceList Attendance = new AttendanceList();
            Attendance.Show();
            this.Close();
        }

    }
}
