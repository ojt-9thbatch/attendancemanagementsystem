﻿namespace Attendance_Management_System
{
    partial class HolidayList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.dgvHolidayList = new System.Windows.Forms.DataGridView();
            this.pPagination = new System.Windows.Forms.Panel();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.StaffPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolidayList)).BeginInit();
            this.pPagination.SuspendLayout();
            this.SuspendLayout();
            // 
            // ddlYear
            // 
            this.ddlYear.BackColor = System.Drawing.Color.White;
            this.ddlYear.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlYear.ForeColor = System.Drawing.Color.Black;
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Items.AddRange(new object[] {
            "2023",
            "2024",
            "2025"});
            this.ddlYear.Location = new System.Drawing.Point(69, 202);
            this.ddlYear.Margin = new System.Windows.Forms.Padding(2);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(128, 27);
            this.ddlYear.TabIndex = 4;
            this.ddlYear.SelectedIndexChanged += new System.EventHandler(this.ddlSearch_SelectedIndexChanged);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYear.Location = new System.Drawing.Point(199, 202);
            this.lblYear.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(81, 19);
            this.lblYear.TabIndex = 5;
            this.lblYear.Text = "Holidays in";
            // 
            // dgvHolidayList
            // 
            this.dgvHolidayList.AllowUserToAddRows = false;
            this.dgvHolidayList.AllowUserToDeleteRows = false;
            this.dgvHolidayList.AllowUserToResizeColumns = false;
            this.dgvHolidayList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvHolidayList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHolidayList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvHolidayList.BackgroundColor = System.Drawing.Color.White;
            this.dgvHolidayList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvHolidayList.ColumnHeadersHeight = 50;
            this.dgvHolidayList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHolidayList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHolidayList.EnableHeadersVisualStyles = false;
            this.dgvHolidayList.Location = new System.Drawing.Point(69, 255);
            this.dgvHolidayList.Margin = new System.Windows.Forms.Padding(2);
            this.dgvHolidayList.Name = "dgvHolidayList";
            this.dgvHolidayList.RowHeadersVisible = false;
            this.dgvHolidayList.RowHeadersWidth = 60;
            this.dgvHolidayList.RowTemplate.Height = 40;
            this.dgvHolidayList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHolidayList.ShowCellErrors = false;
            this.dgvHolidayList.ShowCellToolTips = false;
            this.dgvHolidayList.ShowEditingIcon = false;
            this.dgvHolidayList.ShowRowErrors = false;
            this.dgvHolidayList.Size = new System.Drawing.Size(942, 213);
            this.dgvHolidayList.TabIndex = 8;
            this.dgvHolidayList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHolidayList_CellContentClick);
            this.dgvHolidayList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView_CellPainting);
            this.dgvHolidayList.Paint += new System.Windows.Forms.PaintEventHandler(this.dgvHolidayList_Paint);
            // 
            // pPagination
            // 
            this.pPagination.BackColor = System.Drawing.Color.White;
            this.pPagination.Controls.Add(this.btn_1);
            this.pPagination.Controls.Add(this.btn_7);
            this.pPagination.Controls.Add(this.btn_5);
            this.pPagination.Controls.Add(this.btn_6);
            this.pPagination.Controls.Add(this.btn_2);
            this.pPagination.Controls.Add(this.btn_3);
            this.pPagination.Controls.Add(this.btn_4);
            this.pPagination.Location = new System.Drawing.Point(518, 508);
            this.pPagination.Margin = new System.Windows.Forms.Padding(2);
            this.pPagination.Name = "pPagination";
            this.pPagination.Size = new System.Drawing.Size(542, 39);
            this.pPagination.TabIndex = 11;
            // 
            // btn_1
            // 
            this.btn_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.ForeColor = System.Drawing.Color.White;
            this.btn_1.Location = new System.Drawing.Point(78, 4);
            this.btn_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(56, 31);
            this.btn_1.TabIndex = 0;
            this.btn_1.Text = "<<";
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_7
            // 
            this.btn_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.ForeColor = System.Drawing.Color.White;
            this.btn_7.Location = new System.Drawing.Point(442, 4);
            this.btn_7.Margin = new System.Windows.Forms.Padding(2);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(56, 31);
            this.btn_7.TabIndex = 6;
            this.btn_7.Text = ">>";
            this.btn_7.UseVisualStyleBackColor = false;
            this.btn_7.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_5
            // 
            this.btn_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.ForeColor = System.Drawing.Color.White;
            this.btn_5.Location = new System.Drawing.Point(321, 4);
            this.btn_5.Margin = new System.Windows.Forms.Padding(2);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(56, 31);
            this.btn_5.TabIndex = 4;
            this.btn_5.Text = "3";
            this.btn_5.UseVisualStyleBackColor = false;
            this.btn_5.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_6
            // 
            this.btn_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.ForeColor = System.Drawing.Color.White;
            this.btn_6.Location = new System.Drawing.Point(382, 4);
            this.btn_6.Margin = new System.Windows.Forms.Padding(2);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(56, 31);
            this.btn_6.TabIndex = 5;
            this.btn_6.Text = ">";
            this.btn_6.UseVisualStyleBackColor = false;
            this.btn_6.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_2
            // 
            this.btn_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.ForeColor = System.Drawing.Color.White;
            this.btn_2.Location = new System.Drawing.Point(140, 4);
            this.btn_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(56, 31);
            this.btn_2.TabIndex = 1;
            this.btn_2.Text = "<";
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_3
            // 
            this.btn_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.ForeColor = System.Drawing.Color.White;
            this.btn_3.Location = new System.Drawing.Point(200, 4);
            this.btn_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(56, 31);
            this.btn_3.TabIndex = 2;
            this.btn_3.Text = "1";
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btn_4
            // 
            this.btn_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.ForeColor = System.Drawing.Color.White;
            this.btn_4.Location = new System.Drawing.Point(261, 4);
            this.btn_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(56, 31);
            this.btn_4.TabIndex = 3;
            this.btn_4.Text = "2";
            this.btn_4.UseVisualStyleBackColor = false;
            this.btn_4.Click += new System.EventHandler(this.PaginationBtnClick);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.DeepPink;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdd.Location = new System.Drawing.Point(881, 202);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(130, 37);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "+ Add Holiday";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAddHoliday_Click);
            // 
            // HolidayList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1131, 589);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.pPagination);
            this.Controls.Add(this.dgvHolidayList);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.ddlYear);
            this.MinimizeBox = false;
            this.Name = "HolidayList";
            this.Text = "HolidayList";
            this.Load += new System.EventHandler(this.HolidayList_Load);
            this.Controls.SetChildIndex(this.StaffPanel, 0);
            this.Controls.SetChildIndex(this.ddlYear, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.dgvHolidayList, 0);
            this.Controls.SetChildIndex(this.pPagination, 0);
            this.Controls.SetChildIndex(this.btnAdd, 0);
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolidayList)).EndInit();
            this.pPagination.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.DataGridView dgvHolidayList;
        private System.Windows.Forms.Panel pPagination;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btnAdd;
    }
}