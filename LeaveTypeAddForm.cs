﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class LeaveTypeAddForm : Form
    {
        //declare sql connection
        SqlConnection conn;

        //declare variables
        string leaveId = "";
        string mode = "";

        public void connection()
        {
            //connection method to connect with DB including server name and database name
            conn = new SqlConnection(@"data source=TMPC-051; Database=AttendanceDB; integrated security = true");
        }

        /// <summary>
        //this method is to check there is leave_id or not when "Add Leave Type" button or "edit" button is clicked
        //if there is not leave_id, change groupbox name to "Add Leave Type" and change button to "Add"
        //if there is leave_id, change groupbox name to "Edit Leave Type" and change button to "Edit"
        /// </summary>
        
        public LeaveTypeAddForm(string leave_id)
        {
            InitializeComponent();
            // Assign the leave_id parameter to the leaveId variable
            this.leaveId = Convert.ToString(leave_id);
            
        }

        /// <summary>
        //this method is to retrieve leave information from SQL and textbox data binding
        /// </summary>
        private void PopulateLeaveTypeData()
        {
            try
            {
                //open connection
                connection();
                conn.Open();

                //retrieve leave information from sql
                string selectQuery = "select name,days,description from t_leave_type where leave_type_id = @LeaveTypeId";
                SqlCommand selectCmd = new SqlCommand(selectQuery, conn);
                selectCmd.Parameters.AddWithValue("@LeaveTypeId", leaveId);

                SqlDataReader reader = selectCmd.ExecuteReader();

                //textbox data binding
                if (reader.Read())
                {
                    txtLeaveType.Text = reader["name"].ToString();
                    txtDays.Text = reader["days"].ToString();
                    rchtxtDescrp.Text = reader["description"].ToString();
                }
                else
                {
                    MessageBox.Show("No data found for the selected leave type.");
                }

                //close connection
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }           
        }

        /// <summary>
        /*this method is for add or edit leave type. If there is leave_type_id,mode is equal to edit, do edit leave type process.
        If there is not leave_type_id,mode is equal to add, do add leave type process.*/
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddEdit_Click(object sender, EventArgs e)
        {
            try
            {
                //open connection
                connection();
                conn.Open();

                //declare variables
                string name = txtLeaveType.Text;
                string days = txtDays.Text.ToString();
                string description = rchtxtDescrp.Text;

                //check if any input field is empty
                if (string.IsNullOrEmpty(name))
                {
                    //show message alert
                    MessageBox.Show("Please fill all input fields!");
                    txtLeaveType.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(days))
                {
                    //show message alert
                    MessageBox.Show("Please fill all input fields!");
                    txtDays.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(description))
                {
                    //show message alert
                    MessageBox.Show("Please fill all input fields!");
                    rchtxtDescrp.Focus();
                    return;
                }

                // Validate days input (must be number)
                if (!int.TryParse(days, out _))
                {
                    //show message alert
                    MessageBox.Show("Days must be number!");

                    //cler textbox
                    txtDays.Clear();
                    txtDays.Focus();
                    return;
                }

                // validate leave type input (must be alphabet)
                if (Regex.IsMatch(txtLeaveType.Text, "[^a-zA-Z\\s'\"']") || string.IsNullOrWhiteSpace(txtLeaveType.Text))
                {
                    //show message alert
                    MessageBox.Show("Leave type must be alphabet!");
                    txtLeaveType.Clear();
                    txtLeaveType.Focus();
                    return;
                }

                // validate description input (must be alphabet)
                if (int.TryParse(description, out _))
                {
                    //show message alert
                    MessageBox.Show("Description must not be integer!");
                    rchtxtDescrp.Clear();
                    rchtxtDescrp.Focus();
                    return;
                }

                // if mode is equal to add, do add leave type process,if mode is equal to edit, do edit leave type process
                if (mode == "add")
                {
                    //insert leave type into sql server
                    string insertLeaveTypeList = "insert into t_leave_type (name,days,description) values (@Name, @Days, @Description)";

                    SqlCommand insertCmd = new SqlCommand(insertLeaveTypeList, conn);
                    insertCmd.Parameters.AddWithValue("@Name", name);
                    insertCmd.Parameters.AddWithValue("@Days", days);
                    insertCmd.Parameters.AddWithValue("@Description", description);

                    insertCmd.ExecuteNonQuery();

                    //show message alert
                    MessageBox.Show("Leave Type is added successfully!");

                    //close connection
                    conn.Close();

                    //clear textboxes
                    txtLeaveType.Clear();
                    txtDays.Clear();
                    rchtxtDescrp.Clear();

                    //close form
                    this.Close();

                    //redirect leave type list page
                    LeaveTypeList leaveTypeList = new LeaveTypeList();
                    leaveTypeList.Show();

                }
                else
                {
                    //update leave type list
                    string updateLeaveTypeList = "update t_leave_type set name=@Name, days=@Days, description =@Description where leave_type_id=@LeaveTypeId";
                    SqlCommand updateCmd = new SqlCommand(updateLeaveTypeList, conn);
                    updateCmd.Parameters.AddWithValue("@Name", name);
                    updateCmd.Parameters.AddWithValue("@Days", days);
                    updateCmd.Parameters.AddWithValue("@Description", description);
                    updateCmd.Parameters.AddWithValue("@LeaveTypeId", leaveId);

                    updateCmd.ExecuteNonQuery();

                    //show alert message
                    MessageBox.Show("Leave Type is updated successfully!");

                    //close connection
                    conn.Close();

                    //clear textboxes
                    txtLeaveType.Clear();
                    txtDays.Clear();
                    rchtxtDescrp.Clear();

                    //close form
                    this.Close();

                    //redirect leave type list page
                    LeaveTypeList leaveTypeList = new LeaveTypeList();
                    leaveTypeList.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

        }

        /// <summary>
        /// this method is to redirect leave type list when the user clicks cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                LeaveTypeList LeaveListForm = new LeaveTypeList();
                LeaveListForm.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void LeaveTypeAddForm_Load(object sender, EventArgs e)
        {
            try
            {
                lblEmpName.Text = Login.employeeName;
                if (leaveId == null)
                {
                    mode = "add";
                }
                else
                {
                    mode = "edit";
                }

                //group box name changing
                //button name changing
                if (mode == "add")
                {
                    gpboxLeave.Text = "Add Leave Type";
                    btnAddEdit.Text = "Add";
                }
                else
                {
                    gpboxLeave.Text = "Edit Leave Type";
                    btnAddEdit.Text = "Edit";
                    PopulateLeaveTypeData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
