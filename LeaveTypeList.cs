﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class LeaveTypeList : MenuBar
    {
        SqlConnection conn;
        string leave_id;
        public LeaveTypeList()
        {
            InitializeComponent();
            btnLeave.BackColor = Color.FromArgb(151, 242, 0);

            // set data grid view border style and alignment.
            dgvLeaveTypeList.BorderStyle = BorderStyle.FixedSingle;
            dgvLeaveTypeList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvLeaveTypeList.AutoGenerateColumns = false;
            dgvLeaveTypeList.ColumnHeadersHeight = 35;
            dgvLeaveTypeList.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dgvLeaveTypeList.BorderStyle = BorderStyle.None;

            // create Edit button on data grid view column
            DataGridViewLinkColumn Edit = new DataGridViewLinkColumn();
            Edit.Name = "btnEdit";
            Edit.HeaderText = "Action";
            Edit.DefaultCellStyle.BackColor = Color.FromArgb(201, 201, 201);
            Edit.DefaultCellStyle.ForeColor = Color.Blue;
            Edit.Width = 115;
            Edit.Text = "Edit";
            Edit.UseColumnTextForLinkValue = true;

            // create Delete button on data grid view column
            DataGridViewLinkColumn Delete = new DataGridViewLinkColumn();
            Delete.Name = "btnDelete";
            Delete.Text = "Delete";
            Delete.HeaderText = " ";
            Delete.DefaultCellStyle.BackColor = Color.FromArgb(255, 241, 158);
            Delete.DefaultCellStyle.ForeColor = Color.Blue;
            Delete.Width = 115;
            Delete.UseColumnTextForLinkValue = true;

            // add column buttons to datagridview
            dgvLeaveTypeList.Columns.Add(Edit);
            dgvLeaveTypeList.Columns.Add(Delete);
        }
        public void Connection()
        {
            conn = new SqlConnection(@"data source=TMPC-056; database=AttendanceDB; integrated security = true");
        }
        /// <summary>
        /// load leave type data from table = t_leave_type , database = "AttendanceDB"
        /// </summary>
        public void Load_LeaveType()
        {
            lblNodata.Text = "No leave type data available"; // show lblNodata if there is no row in leave type table
            lblNodata.Visible = false;

            //refresh datagridview when new row is added or updated
            dgvLeaveTypeList.Rows.Clear(); 
            Connection();
            string selectLeaveTypeList = "select * from t_leave_type order by leave_type_id";
            conn.Open();
            SqlCommand cmd = new SqlCommand(selectLeaveTypeList, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    dgvLeaveTypeList.Rows.Add(reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[0].ToString());
                }
            }
            else
            {
                lblNodata.BringToFront();
                lblNodata.Visible = true;
            }
            
            conn.Close();
        }
        private void LeaveTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                // display Leave Type data in datagridview with Load_LeaveType method
                Load_LeaveType();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dgvLeaveTypeList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            // delete selected row with leaveID
            if (dgvLeaveTypeList.Columns[e.ColumnIndex].Name == "btnDelete")
            {
                if (MessageBox.Show("Are you sure to delete?", "Comfirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (e.RowIndex >= 0)
                    {
                        DataGridViewRow row = dgvLeaveTypeList.Rows[e.RowIndex];
                        Connection();
                        conn.Open();
                        string deleteLeaveType = "delete from t_leave_type where leave_type_id=@leave_id";
                        SqlCommand cmd = new SqlCommand(deleteLeaveType, conn);
                        string leaveID = row.Cells[3].Value.ToString();
                        cmd.Parameters.AddWithValue("@leave_id", leaveID);
                        cmd.ExecuteNonQuery();
                        conn.Close();

                        MessageBox.Show("Leave Type is successfully deleted");
                        Load_LeaveType();
                    }
                }
            }
            // send corresponding parameter "leave_id" of selected row to LeaveTypeAddForm 
            else if (dgvLeaveTypeList.Columns[e.ColumnIndex].Name == "btnEdit")
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dgvLeaveTypeList.Rows[e.RowIndex];
                    leave_id = row.Cells[3].Value.ToString();

                    LeaveTypeAddForm leavetypeadd = new LeaveTypeAddForm(this.leave_id);
                    leavetypeadd.Show();

                    Load_LeaveType();
                    this.Close();
                }
            }
        }
        // call leave type add form (Add menu)
        private void btnAddLeaveType_Click(object sender, EventArgs e)
        {
            LeaveTypeAddForm leaveAdd = new LeaveTypeAddForm(null);
            leaveAdd.Show();
            this.Close();

        }
        // Merge 2 column headers 
        private void dgvLeaveTypeList_Paint(object sender, PaintEventArgs e)
        {
            //Offsets to adjust the position of the merged Header.
            int heightOffset = -3;
            int widthOffset = -2;
            int xOffset = 0;
            int yOffset = 1;

            //Index of Header column from where the merging will start.
            int columnIndex = 4;

            //Number of Header columns to be merged.
            int columnCount = 2;

            //Get the position of the Header Cell.
            Rectangle headerCellRectangle = dgvLeaveTypeList.GetCellDisplayRectangle(columnIndex, 0, true);

            //X coordinate  of the merged Header Column.
            int xCord = headerCellRectangle.Location.X + xOffset;

            //Y coordinate  of the merged Header Column.
            int yCord = headerCellRectangle.Location.Y - headerCellRectangle.Height + yOffset;

            //Calculate Width of merged Header Column by adding the widths of all Columns to be merged.
            int mergedHeaderWidth = dgvLeaveTypeList.Columns[columnIndex].Width + dgvLeaveTypeList.Columns[columnIndex + columnCount - 1].Width + widthOffset;

            //Generate the merged Header Column Rectangle.
            Rectangle mergedHeaderRect = new Rectangle(xCord, yCord, mergedHeaderWidth, headerCellRectangle.Height + heightOffset);

            //Draw the merged Header Column Rectangle.
            e.Graphics.FillRectangle(new SolidBrush(Color.White), mergedHeaderRect);

            Font drawFont = new Font("Work Sans", 10);
            //Draw the merged Header Column Text.
            e.Graphics.DrawString("Action", drawFont, Brushes.Black, xCord + 95, yCord + 9);
        }
        private void dgvLeaveTypeList_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            }
        }
    }
}
