﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace Attendance_Management_System
{
    public partial class HolidayAddAndEdit : Form
    {
        //declare sql connection
        SqlConnection conn;

        //declare variables
        string holidayId = "";
        string mode = "";

        public void connection()
        {
            //connection method to connect with DB including server name and database name
            conn = new SqlConnection(@"data source=TMPC-051; Database=AttendanceDB; integrated security = true");
        }

        /// <summary>
        //this method is to check there is holiday_id or not when "Add Holiday" button or "edit" button is clicked
        //if there is not holiday_id, change groupbox name to "Add Holiday" and change button to "Add"
        //if there is holiday_id, change groupbox name to "Edit Holiday" and change button to "Edit"
        /// </summary>
        //holiday_id

        public HolidayAddAndEdit(string holiday_id)
        {
            InitializeComponent();

            // Set the custom date format for the DateTimePicker
            dtpHolidayDate.CustomFormat = "yyyy/MM/dd";

            // Set the format property to Custom to apply the custom format
            dtpHolidayDate.Format = DateTimePickerFormat.Custom;

            // Assign the holiday_id parameter to the holidayId variable
            this.holidayId = holiday_id;


            if (holidayId == null)
            {
                mode = "add";
            }
            else
            {
                mode = "edit";
            }

            // Change the button and groupbox text based on the mode
            if (mode == "add")
            {
                holidayGroupBox.Text = "Add Holiday";
                btnAddEdit.Text = "Add";
            }
            else
            {
                holidayGroupBox.Text = "Edit Holiday";
                btnAddEdit.Text = "Edit";

                //connection open
                connection();
                try
                {
                    conn.Open();

                    //retrieve holiday information from sql
                    string selectQuery = "select name, holiday_date from t_holiday where holiday_id = @HolidayId";
                    SqlCommand selectCmd = new SqlCommand(selectQuery, conn);
                    selectCmd.Parameters.AddWithValue("@HolidayId", holidayId);

                    SqlDataReader reader = selectCmd.ExecuteReader();

                    //textbox data binding
                    if (reader.Read())
                    {
                        txtName.Text = reader["name"].ToString();
                        dtpHolidayDate.Value = Convert.ToDateTime(reader["holiday_date"]);
                    }

                    conn.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Exception error :" + ex.Message);
                }
            }          
        }

        /// <summary>
        //this method is for add or edit holiday. If there is holiday_id,mode is equal to edit, do edit holiday process.
        //If there is not holiday_id,mode is equal to add, do add holiday process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddEdit_Click(object sender, EventArgs e)
        {
            try
            {
                connection();
                conn.Open();

                //string declarations
                string name = txtName.Text;
                string date = dtpHolidayDate.Value.ToString();

                // Check if name field is empty
                if (string.IsNullOrWhiteSpace(name))
                {
                    //show alert message
                    MessageBox.Show("Name is required!");
                    txtName.Focus();
                    return;
                }

                //check if name is integer
                if (Regex.IsMatch(txtName.Text, "[^a-zA-Z\\s'\"']") || string.IsNullOrWhiteSpace(txtName.Text))
                {
                    //show alert message
                    MessageBox.Show("Name must not be integer!");
                    txtName.Clear();
                    txtName.Focus();
                    return;
                }

                // Check if name and date already exist
                string checkQuery = "select count(*) from t_holiday where name = @Name and holiday_date = @HolidayDate";
                SqlCommand checkCmd = new SqlCommand(checkQuery, conn);
                checkCmd.Parameters.AddWithValue("@Name", name);
                checkCmd.Parameters.AddWithValue("@HolidayDate", date);
                int count = (int)checkCmd.ExecuteScalar();
                if (count > 0)
                {
                    //show alert message
                    MessageBox.Show("This holiday is already exist!");
                    txtName.Clear();
                    txtName.Focus();
                    return;
                }

                // if mode is equal to add, do add holiday process
                if (mode == "add")
                {
                    //insert data to SQL Server
                    //query for add holiday
                    string addHoliday = "insert into t_holiday (name, holiday_date) values (@Name, @HolidayDate)";

                    SqlCommand cmd = new SqlCommand(addHoliday, conn);
                    cmd.Parameters.AddWithValue("@Name", name);
                    cmd.Parameters.AddWithValue("@HolidayDate", date);

                    cmd.ExecuteNonQuery();

                    //show alert message
                    MessageBox.Show("Holiday is added successfully!");

                    //close connection
                    conn.Close();

                    //clear textbox
                    txtName.Clear();

                    this.Close();
                    HolidayList holidayList = new HolidayList();
                    holidayList.Show();
                }
                else
                {
                    //query for update holiday
                    string editHoliday = "update t_holiday set name=@Name, holiday_date=@HolidayDate where holiday_id=@HolidayId";
                    SqlCommand editCmd = new SqlCommand(editHoliday, conn);
                    editCmd.Parameters.AddWithValue("@Name", name);
                    editCmd.Parameters.AddWithValue("@HolidayDate", date);
                    editCmd.Parameters.AddWithValue("@HolidayId", holidayId);

                    editCmd.ExecuteNonQuery();

                    MessageBox.Show("Holiday is updated successfully!");

                    //close connection
                    conn.Close();

                    //clear textbox
                    txtName.Clear();
                    this.Close();
                    HolidayList holidayList = new HolidayList();
                    holidayList.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message);
            }
            
        }

        /// <summary>
        /// //this method is to redirect holiday list when the user clicks cancel btn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                HolidayList holidayList = new HolidayList();
                holidayList.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void HolidayAddAndEdit_Load(object sender, EventArgs e)
        {
            lblEmpName.Text = Login.employeeName;
        }
    }
}
