﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

//Thiha
namespace Attendance_Management_System
{
    public partial class HolidayList : MenuBar
    {
        //variable assign
        private int DataPerPage = 5; //each page for show
        public static int currentPageIndex = 1; //for current show
        public static string id = "";
        public static string date = "";
        public static string name = "";
        public static int currentYear = 0;
        public static int indexYear = 0;
        private int totalPages = 1;
        private string[] paginBtnArray = { "0", "0", "0", "0", "0", "0", "0" };//set buttons array


        // connection assign

        List<String> year = new List<String>();
        DataTable dt = new DataTable();
        SqlConnection conn;
        public void connection()
        {
            conn = new SqlConnection(@"data source=TMPC-057; Database=AttendanceDB; integrated security = true");
        }
        public HolidayList()
        {
            InitializeComponent();
            this.BackColor = Color.White;   // Set form background color to white
            btnHoliday.BackColor = Color.FromArgb(151, 242, 0);

        }

        private void HolidayList_Load(object sender, EventArgs e)
        {
            dgv_loading();
            CheckDataAbsent();

            // Get the current year
            int currentYear = DateTime.Now.Year;

            // Bind data for the current year
            DataBind(currentYear);

            dgvHolidayList.DefaultCellStyle.Font = new Font("Calibri", 12);
            dgvHolidayList.DefaultCellStyle.ForeColor = Color.Black;
            dgvHolidayList.DefaultCellStyle.BackColor = Color.White;
            ShowDataForCurrentPage();
            

            if (dgvHolidayList.Rows.Count > 0)
            {
                dgvHolidayList.ColumnHeadersHeight = dgvHolidayList.Rows[0].Height;
            }

            // Set the default value of the ComboBox to the current year
            if (ddlYear.Items.Count > 0)
            {
                ddlYear.SelectedItem = currentYear.ToString();
            }

        }


        private void dgv_loading()
        {
            connection();

            try
            {
                conn.Open();
                // for gridview columns
                dgvHolidayList.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView_CellPainting);
                dgvHolidayList.AutoGenerateColumns = false;
                dgvHolidayList.ColumnCount = 4;
                dgvHolidayList.Columns[0].HeaderText = "ID";
                dgvHolidayList.Columns[0].Name = "ID";
                dgvHolidayList.Columns[0].DataPropertyName = "ID";

                dgvHolidayList.Columns[0].Visible = false;

                dgvHolidayList.Columns[1].HeaderText = "Date";
                dgvHolidayList.Columns[1].Name = "Date";
                dgvHolidayList.Columns[1].DataPropertyName = "Date";
                dgvHolidayList.Columns[1].Width = 300;

                dgvHolidayList.Columns[2].HeaderText = "Days";
                dgvHolidayList.Columns[2].Name = "Days";
                dgvHolidayList.Columns[2].DataPropertyName = "Days";
                dgvHolidayList.Columns[2].Width = 100;

                dgvHolidayList.Columns[3].HeaderText = "Name";
                dgvHolidayList.Columns[3].Name = "Name";
                dgvHolidayList.Columns[3].DataPropertyName = "Name";
                dgvHolidayList.Columns[3].Width = 250;
                dgvHolidayList.RowTemplate.Height = 35;

                dgvHolidayList.DefaultCellStyle.SelectionBackColor = Color.White;
                dgvHolidayList.DefaultCellStyle.SelectionForeColor = Color.Black;

                // Align header cells to the middle
                dgvHolidayList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Align data cells to the middle
                foreach (DataGridViewColumn column in dgvHolidayList.Columns)
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }

                // for gridview columns
                dgvHolidayList.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView_CellPainting);
                dgvHolidayList.AutoGenerateColumns = false;
                dgvHolidayList.ColumnCount = 4;

                // Set the header background color to white
                dgvHolidayList.ColumnHeadersDefaultCellStyle.BackColor = Color.White;

                // Set the header text color to black
                dgvHolidayList.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;

                // Set the header font
                dgvHolidayList.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 12, FontStyle.Regular);

                //to add edit & delete button in gridview
                EditAndDelete();

                //data bind to combobox from sql server
                YearSearch();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private void CheckDataAbsent()
        {
            if (ddlYear.Items.Count > 0)
            {
                int yearToBind = (currentYear == 0) ? Convert.ToInt32(ddlYear.Items[0].ToString()) : currentYear;
                DataBind(yearToBind);
                setTotalPages(dt.Rows.Count);
                currentPageIndex = (currentPageIndex == 0) ? 1 : currentPageIndex;
                showData();
                updatePaginBtn();
                ddlYear.SelectedIndex = indexYear;
            }
            else
            {
                lblYear.Text = "";
                pPagination.Visible = false;
            }
        }


        //Call this method to show data for the current page
        private void ShowDataForCurrentPage()
        {
            if (ddlYear.SelectedItem != null)
            {
                int selectedYear = int.Parse(ddlYear.SelectedValue.ToString());

                // Check if there is data available for the selected year
                if (DataExistsForYear(selectedYear))
                {
                    DataBind(selectedYear);
                    showData();
                }
                else
                {
                    // Handle the case where no data is available for the selected year
                    // For example, you can clear the DataGridView
                    dgvHolidayList.DataSource = null;
                    // MessageBox.Show("No data available for the selected year.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }
        // Method to check if data exists for the selected year
        private bool DataExistsForYear(int year)
        {
            try
            {
                // Perform a SQL query to check if data exists for the selected year
                // For example:
                // "SELECT COUNT(*) FROM t_holiday WHERE YEAR(holiday_date) = @year"
                // Execute the query and return true if the count is greater than 0, indicating data exists
                // Otherwise, return false
                connection();

                {
                    conn.Open();
                    string query = "select count(*) from t_holiday where year(holiday_date) = @year";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@year", year);
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    return count > 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error checking data existence: " + ex.Message);
                // Handle any exceptions, you may choose to return false or throw an exception
                return false;
            }
        }

        private void dgvHolidayList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    string holidayName = dgvHolidayList.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                    string holidayId = dgvHolidayList.Rows[e.RowIndex].Cells["ID"].Value.ToString();
                    string selectedYear = ddlYear.SelectedValue.ToString();
                    // Click Edit Column
                    if (e.ColumnIndex == dgvHolidayList.Columns["btnedit"].Index)
                    {
                        // Initialize a list to store holiday IDs
                        List<string> holidayIds = new List<string>();

                        // Query to retrieve holiday IDs for the given holiday name
                        string query = "SELECT holiday_id FROM t_holiday WHERE name = @name AND YEAR(holiday_date) = @year";

                        try
                        {
                            // Open the connection
                            conn.Open();
                            
                            // Execute the query
                            SqlCommand cmd = new SqlCommand(query, conn);
                            cmd.Parameters.AddWithValue("@name", holidayName);
                            cmd.Parameters.AddWithValue("@year", selectedYear);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                // Read the holiday IDs and add them to the list
                                while (reader.Read())
                                {
                                    holidayIds.Add(reader["holiday_id"].ToString());
                                }
                            }
                            SelectedYearPage();
                            // Close the connection
                            conn.Close();
                        }
                        catch (Exception ex)
                        {
                            // Handle any exceptions
                            MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            conn.Close(); // Make sure to close the connection in case of an error
                            return; // Exit the method to prevent further execution
                        }

                        // Check if holidayIds list contains any elements
                        if (holidayIds.Count > 0)
                        {
                            // Get the ID of the last holiday (if multiple)
                            string editHolidayId = holidayIds[holidayIds.Count - 1];

                            // Open the HolidayAddAndEdit form with the holiday ID for editing
                            HolidayAddAndEdit holidayAddAndEdit = new HolidayAddAndEdit(editHolidayId);
                            holidayAddAndEdit.Show();
                            this.Close();
                        }
                        else
                        {
                            // No holiday IDs found for the given name
                            MessageBox.Show("No holiday ID found for " + holidayName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                   
                    // Click Delete Column
                    else if (e.ColumnIndex == dgvHolidayList.Columns["btndelete"].Index)
                    {
                        // Confirmation message for Delete
                        DialogResult result = MessageBox.Show("Are you sure you want to delete this holiday?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                        if (result == DialogResult.Yes)
                        {
                            // Initialize a list to store holiday IDs
                            List<string> holidayIds = new List<string>();

                            // Query to retrieve holiday IDs for the given holiday name
                            string query = "select holiday_id from t_holiday where name = @name";

                            try
                            {
                                // Open the connection
                                conn.Open();

                                // Execute the query
                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.Parameters.AddWithValue("@name", holidayName);

                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    // Read the holiday IDs and add them to the list
                                    while (reader.Read())
                                    {
                                        holidayIds.Add(reader["holiday_id"].ToString());
                                    }
                                }

                                // Close the connection
                                conn.Close();
                            }
                            catch (Exception ex)
                            {
                                // Handle any exceptions
                                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                conn.Close(); // Make sure to close the connection in case of an error
                                return; // Exit the method to prevent further execution
                            }

                            // Check if holidayIds list contains any elements
                            if (holidayIds.Count > 0)
                            {
                                // Call the method to delete holidays with the retrieved IDs
                                DeleteHoliday(holidayIds.ToArray());
                            }
                            else
                            {
                                // No holiday IDs found for the given name
                                MessageBox.Show("No holiday IDs found for " + holidayName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close(); // Close the database connection in a finally block to ensure it is always executed
            }
        }


        // Method to delete a holiday record
        // Method to delete holiday records by IDs
        private void DeleteHoliday(string[] holidayIds)
        {
            int yearDelete = int.Parse(ddlYear.SelectedValue.ToString());

            try
            {
                connection();

                // Retrieve the last holiday ID for the given name
                string lastHolidayId = holidayIds.Last();
                // Retrieve the name of the holiday using the last holiday ID
                string holidayName = "";

                string holidayname = "select name from t_holiday where holiday_id = @id";

                using (SqlCommand cmd = new SqlCommand(holidayname, conn))
                {
                    // Assign the last holiday ID as the parameter value
                    cmd.Parameters.AddWithValue("@id", lastHolidayId);

                    // Open Connection
                    conn.Open();

                    // Execute SqlCommand to retrieve the holiday name
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        holidayName = result.ToString();
                    }
                    conn.Close();
                }

                // Construct success message with the holiday name
                string successMessage = holidayName +" is deleted successfully.";

                string query = "delete from t_holiday where holiday_id = @id";

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    // Assign the last holiday ID as the parameter value
                    cmd.Parameters.AddWithValue("@id", lastHolidayId);

                    // Open Connection
                    conn.Open();

                    // Execute SqlCommand to delete the record
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected > 0)
                    {
                        MessageBox.Show(successMessage, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        dt = SelectHolidayList(yearDelete);

                        // Update the total number of pages
                        setTotalPages(dt.Rows.Count);
                        showData();
                        updatePaginBtn();
                    }
                    else
                    {
                        MessageBox.Show("No holiday with the provided ID found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }



        // edit and delete header added
        private void dgvHolidayList_Paint(object sender, PaintEventArgs e)
        {
            // get the column header cell
            Rectangle r1 = this.dgvHolidayList.GetCellDisplayRectangle(4, -1, true);
            r1.X += 1;
            r1.Y += 1;
            r1.Width = r1.Width * 2 - 4;
            r1.Height = r1.Height - 3;
            e.Graphics.FillRectangle(new SolidBrush(Color.White), r1);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            e.Graphics.DrawString("Description", this.dgvHolidayList.ColumnHeadersDefaultCellStyle.Font, new SolidBrush(color: Color.Black), r1, format);

        }


        // for edit and delete column border
        private void dataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            }
        }

        //edit&delete column added
        public void EditAndDelete()
        {

            DataGridViewLinkColumn edit = new DataGridViewLinkColumn();
            dgvHolidayList.Columns.Add(edit);
            edit.HeaderText = "";
            edit.Name = "btnedit";
            edit.Text = "Edit";
            edit.Width = 100;
            edit.UseColumnTextForLinkValue = true;
            edit.LinkColor = Color.DodgerBlue;
            edit.VisitedLinkColor = Color.DodgerBlue;
            edit.SortMode = DataGridViewColumnSortMode.NotSortable;
            edit.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter; // Center align the content

            DataGridViewLinkColumn delete = new DataGridViewLinkColumn();
            dgvHolidayList.Columns.Add(delete);
            delete.HeaderText = "";
            delete.Name = "btndelete";
            delete.Text = "Delete";
            delete.Width = 100;
            delete.UseColumnTextForLinkValue = true;
            delete.LinkColor = Color.DodgerBlue;
            delete.VisitedLinkColor = Color.DodgerBlue;
            delete.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter; // Center align the content



            delete.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.dgvHolidayList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHolidayList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }


        // data bind to combobox from sql server
        private void YearSearch()
        {
            connection();
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select DISTINCT YEAR(holiday_date) from t_holiday", conn);
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        year.Add(dr[0].ToString());
                    }

                    // Bind year from database to combobox
                    ddlYear.DataSource = year;

                    // Set ComboBox style to DropDownList to make it non-editable
                    ddlYear.DropDownStyle = ComboBoxStyle.DropDownList;

                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }


        // for total page
        // calculatr total page according row count by year 
        // for button pagination & show per page
        private void setTotalPages(int rowCount)
        {
            // caculate number of pages
            int numberOfPages = rowCount / DataPerPage;
            if (numberOfPages * DataPerPage < rowCount)
            {
                totalPages = numberOfPages + 1;
            }
            else
            {
                totalPages = numberOfPages;
            }
        }
        //calculate data from sql to data grid view according by selected year
        public void DataBind(int selectedYear)
        {
            try
            {
                conn.Open();
                dt = SelectHolidayList(selectedYear);

                // Now, you can bind the DataTable to the DataGridView
                dgvHolidayList.DataSource = dt;
                SqlCommand cmd1 = new SqlCommand("select count(*) from t_holiday where year(holiday_date) = @year", conn);
                cmd1.Parameters.AddWithValue("@year", selectedYear);

                // execute data 
                int holidayCount = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                showNumberOfData(selectedYear, holidayCount);
            }
            catch (Exception error)
            {
                Console.WriteLine("Error: " + error.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private DataTable SelectHolidayList(int year)
        {
            DataTable dataTable = new DataTable();
            string selectedHolidayList = "select string_agg(holiday_id, '/') as id, " +
                                            "case when min(holiday_date) = max(holiday_date) " +
                                            "then concat(format(min(holiday_date), 'yyyy/MM/dd'), ' [', datename(weekday, min(holiday_date)), ']') " +
                                            "else concat(format(min(holiday_date), 'yyyy/MM/dd'), ' [', datename(weekday, min(holiday_date)), ']',' ', '-', ' ', " +
                                            "format(max(holiday_date), 'yyyy/MM/dd'), ' [', datename(weekday, max(holiday_date)), ']') end as date, " +
                                            "count(name) as days, name as name " +
                                            "from t_holiday where year(holiday_date) = @year " +
                                            "group by name order by date;";

            SqlCommand cmd = new SqlCommand(selectedHolidayList, conn);
            cmd.Parameters.AddWithValue("@year", year);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
          
            da.Fill(dataTable); // Fill the DataTable with data from the database
            return dataTable;
        }


        // return datatable from current state
        private DataTable GetCurrentRecords(int page, DataTable dataTable)
        {
            DataTable resultDataTable = new DataTable();

            try
            {
                // Calculate the offset and limit
                int offset = (page - 1) * DataPerPage;
                int limit = Math.Min(dataTable.Rows.Count - offset, DataPerPage);

                // Clone the structure of the original table
                resultDataTable = dataTable.Clone();

                // Populate the resultDataTable with the data for the current page
                if (limit > 0)
                {
                    for (int i = offset; i < offset + limit; i++)
                    {
                        resultDataTable.ImportRow(dataTable.Rows[i]);
                    }
                }
                else if (page > 1) // If current page is empty and it's not the first page
                {
                    // Show the data from the previous page
                    int previousPage = page - 1;
                    currentPageIndex = previousPage;
                    offset = (previousPage - 1) * DataPerPage;
                    limit = Math.Min(dataTable.Rows.Count - offset, DataPerPage);

                    for (int i = offset; i < offset + limit; i++)
                    {
                        resultDataTable.ImportRow(dataTable.Rows[i]);
                    }
                    
                }
               
                // Show number of data
                int totalDays = GetTotalHolidaysForSelectedYear();
                showNumberOfData(totalDays, DataPerPage);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return resultDataTable;
        }


        // Method to get total holidays for the selected year
        private int GetTotalHolidaysForSelectedYear()
        {
            int totalHolidays = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(@"data source=TMPC-057; Database=AttendanceDB; integrated security=true"))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) from t_holiday where year(holiday_date) = @year", conn);
                    cmd.Parameters.AddWithValue("@year", Convert.ToInt32(ddlYear.SelectedValue.ToString()));
                    totalHolidays = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error fetching total holidays: " + ex.Message);
            }
            return totalHolidays;
        }

        private void showData()
        {
            // Check if DataGridView has any rows
            if (dgvHolidayList.Rows.Count > 0)
            {
                // Show the pagination panel
                pPagination.Visible = true;

                // Show data for the current page
                this.dgvHolidayList.DataSource = GetCurrentRecords(currentPageIndex, dt);
                dgvHolidayList.Columns["Date"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvHolidayList.Columns["Days"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvHolidayList.Columns["Name"].SortMode = DataGridViewColumnSortMode.NotSortable;

                int year = int.Parse(ddlYear.SelectedValue.ToString());
                int totalHolidays = GetTotalHolidays(year);
                showNumberOfData(year, totalHolidays);
               
            }
            else
            {
                // Hide the pagination panel when there are no rows in the DataGridView
                pPagination.Visible = false;

            }
        }


        private void updatePaginBtn()
        {
            // check all data can not show in first page
            if (totalPages <= 1)
            {
                // hide panel (pagination buttons)
                pPagination.Visible = false;
            }
            else
            {
                // show panel (pagination buttons)
                pPagination.Visible = true;

                // check current page is first page
                if (currentPageIndex <= 1)
                {
                    paginBtnArray[6] = ">>";
                    paginBtnArray[5] = ">";
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";

                    if (totalPages == 2)
                    {
                        paginBtnArray[4] = "2";
                        paginBtnArray[3] = "1";
                        paginBtnArray[2] = "0";
                    }
                    else
                    {
                        paginBtnArray[4] = "3";
                        paginBtnArray[3] = "2";
                        paginBtnArray[2] = "1";
                    }
                }
                // check current page is last page
                else if (currentPageIndex == totalPages)
                {
                    paginBtnArray[6] = (currentPageIndex).ToString();
                    paginBtnArray[5] = (currentPageIndex - 1).ToString();
                    paginBtnArray[4] = (currentPageIndex - 2).ToString();
                    paginBtnArray[3] = "<";
                    paginBtnArray[2] = "<<";
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";
                    if (currentPageIndex - 2 == 0)
                    {
                        paginBtnArray[4] = "<";
                        paginBtnArray[3] = "<<";
                        paginBtnArray[2] = "0";
                    }
                }
                else
                {
                    paginBtnArray[6] = ">>";
                    paginBtnArray[5] = ">";
                    paginBtnArray[4] = (currentPageIndex + 1).ToString();
                    paginBtnArray[3] = (currentPageIndex).ToString();
                    paginBtnArray[2] = (currentPageIndex - 1).ToString();
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";

                    if (currentPageIndex - 1 >= 1)
                    {
                        paginBtnArray[1] = "<";
                        paginBtnArray[0] = "<<";
                    }
                }
            }
            setPaginBtn();
        }
        private void setPaginBtn()
        {
            for (int i = 0; i < paginBtnArray.Length; i++)
            {
                if (!paginBtnArray[i].Equals("0"))
                {
                    // get all button from panel 
                    Button paginBtn = (Button)pPagination.Controls.Find("btn_" + (i + 1), true)[0];
                    paginBtn.Visible = true;

                    // set new text to pagin button
                    paginBtn.Text = paginBtnArray[i];

                    // change hightlight color to current page number button
                    if (currentPageIndex.ToString().Equals(paginBtnArray[i]))
                    {
                        paginBtn.BackColor = Color.LimeGreen;
                        paginBtn.ForeColor = Color.White;
                    }
                    else
                    {
                        paginBtn.BackColor = Color.DeepSkyBlue;
                        paginBtn.ForeColor = Color.White;
                    }
                }
                else
                {
                    // hide buttons that are not required
                    Button paginBtn = (Button)pPagination.Controls.Find("btn_" + (i + 1), true)[0];
                    paginBtn.Visible = false;
                }
            }
        }

        // for all paging buttons
        private void PaginationBtnClick(object sender, EventArgs e)
        {
            string btnText = (sender as Button).Text;

            if (btnText.Equals("<<"))
            {
                currentPageIndex = 1;
            }
            else if (btnText.Equals("<"))
            {
                currentPageIndex--;
            }
            else if (btnText.Equals(">"))
            {
                currentPageIndex++;
            }
            else if (btnText.Equals(">>"))
            {
                currentPageIndex = totalPages;
            }
            else
            {
                int pageNumber = int.Parse(btnText);
                currentPageIndex = pageNumber;
            }

            // Update pagination buttons
            updatePaginBtn();

            // Show data for the current page
            showData();
        }


        // Total Holiday express for selected year
        private void showNumberOfData(int year, int total)
        {
            lblYear.Text = total + " Holidays in " + ddlYear.SelectedValue.ToString() + " (show " + "5 records per page)";
        }

        private void ddlSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Check if the form is currently loading
            if (this.IsHandleCreated)
            {
                int year = int.Parse(ddlYear.SelectedValue.ToString());
                DataBind(year);
                setTotalPages(dt.Rows.Count);

                if (dt.Rows.Count % 5 == 0)
                {
                    if ((dt.Rows.Count / 5) < currentPageIndex)
                    {
                        currentPageIndex = 1;
                    }
                }
                else
                {
                    if ((dt.Rows.Count / 5) + 1 < currentPageIndex)
                    {
                        currentPageIndex = 1;
                    }
                }
                showData();

                // for pagination button
                updatePaginBtn();
            }

        }

        public void SelectedYearPage()
        {
            int year = int.Parse(ddlYear.SelectedValue.ToString());
            DataBind(year);
            setTotalPages(dt.Rows.Count);

           
            // for pagination button
            updatePaginBtn();
        }

        // Method to get total holidays for a specific year
        private int GetTotalHolidays(int year)
        {
            int totalHolidays = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(@"data source=TMPC-057; Database=AttendanceDB; integrated security=true"))
                {
                    conn.Open();
                    string query = "select count(*) from t_holiday where year(holiday_date) = @year";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@year", year);
                    totalHolidays = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error fetching total holidays: " + ex.Message);
            }
            return totalHolidays;
        }


        private void btnAddHoliday_Click(object sender, EventArgs e)
        {
            // call next form
            HolidayAddAndEdit holidayAddAndEdit = new HolidayAddAndEdit(null);
            holidayAddAndEdit.Show();
            this.Close();
        }

    }
}