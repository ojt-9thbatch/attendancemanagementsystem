﻿namespace Attendance_Management_System
{
    partial class LeaveTypeAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpboxLeave = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddEdit = new System.Windows.Forms.Button();
            this.rchtxtDescrp = new System.Windows.Forms.RichTextBox();
            this.txtDays = new System.Windows.Forms.TextBox();
            this.txtLeaveType = new System.Windows.Forms.TextBox();
            this.lblDescrp = new System.Windows.Forms.Label();
            this.lblDays = new System.Windows.Forms.Label();
            this.lblLeaveType = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblEmpName = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.gpboxLeave.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpboxLeave
            // 
            this.gpboxLeave.Controls.Add(this.btnCancel);
            this.gpboxLeave.Controls.Add(this.btnAddEdit);
            this.gpboxLeave.Controls.Add(this.rchtxtDescrp);
            this.gpboxLeave.Controls.Add(this.txtDays);
            this.gpboxLeave.Controls.Add(this.txtLeaveType);
            this.gpboxLeave.Controls.Add(this.lblDescrp);
            this.gpboxLeave.Controls.Add(this.lblDays);
            this.gpboxLeave.Controls.Add(this.lblLeaveType);
            this.gpboxLeave.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpboxLeave.Location = new System.Drawing.Point(252, 151);
            this.gpboxLeave.Name = "gpboxLeave";
            this.gpboxLeave.Size = new System.Drawing.Size(564, 323);
            this.gpboxLeave.TabIndex = 5;
            this.gpboxLeave.TabStop = false;
            this.gpboxLeave.Text = "Leave Type";
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.Location = new System.Drawing.Point(305, 240);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(167, 55);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddEdit
            // 
            this.btnAddEdit.FlatAppearance.BorderSize = 0;
            this.btnAddEdit.Location = new System.Drawing.Point(99, 240);
            this.btnAddEdit.Name = "btnAddEdit";
            this.btnAddEdit.Size = new System.Drawing.Size(167, 55);
            this.btnAddEdit.TabIndex = 4;
            this.btnAddEdit.Text = "Add / Edit";
            this.btnAddEdit.UseVisualStyleBackColor = true;
            this.btnAddEdit.Click += new System.EventHandler(this.btnAddEdit_Click);
            // 
            // rchtxtDescrp
            // 
            this.rchtxtDescrp.Font = new System.Drawing.Font("Calibri", 12F);
            this.rchtxtDescrp.Location = new System.Drawing.Point(231, 169);
            this.rchtxtDescrp.Name = "rchtxtDescrp";
            this.rchtxtDescrp.Size = new System.Drawing.Size(260, 61);
            this.rchtxtDescrp.TabIndex = 3;
            this.rchtxtDescrp.Text = "";
            // 
            // txtDays
            // 
            this.txtDays.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtDays.Location = new System.Drawing.Point(231, 101);
            this.txtDays.Multiline = true;
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(260, 34);
            this.txtDays.TabIndex = 2;
            // 
            // txtLeaveType
            // 
            this.txtLeaveType.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtLeaveType.Location = new System.Drawing.Point(231, 40);
            this.txtLeaveType.Multiline = true;
            this.txtLeaveType.Name = "txtLeaveType";
            this.txtLeaveType.Size = new System.Drawing.Size(260, 34);
            this.txtLeaveType.TabIndex = 1;
            // 
            // lblDescrp
            // 
            this.lblDescrp.AutoSize = true;
            this.lblDescrp.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblDescrp.Location = new System.Drawing.Point(71, 172);
            this.lblDescrp.Name = "lblDescrp";
            this.lblDescrp.Size = new System.Drawing.Size(119, 24);
            this.lblDescrp.TabIndex = 2;
            this.lblDescrp.Text = "Description  :";
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblDays.Location = new System.Drawing.Point(71, 104);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(119, 24);
            this.lblDays.TabIndex = 1;
            this.lblDays.Text = "Days             :";
            // 
            // lblLeaveType
            // 
            this.lblLeaveType.AutoSize = true;
            this.lblLeaveType.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblLeaveType.Location = new System.Drawing.Point(71, 43);
            this.lblLeaveType.Name = "lblLeaveType";
            this.lblLeaveType.Size = new System.Drawing.Size(121, 24);
            this.lblLeaveType.TabIndex = 0;
            this.lblLeaveType.Text = "Leave Type   :";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(149, 47);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(429, 37);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "Attendance Management System";
            // 
            // lblEmpName
            // 
            this.lblEmpName.AutoSize = true;
            this.lblEmpName.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpName.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblEmpName.Location = new System.Drawing.Point(775, 57);
            this.lblEmpName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmpName.Name = "lblEmpName";
            this.lblEmpName.Size = new System.Drawing.Size(130, 22);
            this.lblEmpName.TabIndex = 11;
            this.lblEmpName.Text = "EmployeeName";
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(688, 57);
            this.lblWelcome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(79, 22);
            this.lblWelcome.TabIndex = 10;
            this.lblWelcome.Text = "Welcome";
            // 
            // LeaveTypeAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1082, 565);
            this.Controls.Add(this.lblEmpName);
            this.Controls.Add(this.lblWelcome);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.gpboxLeave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LeaveTypeAddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LeaveTypeAddEditForm";
            this.Load += new System.EventHandler(this.LeaveTypeAddForm_Load);
            this.gpboxLeave.ResumeLayout(false);
            this.gpboxLeave.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox gpboxLeave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddEdit;
        private System.Windows.Forms.RichTextBox rchtxtDescrp;
        private System.Windows.Forms.TextBox txtDays;
        private System.Windows.Forms.TextBox txtLeaveType;
        private System.Windows.Forms.Label lblDescrp;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Label lblLeaveType;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblEmpName;
        private System.Windows.Forms.Label lblWelcome;
    }
}