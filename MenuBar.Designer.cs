﻿namespace Attendance_Management_System
{
    partial class MenuBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AdminPanel = new System.Windows.Forms.Panel();
            this.btnEmployee = new System.Windows.Forms.Button();
            this.btnLeave = new System.Windows.Forms.Button();
            this.btnMyAccount = new System.Windows.Forms.Button();
            this.btnHoliday = new System.Windows.Forms.Button();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lblAdminName = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.StaffPanel = new System.Windows.Forms.Panel();
            this.btnStaffProfile = new System.Windows.Forms.Button();
            this.btnAttendance = new System.Windows.Forms.Button();
            this.btnStaffPass = new System.Windows.Forms.Button();
            this.btnStaffLog = new System.Windows.Forms.Button();
            this.lblStaffName = new System.Windows.Forms.Label();
            this.lblStaffWelcome = new System.Windows.Forms.Label();
            this.lblStaffTitle = new System.Windows.Forms.Label();
            this.AdminPanel.SuspendLayout();
            this.StaffPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AdminPanel
            // 
            this.AdminPanel.Controls.Add(this.btnEmployee);
            this.AdminPanel.Controls.Add(this.btnLeave);
            this.AdminPanel.Controls.Add(this.btnMyAccount);
            this.AdminPanel.Controls.Add(this.btnHoliday);
            this.AdminPanel.Controls.Add(this.btnChangePassword);
            this.AdminPanel.Controls.Add(this.btnLogOut);
            this.AdminPanel.Controls.Add(this.lblAdminName);
            this.AdminPanel.Controls.Add(this.lblWelcome);
            this.AdminPanel.Controls.Add(this.lblTitle);
            this.AdminPanel.Location = new System.Drawing.Point(4, 4);
            this.AdminPanel.Margin = new System.Windows.Forms.Padding(4);
            this.AdminPanel.Name = "AdminPanel";
            this.AdminPanel.Size = new System.Drawing.Size(1372, 168);
            this.AdminPanel.TabIndex = 6;
            // 
            // btnEmployee
            // 
            this.btnEmployee.AutoSize = true;
            this.btnEmployee.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmployee.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmployee.ForeColor = System.Drawing.Color.Transparent;
            this.btnEmployee.Location = new System.Drawing.Point(513, 118);
            this.btnEmployee.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnEmployee.Name = "btnEmployee";
            this.btnEmployee.Size = new System.Drawing.Size(182, 47);
            this.btnEmployee.TabIndex = 18;
            this.btnEmployee.Text = "Employee";
            this.btnEmployee.UseVisualStyleBackColor = false;
            this.btnEmployee.Click += new System.EventHandler(this.btnEmployee_Click);
            // 
            // btnLeave
            // 
            this.btnLeave.AutoSize = true;
            this.btnLeave.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeave.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeave.ForeColor = System.Drawing.Color.Transparent;
            this.btnLeave.Location = new System.Drawing.Point(298, 118);
            this.btnLeave.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(182, 47);
            this.btnLeave.TabIndex = 17;
            this.btnLeave.Text = "Leave";
            this.btnLeave.UseVisualStyleBackColor = false;
            this.btnLeave.Click += new System.EventHandler(this.btnLeave_Click);
            // 
            // btnMyAccount
            // 
            this.btnMyAccount.AutoSize = true;
            this.btnMyAccount.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnMyAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMyAccount.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMyAccount.ForeColor = System.Drawing.Color.Transparent;
            this.btnMyAccount.Location = new System.Drawing.Point(730, 118);
            this.btnMyAccount.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnMyAccount.Name = "btnMyAccount";
            this.btnMyAccount.Size = new System.Drawing.Size(182, 47);
            this.btnMyAccount.TabIndex = 16;
            this.btnMyAccount.Text = "My Account";
            this.btnMyAccount.UseVisualStyleBackColor = false;
            this.btnMyAccount.Click += new System.EventHandler(this.btnMyAccount_Click);
            // 
            // btnHoliday
            // 
            this.btnHoliday.AutoSize = true;
            this.btnHoliday.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnHoliday.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHoliday.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHoliday.ForeColor = System.Drawing.Color.Transparent;
            this.btnHoliday.Location = new System.Drawing.Point(88, 118);
            this.btnHoliday.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnHoliday.Name = "btnHoliday";
            this.btnHoliday.Size = new System.Drawing.Size(182, 47);
            this.btnHoliday.TabIndex = 11;
            this.btnHoliday.Text = "Holiday";
            this.btnHoliday.UseVisualStyleBackColor = false;
            this.btnHoliday.Click += new System.EventHandler(this.btnHoliday_Click);
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnChangePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePassword.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePassword.ForeColor = System.Drawing.Color.Transparent;
            this.btnChangePassword.Location = new System.Drawing.Point(948, 118);
            this.btnChangePassword.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(182, 47);
            this.btnChangePassword.TabIndex = 12;
            this.btnChangePassword.Text = "Change Password";
            this.btnChangePassword.UseVisualStyleBackColor = false;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.AutoSize = true;
            this.btnLogOut.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnLogOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOut.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.Transparent;
            this.btnLogOut.Location = new System.Drawing.Point(1163, 118);
            this.btnLogOut.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(182, 47);
            this.btnLogOut.TabIndex = 13;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lblAdminName
            // 
            this.lblAdminName.AutoSize = true;
            this.lblAdminName.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdminName.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblAdminName.Location = new System.Drawing.Point(1141, 46);
            this.lblAdminName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAdminName.Name = "lblAdminName";
            this.lblAdminName.Size = new System.Drawing.Size(126, 22);
            this.lblAdminName.TabIndex = 9;
            this.lblAdminName.Text = "EmployeeName";
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(1039, 46);
            this.lblWelcome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(79, 22);
            this.lblWelcome.TabIndex = 8;
            this.lblWelcome.Text = "Welcome";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(78, 34);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(424, 37);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Attendance Management System";
            // 
            // StaffPanel
            // 
            this.StaffPanel.Controls.Add(this.btnStaffProfile);
            this.StaffPanel.Controls.Add(this.btnAttendance);
            this.StaffPanel.Controls.Add(this.btnStaffPass);
            this.StaffPanel.Controls.Add(this.btnStaffLog);
            this.StaffPanel.Controls.Add(this.lblStaffName);
            this.StaffPanel.Controls.Add(this.lblStaffWelcome);
            this.StaffPanel.Controls.Add(this.lblStaffTitle);
            this.StaffPanel.Location = new System.Drawing.Point(4, 4);
            this.StaffPanel.Margin = new System.Windows.Forms.Padding(4);
            this.StaffPanel.Name = "StaffPanel";
            this.StaffPanel.Size = new System.Drawing.Size(1372, 168);
            this.StaffPanel.TabIndex = 7;
            // 
            // btnStaffProfile
            // 
            this.btnStaffProfile.AutoSize = true;
            this.btnStaffProfile.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnStaffProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStaffProfile.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffProfile.ForeColor = System.Drawing.Color.Transparent;
            this.btnStaffProfile.Location = new System.Drawing.Point(302, 118);
            this.btnStaffProfile.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnStaffProfile.Name = "btnStaffProfile";
            this.btnStaffProfile.Size = new System.Drawing.Size(182, 47);
            this.btnStaffProfile.TabIndex = 16;
            this.btnStaffProfile.Text = "My Account";
            this.btnStaffProfile.UseVisualStyleBackColor = false;
            this.btnStaffProfile.Click += new System.EventHandler(this.btnStaffProfile_Click);
            // 
            // btnAttendance
            // 
            this.btnAttendance.AutoSize = true;
            this.btnAttendance.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAttendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAttendance.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAttendance.ForeColor = System.Drawing.Color.Transparent;
            this.btnAttendance.Location = new System.Drawing.Point(88, 118);
            this.btnAttendance.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnAttendance.Name = "btnAttendance";
            this.btnAttendance.Size = new System.Drawing.Size(182, 47);
            this.btnAttendance.TabIndex = 11;
            this.btnAttendance.Text = "Attendance";
            this.btnAttendance.UseVisualStyleBackColor = false;
            this.btnAttendance.Click += new System.EventHandler(this.btnAttendance_Click);
            // 
            // btnStaffPass
            // 
            this.btnStaffPass.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnStaffPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStaffPass.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffPass.ForeColor = System.Drawing.Color.Transparent;
            this.btnStaffPass.Location = new System.Drawing.Point(522, 119);
            this.btnStaffPass.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnStaffPass.Name = "btnStaffPass";
            this.btnStaffPass.Size = new System.Drawing.Size(182, 47);
            this.btnStaffPass.TabIndex = 12;
            this.btnStaffPass.Text = "Change Password";
            this.btnStaffPass.UseVisualStyleBackColor = false;
            this.btnStaffPass.Click += new System.EventHandler(this.btnStaffPass_Click);
            // 
            // btnStaffLog
            // 
            this.btnStaffLog.AutoSize = true;
            this.btnStaffLog.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnStaffLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStaffLog.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffLog.ForeColor = System.Drawing.Color.Transparent;
            this.btnStaffLog.Location = new System.Drawing.Point(745, 118);
            this.btnStaffLog.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnStaffLog.Name = "btnStaffLog";
            this.btnStaffLog.Size = new System.Drawing.Size(182, 47);
            this.btnStaffLog.TabIndex = 13;
            this.btnStaffLog.Text = "Log out";
            this.btnStaffLog.UseVisualStyleBackColor = false;
            this.btnStaffLog.Click += new System.EventHandler(this.btnStaffLog_Click);
            // 
            // lblStaffName
            // 
            this.lblStaffName.AutoSize = true;
            this.lblStaffName.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStaffName.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblStaffName.Location = new System.Drawing.Point(1141, 46);
            this.lblStaffName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStaffName.Name = "lblStaffName";
            this.lblStaffName.Size = new System.Drawing.Size(126, 22);
            this.lblStaffName.TabIndex = 9;
            this.lblStaffName.Text = "EmployeeName";
            // 
            // lblStaffWelcome
            // 
            this.lblStaffWelcome.AutoSize = true;
            this.lblStaffWelcome.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStaffWelcome.Location = new System.Drawing.Point(1039, 46);
            this.lblStaffWelcome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStaffWelcome.Name = "lblStaffWelcome";
            this.lblStaffWelcome.Size = new System.Drawing.Size(79, 22);
            this.lblStaffWelcome.TabIndex = 8;
            this.lblStaffWelcome.Text = "Welcome";
            // 
            // lblStaffTitle
            // 
            this.lblStaffTitle.AutoSize = true;
            this.lblStaffTitle.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStaffTitle.Location = new System.Drawing.Point(78, 34);
            this.lblStaffTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStaffTitle.Name = "lblStaffTitle";
            this.lblStaffTitle.Size = new System.Drawing.Size(424, 37);
            this.lblStaffTitle.TabIndex = 0;
            this.lblStaffTitle.Text = "Attendance Management System";
            // 
            // MenuBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1453, 1053);
            this.Controls.Add(this.AdminPanel);
            this.Controls.Add(this.StaffPanel);
            this.MaximizeBox = false;
            this.Name = "MenuBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            this.Load += new System.EventHandler(this.MenuBar_Load);
            this.AdminPanel.ResumeLayout(false);
            this.AdminPanel.PerformLayout();
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel AdminPanel;
        private System.Windows.Forms.Label lblAdminName;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblStaffName;
        private System.Windows.Forms.Label lblStaffWelcome;
        private System.Windows.Forms.Label lblStaffTitle;
        protected System.Windows.Forms.Button btnMyAccount;
        protected System.Windows.Forms.Button btnHoliday;
        protected System.Windows.Forms.Button btnChangePassword;
        protected System.Windows.Forms.Button btnLogOut;
        protected System.Windows.Forms.Button btnEmployee;
        protected System.Windows.Forms.Button btnLeave;
        protected System.Windows.Forms.Button btnStaffProfile;
        protected System.Windows.Forms.Button btnAttendance;
        protected System.Windows.Forms.Button btnStaffPass;
        protected System.Windows.Forms.Button btnStaffLog;
        protected System.Windows.Forms.Panel StaffPanel;
    }
}