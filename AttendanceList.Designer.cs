﻿using System.Windows.Forms;

namespace Attendance_Management_System
{
    partial class AttendanceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvAttendanceList = new System.Windows.Forms.DataGridView();
            this.datevalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Invalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Outvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Late = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Early = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAttendanceList = new System.Windows.Forms.Label();
            this.btnBackward = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.StaffPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttendanceList)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAttendanceList
            // 
            this.dgvAttendanceList.AllowUserToAddRows = false;
            this.dgvAttendanceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAttendanceList.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAttendanceList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAttendanceList.ColumnHeadersHeight = 40;
            this.dgvAttendanceList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.datevalue,
            this.dayName,
            this.Invalue,
            this.Outvalue,
            this.Late,
            this.Early,
            this.Reason});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAttendanceList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAttendanceList.EnableHeadersVisualStyles = false;
            this.dgvAttendanceList.Location = new System.Drawing.Point(89, 290);
            this.dgvAttendanceList.Name = "dgvAttendanceList";
            this.dgvAttendanceList.ReadOnly = true;
            this.dgvAttendanceList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvAttendanceList.RowHeadersVisible = false;
            this.dgvAttendanceList.RowHeadersWidth = 51;
            this.dgvAttendanceList.RowTemplate.Height = 30;
            this.dgvAttendanceList.RowTemplate.ReadOnly = true;
            this.dgvAttendanceList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAttendanceList.Size = new System.Drawing.Size(1236, 1207);
            this.dgvAttendanceList.TabIndex = 0;
            this.dgvAttendanceList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAttendanceList_CellContentClick);
            this.dgvAttendanceList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvAttendanceList_CellPainting);
            this.dgvAttendanceList.Paint += new System.Windows.Forms.PaintEventHandler(this.dgvAttendanceList_Paint);
           
            // 
            // datevalue
            // 
            this.datevalue.FillWeight = 71.03919F;
            this.datevalue.HeaderText = "MM/DD";
            this.datevalue.MinimumWidth = 6;
            this.datevalue.Name = "datevalue";
            this.datevalue.ReadOnly = true;
            this.datevalue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dayName
            // 
            this.dayName.FillWeight = 115.3846F;
            this.dayName.HeaderText = "Day";
            this.dayName.MinimumWidth = 6;
            this.dayName.Name = "dayName";
            this.dayName.ReadOnly = true;
            this.dayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Invalue
            // 
            this.Invalue.FillWeight = 71.03919F;
            this.Invalue.HeaderText = "In";
            this.Invalue.MinimumWidth = 6;
            this.Invalue.Name = "Invalue";
            this.Invalue.ReadOnly = true;
            this.Invalue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Outvalue
            // 
            this.Outvalue.FillWeight = 71.03919F;
            this.Outvalue.HeaderText = "Out";
            this.Outvalue.MinimumWidth = 6;
            this.Outvalue.Name = "Outvalue";
            this.Outvalue.ReadOnly = true;
            this.Outvalue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Late
            // 
            this.Late.FillWeight = 101.9701F;
            this.Late.HeaderText = "Late In/Out";
            this.Late.MinimumWidth = 6;
            this.Late.Name = "Late";
            this.Late.ReadOnly = true;
            this.Late.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Early
            // 
            this.Early.FillWeight = 108.0749F;
            this.Early.HeaderText = "Early In/Out";
            this.Early.MinimumWidth = 6;
            this.Early.Name = "Early";
            this.Early.ReadOnly = true;
            this.Early.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Reason
            // 
            this.Reason.FillWeight = 161.4527F;
            this.Reason.HeaderText = "Reason";
            this.Reason.MinimumWidth = 6;
            this.Reason.Name = "Reason";
            this.Reason.ReadOnly = true;
            this.Reason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lblAttendanceList
            // 
            this.lblAttendanceList.AutoSize = true;
            this.lblAttendanceList.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttendanceList.Location = new System.Drawing.Point(544, 211);
            this.lblAttendanceList.Name = "lblAttendanceList";
            this.lblAttendanceList.Size = new System.Drawing.Size(138, 24);
            this.lblAttendanceList.TabIndex = 1;
            this.lblAttendanceList.Text = "Attendance List";
            // 
            // btnBackward
            // 
            this.btnBackward.BackColor = System.Drawing.Color.White;
            this.btnBackward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackward.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackward.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.btnBackward.Location = new System.Drawing.Point(348, 203);
            this.btnBackward.Name = "btnBackward";
            this.btnBackward.Size = new System.Drawing.Size(85, 40);
            this.btnBackward.TabIndex = 2;
            this.btnBackward.Text = "<<";
            this.btnBackward.UseVisualStyleBackColor = false;
            this.btnBackward.Click += new System.EventHandler(this.btnBackward_Click);
            // 
            // btnForward
            // 
            this.btnForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForward.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnForward.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.btnForward.Location = new System.Drawing.Point(869, 203);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(85, 40);
            this.btnForward.TabIndex = 3;
            this.btnForward.Text = ">>";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // AttendanceList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1487, 1055);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.lblAttendanceList);
            this.Controls.Add(this.btnBackward);
            this.Controls.Add(this.dgvAttendanceList);
            this.MinimizeBox = false;
            this.Name = "AttendanceList";
            this.Text = "Attendance List";
            this.Load += new System.EventHandler(this.AttendanceList_Load);
            this.Controls.SetChildIndex(this.StaffPanel, 0);
            this.Controls.SetChildIndex(this.dgvAttendanceList, 0);
            this.Controls.SetChildIndex(this.btnBackward, 0);
            this.Controls.SetChildIndex(this.lblAttendanceList, 0);
            this.Controls.SetChildIndex(this.btnForward, 0);
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttendanceList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAttendanceList;
        private System.Windows.Forms.Label lblAttendanceList;
        private System.Windows.Forms.Button btnBackward;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.DataGridViewTextBoxColumn datevalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Outvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Late;
        private System.Windows.Forms.DataGridViewTextBoxColumn Early;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reason;
    }
}