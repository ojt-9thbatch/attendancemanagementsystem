﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace Attendance_Management_System
{
    public partial class AttendanceList : MenuBar
    {
        SqlConnection conn;
        string month;
        string year;
        string days;
        string date; // var used to check weekends, holiday or current day
        string dates; // var used to create datagridviewrow according to months
        string recDates;// var used to display MM/dd in first column of datagridview

        DateTime Current_Date = DateTime.Today; // calendar month to show current month
        DataTable holiday = new DataTable(); 

        TextBox txtIn = new TextBox();
        TextBox txtOut = new TextBox();
        TextBox txtReason = new TextBox();

        string empID = Login.employeeId; // get employee id from Login form 
        string holidayDate;
        public AttendanceList()
        {
            InitializeComponent();
            btnAttendance.BackColor = Color.FromArgb(151, 242, 0);

            DataGridViewLinkColumn btnEdit = new DataGridViewLinkColumn();
            DataGridViewLinkColumn btnSave = new DataGridViewLinkColumn();
            DataGridViewTextBoxColumn orgAttDate = new DataGridViewTextBoxColumn();
            
            btnEdit.HeaderText = "Action";
            btnEdit.Text = "Edit";
            btnEdit.UseColumnTextForLinkValue = true;

            btnSave.Text = "Save";
            btnSave.UseColumnTextForLinkValue= true;

            orgAttDate.HeaderText = "OrgAttDate";
            orgAttDate.Visible = false;

            dgvAttendanceList.Columns.Add(btnEdit);
            dgvAttendanceList.Columns.Add(btnSave);
            dgvAttendanceList.Columns.Add(orgAttDate);
        }
        public void Connection()
        {
            conn = new SqlConnection(@"data source=TMPC-056; database=AttendanceDB; integrated security = true");
        }
        public void TransparentBtn()
        {
            btnBackward.FlatStyle = FlatStyle.Flat;
            btnBackward.FlatAppearance.BorderSize = 0;
            btnForward.FlatStyle = FlatStyle.Flat;
            btnForward.FlatAppearance.BorderSize = 0;
        }
        /// <summary>
        /// day_Color method to set the desired weekends, current date and holiday color.
        /// </summary>
        public void day_Color()
        {
            Connection();
            conn.Open();
            // get holiday date from database and save it in datatable --> holiday
            string selectHolidayDateByAttDate = @"select holiday_date from t_holiday where month(holiday_date)=@month and year(holiday_date)=@year";
            SqlCommand holidayCmd = new SqlCommand(selectHolidayDateByAttDate, conn);

            holidayCmd.Parameters.AddWithValue("@month", Convert.ToDateTime(Current_Date).ToString("MM"));
            holidayCmd.Parameters.AddWithValue("@year", Convert.ToDateTime(Current_Date).ToString("yyyy"));
            SqlDataReader holiday_Read = holidayCmd.ExecuteReader();

            while (holiday_Read.Read())
            {
                holiday.Rows.Add(holiday_Read.GetValue(0));
            }

            holiday_Read.Close();
            conn.Close();

            foreach (DataGridViewRow row in dgvAttendanceList.Rows)// loop the whole datagridview to check which day is weekends, today and holiday
            {
                days = row.Cells[1].Value.ToString(); // retrieve day of week(mon-sun) from dgvAttendanceList.
                date = row.Cells[0].Value.ToString(); // retrieve MM/dd from datagridview
                                                      // if days(mon-sun) is weekends , set cell back-color to light gray
                if (days == "Saturday" || days == "Sunday")
                {
                    row.DefaultCellStyle.BackColor = Color.LightGray;
                }
                // if date(MM/dd) in datagridview is current date ,set color to Green
                if (date == DateTime.Today.ToString("MM/dd"))
                {
                    row.DefaultCellStyle.BackColor = Color.GreenYellow;
                }
                foreach (DataRow Holi_Row in holiday.Rows) //loop datatable -->holiday's row to check holiday date
                {
                    holidayDate = Convert.ToDateTime(Holi_Row[0]).ToString("MM/dd");
                    // if date is holiday, set back-color to Gray
                    if (date == holidayDate)
                    {
                        row.DefaultCellStyle.BackColor = Color.Gray;
                    }
                }
            }
        }
        /// <summary>
        /// Load_AttData method : to construct blank row according to month and set data from database
        /// </summary>
        public void Load_AttData()
        {
            month = Convert.ToDateTime(Current_Date).ToString("MM");
            year = Convert.ToDateTime(Current_Date).ToString("yyyy");
            Connection();

            // DataGridViewCellStyle for hiding column link in empty cell
            DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
            dataGridViewCellStyle.Padding = new Padding(0, 0, 1000, 0);

            dgvAttendanceList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvAttendanceList.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10);
            dgvAttendanceList.ColumnHeadersHeight = 35;

            lblAttendanceList.Text = year + "/" + month + "  Attendance List";
            // create blank datagridview row with how many days are in current month
            dgvAttendanceList.RowCount = DateTime.DaysInMonth(Convert.ToInt16(year), Convert.ToInt16(month));

            for (int i = 1; i <= dgvAttendanceList.RowCount; i++)
            {
                // current year and month
                dates = year + "/" + month + "/" + i;
                // reconstruct to datetime format becuz we need dd to display in datagridview
                recDates = Convert.ToDateTime(dates).ToString("yyyy/MM/dd");
                // display MM/dd in first column of datagridview
                dgvAttendanceList.Rows[i - 1].Cells[0].Value = Convert.ToDateTime(dates).ToString("MM/dd");
                // display (mon- sun) in second column of datagridview
                dgvAttendanceList.Rows[i - 1].Cells[1].Value = Convert.ToDateTime(recDates).ToString("dddd");

            }
            day_Color();
            string selectAttendanceList = @"select * from t_att_daily where emp_id=@emp_id and month(att_date)=@month and year(att_date)=@year";
            SqlCommand cmd = new SqlCommand(selectAttendanceList, conn);
            conn.Open();

            cmd.Parameters.AddWithValue("@emp_id", empID);
            cmd.Parameters.AddWithValue("@month", month);
            cmd.Parameters.AddWithValue("@year", year);
            SqlDataReader reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {
                foreach (DataGridViewRow row in dgvAttendanceList.Rows)
                {
                    // add database data to each related month and day in blank gridview
                    if (Convert.ToDateTime(row.Cells[0].Value).ToString("dd").Equals(Convert.ToDateTime(reader[2]).ToString("dd")))
                    {
                        row.Cells[2].Value = ((TimeSpan)reader[3]).ToString(@"hh\:mm");
                        row.Cells[3].Value = ((TimeSpan)reader[4]).ToString(@"hh\:mm");
                        row.Cells[4].Value = ((TimeSpan)reader[5]).ToString(@"hh\:mm") + "/" + ((TimeSpan)reader[6]).ToString(@"hh\:mm");
                        row.Cells[5].Value = ((TimeSpan)reader[7]).ToString(@"hh\:mm") + "/" + ((TimeSpan)reader[8]).ToString(@"hh\:mm");
                        row.Cells[6].Value = reader[9].ToString();
                        row.Cells[9].Value = Convert.ToDateTime(reader[2]).ToString("yyyy-MM-dd");
                    }
                }
            }
            reader.Close();
            // check which cell is empty cell in datagridview
            for( int nullCount = 0; nullCount<dgvAttendanceList.Rows.Count; nullCount++ )
            {
                // hide edit and save link for empty datagridview cell.
                if (dgvAttendanceList.Rows[nullCount].Cells[2].Value == null)
                {
                    dgvAttendanceList.Rows[nullCount].Cells[7].Style = dataGridViewCellStyle;
                    dgvAttendanceList.Rows[nullCount].Cells[8].Style = dataGridViewCellStyle;
                }
            }
            conn.Close();
        }
        private void AttendanceList_Load(object sender, EventArgs e)
        {
            // set form display from menu bar
            btnAttendance.Select();
            TransparentBtn();
            //add HolidayDate column to holiday datatable
            holiday.Columns.Add("HolidayDate");
            dgvAttendanceList.AutoGenerateColumns = false;
            dgvAttendanceList.AllowUserToAddRows = false;
            dgvAttendanceList.AllowUserToResizeColumns = false;
            dgvAttendanceList.BorderStyle = BorderStyle.None;
            
            foreach (DataGridViewColumn column in dgvAttendanceList.Columns)
            {
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            Load_AttData();
        }
        private void btnBackward_Click(object sender, EventArgs e)
        {
            Current_Date = Current_Date.AddMonths(-1);
            dgvAttendanceList.Rows.Clear();
            Load_AttData();
        }
        private void btnForward_Click(object sender, EventArgs e)
        {
            Current_Date = Current_Date.AddMonths(+1);
            dgvAttendanceList.Rows.Clear();
            Load_AttData();
        }


        //declared a variable to check the row of clicked edit buttons
        int clickedEditRow;
        private void dgvAttendanceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Connection();
            // declared two timespan variables for standard in time and out time
            TimeSpan standardInTimeSpan = TimeSpan.Parse("08:00");
            TimeSpan standardOutTimeSpan = TimeSpan.Parse("17:00");

            // set default timespan to 00:00:00
            TimeSpan lateInTimeSpan = TimeSpan.Zero;
            TimeSpan earlyInTimeSpan = TimeSpan.Zero;
            TimeSpan lateOutTimeSpan = TimeSpan.Zero;
            TimeSpan earlyOutTimeSpan = TimeSpan.Zero;
            TimeSpan inTimeSpan = TimeSpan.Zero;
            TimeSpan outTimeSpan = TimeSpan.Zero;

            try
            {
                // when edit link button is clicked...
                if (dgvAttendanceList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Edit"))
                {
                    //
                    clickedEditRow = e.RowIndex;
                    dgvAttendanceList.Controls.Add(txtIn);
                    dgvAttendanceList.Controls.Add(txtOut);
                    dgvAttendanceList.Controls.Add(txtReason);

                    dgvAttendanceList.FirstDisplayedScrollingRowIndex = e.RowIndex;
                    // create textbox txtIn

                    txtIn.Visible = true;

                    txtIn.Text = dgvAttendanceList.Rows[e.RowIndex].Cells[2].Value.ToString();

                    txtIn.TextAlign = HorizontalAlignment.Center;

                    txtIn.Multiline = true;

                    txtIn.Height = dgvAttendanceList.RowTemplate.Height;

                    txtIn.Location = dgvAttendanceList.GetCellDisplayRectangle(2, e.RowIndex, false).Location;

                    txtIn.Size = dgvAttendanceList.GetCellDisplayRectangle(2, e.RowIndex, false).Size;

                    txtIn.Font = new Font("Calibri", 12);

                    txtIn.Focus();

                    txtIn.BringToFront();

                    // create textbox txtout 

                    txtOut.Visible = true;

                    txtOut.Text = dgvAttendanceList.Rows[e.RowIndex].Cells[3].Value.ToString();

                    txtOut.TextAlign = HorizontalAlignment.Center;

                    txtOut.Multiline = true;

                    txtOut.Height = dgvAttendanceList.RowTemplate.Height;

                    txtOut.Location = dgvAttendanceList.GetCellDisplayRectangle(3, e.RowIndex, false).Location;

                    txtOut.Size = dgvAttendanceList.GetCellDisplayRectangle(3, e.RowIndex, false).Size;

                    txtOut.Font = new Font("Calibri", 12);

                    txtOut.BringToFront();

                    // create textbox txtReason 

                    txtReason.Visible = true;

                    txtReason.Text = dgvAttendanceList.Rows[e.RowIndex].Cells[6].Value.ToString();

                    txtReason.TextAlign = HorizontalAlignment.Center;

                    txtReason.Multiline = true;

                    txtReason.Height = dgvAttendanceList.RowTemplate.Height;

                    txtReason.Location = dgvAttendanceList.GetCellDisplayRectangle(6, e.RowIndex, false).Location;

                    txtReason.Size = dgvAttendanceList.GetCellDisplayRectangle(6, e.RowIndex, false).Size;

                    txtReason.Font = new Font("Calibri", 12);

                    txtReason.BringToFront();

                }
                else if (dgvAttendanceList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Save") && e.RowIndex == clickedEditRow)
                {
                    dgvAttendanceList.FirstDisplayedScrollingRowIndex = e.RowIndex;

                    // when save link button is clicked && match with edit button row ...

                    // declared 'endofday' to use as 24:00:00
                    TimeSpan EndOfDay = TimeSpan.FromHours(24);

                    string inTime = txtIn.Text.Trim().ToString();
                    string outTime = txtOut.Text.Trim().ToString();

                    // if 1 of 3 textboxes is empty ...
                    if (!(inTime.Equals("")) && !(outTime.Equals("")) && !(txtReason.Text.Equals("")))
                    {
                        // regular expression for 24-hr timeFormat = @"hh:mm"
                        Regex reg_Exp = new Regex(@"^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$");  //(@"^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$");

                        // if textbox text is timeformat or not
                        if (reg_Exp.IsMatch(inTime) && reg_Exp.IsMatch(outTime))
                        {
                            // if in time is earlier than out time (-1) ==> Correct
                            if (inTime.CompareTo(outTime) == -1 || outTime.Equals("00:00"))
                            {
                                /**************In Time Calculation**************/

                                inTimeSpan = TimeSpan.Parse(inTime);
                                outTimeSpan = TimeSpan.Parse(outTime);

                                // if in time is greater than standard in time
                                if (inTimeSpan > standardInTimeSpan)
                                {
                                    // if late , substract standard in time from in time and add to lateInTimeSpan
                                    lateInTimeSpan = inTimeSpan - standardInTimeSpan;
                                }
                                else if (inTimeSpan < standardInTimeSpan)
                                {
                                    // If early , substract in time from standard in time and add to earlyInTimeSpan
                                    earlyInTimeSpan = standardInTimeSpan - inTimeSpan;
                                }
                                else
                                {
                                    // if there is no early and late in time, set in time "00:00"
                                    lateInTimeSpan = TimeSpan.Parse("00:00");
                                    earlyInTimeSpan = TimeSpan.Parse("00:00");
                                }

                                /**************Out Time Calculation**************/
                                // if time is 00:00, change to 24:00 and calculate the late out
                                //if it is not , do the regular calculation4
                                if (outTime.Equals("00:00"))
                                {
                                    lateOutTimeSpan = EndOfDay - standardOutTimeSpan;
                                }
                                else
                                {
                                    // if out time is greater than standard out time
                                    if (outTimeSpan > standardOutTimeSpan)
                                    {
                                        // If late ==> substract standard out time from out time and add to lateOutTimeSpan
                                        lateOutTimeSpan = outTimeSpan - standardOutTimeSpan;
                                    }
                                    else if (outTimeSpan < standardOutTimeSpan)
                                    {
                                        // If early ==> substract out time from standard out time and add to earlyOutTimeSpan
                                        earlyOutTimeSpan = standardOutTimeSpan - outTimeSpan;
                                    }
                                    else
                                    {
                                        // if there is no early and late out time ==> set out time "00:00"
                                        lateOutTimeSpan = TimeSpan.Zero;
                                        lateInTimeSpan = TimeSpan.Zero;
                                    }
                                }

                                string testUpdateQuery = @"update t_att_daily set att_in = @in, att_out = @out, late_in = @latein, late_out = @lateout, early_in = @earlyin, early_out = @earlyout, reason = @reasontext where emp_id = '" + empID + "' and att_date = '" + Convert.ToDateTime(dgvAttendanceList.Rows[e.RowIndex].Cells[9].Value) + "'";
                                conn.Open();

                                SqlCommand updateCmd = new SqlCommand(testUpdateQuery, conn);

                                updateCmd.Parameters.AddWithValue("@in", inTimeSpan);
                                updateCmd.Parameters.AddWithValue("@out", outTimeSpan);
                                updateCmd.Parameters.AddWithValue("@latein", lateInTimeSpan);
                                updateCmd.Parameters.AddWithValue("@lateout", lateOutTimeSpan);
                                updateCmd.Parameters.AddWithValue("@earlyin", earlyInTimeSpan);
                                updateCmd.Parameters.AddWithValue("@earlyout", earlyOutTimeSpan);
                                updateCmd.Parameters.AddWithValue("@reasontext", txtReason.Text.Trim());

                                updateCmd.ExecuteNonQuery();
                                conn.Close();

                                // Show successful alert
                                MessageBox.Show("Attendance data is updated successfully"); // Msg009

                                // after save process is finished, textbox will be closed;
                                txtIn.Visible = false;
                                txtOut.Visible = false;
                                txtReason.Visible = false;

                                // dgvAttendanceEntry.DataSource = null;
                                // and updated database data will display again ...
                                Load_AttData();
                            }
                            else
                            {
                                MessageBox.Show("The In time must be smaller than Out time"); // Error056
                            }
                        }
                        else
                        {
                            MessageBox.Show("The input must be Time format (eg. 11:00)"); // Error054
                        }
                    }
                    else
                    {
                        MessageBox.Show("The input must not be Empty"); // Error055
                    }
                }
                else
                {
                    MessageBox.Show("Please Click Edit First"); // Error057
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dgvAttendanceList_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // remove border between two datagridview column link
            if (e.ColumnIndex == 7)
            {
                e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            }
        }
        // merge two column headers of column link cell
        private void dgvAttendanceList_Paint(object sender, PaintEventArgs e)
        {
            //Offsets to adjust the position of the merged Header.
            int heightOffset = 1;
            int widthOffset = -2;
            int xOffset = 0;
            int yOffset = -3;

            //Index of Header column from where the merging will start.
            int columnIndex = 7;

            //Number of Header columns to be merged.
            int columnCount = 2;

            //Get the position of the Header Cell.
            Rectangle headerCellRectangle = dgvAttendanceList.GetCellDisplayRectangle(columnIndex, 0, true);

            //X coordinate  of the merged Header Column.
            int xCord = headerCellRectangle.Location.X + xOffset;

            //Y coordinate  of the merged Header Column.
            int yCord = headerCellRectangle.Location.Y - headerCellRectangle.Height + yOffset;

            //Calculate Width of merged Header Column by adding the widths of all Columns to be merged.
            int mergedHeaderWidth = dgvAttendanceList.Columns[columnIndex].Width + dgvAttendanceList.Columns[columnIndex + columnCount - 1].Width + widthOffset;

            //Generate the merged Header Column Rectangle.
            Rectangle mergedHeaderRect = new Rectangle(xCord, yCord, mergedHeaderWidth, headerCellRectangle.Height + heightOffset);

            //Draw the merged Header Column Rectangle.
            e.Graphics.FillRectangle(new SolidBrush(Color.White), mergedHeaderRect);
            
            Font drawFont = new Font("Work Sans", 10);
            //Draw the merged Header Column Text.
            e.Graphics.DrawString("Action", drawFont, Brushes.Black, xCord + 80, yCord + 9);
            
        }
    }
}
