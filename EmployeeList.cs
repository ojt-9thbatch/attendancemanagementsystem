﻿using AttendanceManagementSystem;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class EmployeeList : MenuBar
    {   

        SqlConnection conn;
        DataTable dataTableCompleteData = new DataTable();
        DataTable dataTableSearchData = new DataTable();

        private int noOfDataPerPage = 5;
        public static int currentPageIndex = 1;
        private int totalPages = 1;
        private string[] paginBtnArray = { "0", "0", "0", "0", "0", "0", "0" };
        string emp_id;

        public EmployeeList()
        {
            InitializeComponent();
            btnEmployee.BackColor = Color.FromArgb(151, 242, 0);
        }
        public void connection()
        {
            conn = new SqlConnection(@"data source=TMPC-059\SQLEXPRESS; Database=AttendanceDB; integrated security = true");
        }
        public string _btnAdd
        {
            get { return btnAddEmployee.Text; }
        }
        public string _btnEdit
        {
            get { return "Edit"; }
        }

        private void EmployeeList_Load(object sender, EventArgs e)
        {
             //lblName.Text = LogIn.employeeName;

            // set search text to focus
            txtSearch.Focus();
            txtSearch.TabIndex = 0;
            DataBind();
            // Add this line to ensure "Edit" link buttons are added
            showData();
            // set total number pages
            setTotalPages(dataTableCompleteData.Rows.Count);
            foreach (DataGridViewColumn col in dgvEmployeeList.Columns)
            {
               // Set the SortMode property to NotSortable
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
                ;
            }

            dgvEmployeeList.DefaultCellStyle.Font = new Font("Calibri", 12);

            dgvEmployeeList.AllowUserToAddRows = false;

            //// Set the border style of the DataGridView to None
            dgvEmployeeList.BorderStyle = BorderStyle.None;
            addEditBtn();
            // cell selection color for gridview
            dgvEmployeeList.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvEmployeeList.DefaultCellStyle.SelectionForeColor = Color.Black;
          
            updatePaginBtn();
        }

        public void DataBind()
        {
            connection(); // Establish database connection
            try
            {
                // Open the database connection
                conn.Open();

                // Query string to select employee information
                string selectEmployeeInfo = @"
 -- Select employee information
    select 
    empInfo.emp_id as 'Employee ID',                                                                     -- Employee ID
    empInfo.emp_name as 'Name',                                                                          -- Employee Name
    case 
    when empInfo.emp_gender = '1' then 'M'                                                               -- Male if gender is 1
    else 'F'                                                                                             -- Female otherwise
    end as 'Gender', -- Employee gender
    empInfo.emp_dob as 'Date of Birth',                                                                  -- Employee Date of Birth
    case
    when empWorkExp.totalMonths is null then '-'                                                         -- If no work experience, display '-'
    else 
    case 
    when empWorkExp.totalMonths < 12 then concat(empWorkExp.totalMonths, ' months')                      -- If less than a year, display only months
    else 
    case 
    when empWorkExp.totalMonths % 12 = 0 then concat(empWorkExp.totalMonths / 12, ' yrs')                -- If only whole years, display years
    else concat(floor(empWorkExp.totalMonths / 12), ' yrs ', empWorkExp.totalMonths % 12, ' months')     -- If a year or more, display years and month                
    end
    end
    end as 'Prev Work Exp',                                                                              -- Previous Work Experience
         coalesce(jpskill.emp_jpskill_level_name, '-') as 'JLPT Maximum Level'                           -- JLPT Maximum Level, display '-' if not available
    from 
         t_emp_basic_info as empInfo -- Employee Basic Information
    left join (
                                                                                                         -- Join to calculate total months of work experience
    select 
    emp_id,
    sum(
    case 
    when month(emp_comp_to) = month(getdate()) and year(emp_comp_to) = year(getdate()) then
    datediff(month, emp_comp_from, getdate())                                                            -- Calculate experience until current date if currently employed
    else 
    datediff(month, emp_comp_from, emp_comp_to) 
    end
    ) as totalMonths
    from 
    t_emp_work_experience
    group by 
    emp_id
    ) as empWorkExp on empInfo.emp_id = empWorkExp.emp_id 
    left join (
                                                                                                         -- Join to get JLPT maximum level
    select 
    emp_id,
    emp_jpskill_level_name
    from 
    t_emp_jpkill
    where 
    emp_jpskill_maxlevel = '1'                                                                            -- Filter JLPT maximum level
    ) as jpskill on empInfo.emp_id = jpskill.emp_id
    ORDER BY empInfo.emp_id;";
                // Order by Employee ID

                // Execute the query and fill the DataTable with the results
                SqlDataAdapter da = new SqlDataAdapter(selectEmployeeInfo, conn);
                dataTableCompleteData.Clear(); // Clear existing data
                da.Fill(dataTableCompleteData);

                // Add a 'No.' column to the DataTable
                DataColumn col = dataTableCompleteData.Columns.Add("No.");
                col.SetOrdinal(0);

                // Set row numbers
                for (int i = 0; i < dataTableCompleteData.Rows.Count; i++)
                {
                    dataTableCompleteData.Rows[i][0] = i + 1;
                }

                // Bind the data to the DataGridView
                dgvEmployeeList.DataSource = dataTableCompleteData;
            }
            catch (Exception error)
            {
                Console.WriteLine("Error .... " + error.Message); // Print error message
            }
            finally
            {
                conn.Close(); // Close database connection
            }
        }
        private void setTotalPages(int rowCount)
        {
            // caculate number of pages
            int numberOfPages = rowCount / noOfDataPerPage;
            if (numberOfPages * noOfDataPerPage < rowCount)
            {
                totalPages = numberOfPages + 1;
            }
            else
            {
                totalPages = numberOfPages;
            }
        }
        //add EditButtonToGridView
        private void addEditBtn()
        {
            // Create instance for data gridview link column class
            DataGridViewLinkColumn edit = new DataGridViewLinkColumn();
            edit.Name = "edit";
            edit.UseColumnTextForLinkValue = true;
            edit.HeaderText = "Action";
            edit.DataPropertyName = "linkColumn";
            edit.ActiveLinkColor = Color.White;
            edit.LinkBehavior = LinkBehavior.SystemDefault;
            edit.LinkColor = Color.DodgerBlue;
            edit.Text = "Edit";
          

            // Add this link column to datagridview
            dgvEmployeeList.Columns.Add(edit);
            edit.SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        // show data to datagridview
        private void showData(string currentBtnText = "")
        {
            // check search box is null not
            if (!txtSearch.Text.Equals(""))
            {
                this.dgvEmployeeList.DataSource = GetCurrentRecords(currentPageIndex, dataTableSearchData);
            }
            else
            {
                this.dgvEmployeeList.DataSource = GetCurrentRecords(currentPageIndex, dataTableCompleteData);
            }

            // Center align data in cells
            foreach (DataGridViewColumn col in dgvEmployeeList.Columns)
            {
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            // Fill columns
            foreach (DataGridViewColumn col in dgvEmployeeList.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                // Set column header font and size
                col.HeaderCell.Style.Font = new Font("Calibri", 12, FontStyle.Regular);
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Ensure headers display in one line
                col.HeaderCell.Style.WrapMode = DataGridViewTriState.False;
                // Check if the column is "Date Of Birth"
                if (col.Name == "Date Of Birth")
                {
                    // Set the date format for the "Date Of Birth" column
                    col.DefaultCellStyle.Format = "yyyy/MM/dd";
                }
            }


          
            // Set the AutoSizeColumnsMode property to None to prevent automatic resizing
            dgvEmployeeList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            // Set the width of specific columns
            dgvEmployeeList.Columns["No."].Width = 50;
            dgvEmployeeList.Columns["Employee ID"].Width = 110; // Adjust the width as needed
            dgvEmployeeList.Columns["Name"].Width = 120; // Adjust the width as needed
            dgvEmployeeList.Columns["Gender"].Width = 70; // Adjust the width as needed
            dgvEmployeeList.Columns["Date Of Birth"].Width = 120; // Adjust the width as needed
            dgvEmployeeList.Columns["Prev Work Exp"].Width = 100; // Adjust the width as needed
            dgvEmployeeList.Columns["JLPT Maximum Level"].Width = 190; // Adjust the width as needed

            

            // Set DataGridView read-only
            dgvEmployeeList.ReadOnly = true;

            // Set the format for the "Date Of Birth" column
            dgvEmployeeList.Columns["Date Of Birth"].DefaultCellStyle.Format = "yyyy/MM/dd";
          
        }
        // get data fron data table
        private DataTable GetCurrentRecords(int page, DataTable dataTable)
        {
            DataTable resultDataTable = new DataTable("EmployeeInfo");

            try
            {
                // Determine total row count
                int rowCount = dataTable.Rows.Count;

                // Define columns for the result DataTable
                DefineColumns(resultDataTable);

                // Calculate limits for data retrieval
                int startIndex = (page - 1) * noOfDataPerPage;
                int endIndex = Math.Min(startIndex + noOfDataPerPage, rowCount);

                // Populate result DataTable with limited data
                for (int i = startIndex; i < endIndex; i++)
                {
                    DataRow newRow = resultDataTable.NewRow();

                    newRow["No."] = dataTable.Rows[i].Field<object>("No.");
                    newRow["Employee ID"] = dataTable.Rows[i].Field<object>("Employee ID");
                    newRow["Name"] = dataTable.Rows[i].Field<object>("Name");
                    newRow["Gender"] = dataTable.Rows[i].Field<object>("Gender");
                    newRow["Date Of Birth"] = dataTable.Rows[i].Field<DateTime>("Date Of Birth").ToString("yyyy/MM/dd");
                    newRow["Prev Work Exp"] = dataTable.Rows[i].Field<object>("Prev Work Exp");
                    newRow["JLPT Maximum Level"] = dataTable.Rows[i].Field<object>("JLPT Maximum Level");

                    resultDataTable.Rows.Add(newRow);


                }
              

                // show number of data
                showNumberOfData(dataTableCompleteData.Rows.Count, noOfDataPerPage);
            }
            catch (Exception error)
            {
                MessageBox.Show("Error: " + error.Message);
            }

            return resultDataTable;
        }

        // show number of data
        private void showNumberOfData(int total, int dataPerPage)
        {
            lblShowRecord.Text = total + " results (show " + dataPerPage + " records per page)";
        }

        private void DefineColumns(DataTable dataTable)
        {
            // Define columns for the result DataTable
            dataTable.Columns.Add("No.");
            dataTable.Columns.Add("Employee ID");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("Gender");
            dataTable.Columns.Add("Date Of Birth", typeof(DateTime));
            dataTable.Columns.Add("Prev Work Exp");
            dataTable.Columns.Add("JLPT Maximum Level");
        }
        private string BuildFilterExpression(string searchText)
        {
            // Build a filter expression to search across multiple columns
            return $"[Employee ID] LIKE '%{searchText}%' OR " +
                   $"[Name] LIKE '%{searchText}%' OR " +
                   $"[Gender] LIKE '%{searchText}%' OR " +
                   $"CONVERT([Date Of Birth], System.String) LIKE '%{searchText}%' OR " +
                   $"[Prev Work Exp] LIKE '%{searchText}%' OR " +
                   $"[JLPT Maximum Level] LIKE '%{searchText}%'";
        }

        private void ResetSearch()
        {
            txtSearch.Text = "";
            dataTableSearchData.Clear();
            setTotalPages(dataTableCompleteData.Rows.Count);
            currentPageIndex = 1;
            dgvEmployeeList.DataSource = GetCurrentRecords(currentPageIndex, dataTableCompleteData);
            updatePaginBtn();
            
        }

        private void updatePaginBtn()
        {
            // check all data  show in first page
            if (totalPages <= 1)
            {
                // hide panel (pagination buttons)
                pPagination.Visible = false;
            }
            else
            {
                // show panel (pagination buttons)
                pPagination.Visible = true;

                // check current page is first page
                if (currentPageIndex <= 1)
                {
                    paginBtnArray[6] = ">>";
                    paginBtnArray[5] = ">";
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";

                    if (totalPages == 2)
                    {
                        paginBtnArray[4] = "2";
                        paginBtnArray[3] = "1";
                        paginBtnArray[2] = "0";
                    }
                    else
                    {
                        paginBtnArray[4] = "3";
                        paginBtnArray[3] = "2";
                        paginBtnArray[2] = "1";
                    }
                }
                // check current page is last page
                else if (currentPageIndex == totalPages)
                {
                    paginBtnArray[6] = (currentPageIndex).ToString();
                    paginBtnArray[5] = (currentPageIndex - 1).ToString();
                    paginBtnArray[4] = (currentPageIndex - 2).ToString();
                    paginBtnArray[3] = "<";
                    paginBtnArray[2] = "<<";
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";
                    if (currentPageIndex - 2 == 0)
                    {
                        paginBtnArray[4] = "<";
                        paginBtnArray[3] = "<<";
                        paginBtnArray[2] = "0";
                    }
                }
                else
                {
                    paginBtnArray[6] = ">>";
                    paginBtnArray[5] = ">";
                    paginBtnArray[4] = (currentPageIndex + 1).ToString();
                    paginBtnArray[3] = (currentPageIndex).ToString();
                    paginBtnArray[2] = (currentPageIndex - 1).ToString();
                    paginBtnArray[1] = "0";
                    paginBtnArray[0] = "0";

                    if (currentPageIndex - 1 >= 1)
                    {
                        paginBtnArray[1] = "<";
                        paginBtnArray[0] = "<<";
                    }
                }
            }
            setPaginBtn();
        }

        // set pagination button
        private void setPaginBtn()
        {
            for (int i = 0; i < paginBtnArray.Length; i++)
            {
                if (!paginBtnArray[i].Equals("0"))
                {
                    // get all button from panel 
                    Button paginBtn = (Button)pPagination.Controls.Find("btn_" + (i + 1), true)[0];
                    paginBtn.Visible = true;

                    // set new text to pagin button
                    paginBtn.Text = paginBtnArray[i];

                    // change hightlight color to current page number button
                    if (currentPageIndex.ToString().Equals(paginBtnArray[i]))
                    {
                        paginBtn.BackColor = Color.LimeGreen;
                        paginBtn.ForeColor = Color.White;
                    }
                    else
                    {
                        paginBtn.BackColor = Color.DeepSkyBlue;
                        paginBtn.ForeColor = Color.White;
                    }
                }
                else
                {
                    // hide buttons that are not required
                    Button paginBtn = (Button)pPagination.Controls.Find("btn_" + (i + 1), true)[0];
                    paginBtn.Visible = false;
                }
            }
        }

        // for all paging buttons
        private void PaginationBtnClick(object sender, EventArgs e)
        {
            string btnText = (sender as Button).Text;

            if (btnText.Equals("<<"))
            {
                currentPageIndex = 1;
            }
            else if (btnText.Equals("<"))
            {
                currentPageIndex--;
            }
            else if (btnText.Equals(">"))
            {
                currentPageIndex++;
            }
            else if (btnText.Equals(">>"))
            {
                currentPageIndex = totalPages;
            }
            else
            {
                int pageNumber = int.Parse(btnText);
                currentPageIndex = pageNumber;
            }

            // Update pagination buttons
            updatePaginBtn();

            // Show data for the current page
            showData();
        }
        //for edit link button
        private void dgvEmployeeList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        
        {
            if (dgvEmployeeList.Columns[e.ColumnIndex].Name == "edit")
            {
                if (e.RowIndex >= 0)
                {
                    // Calculate the actual index of the cell containing emp_id based on the current page
                    int empIdColumnIndex = dgvEmployeeList.Columns["Employee ID"].Index;

                    // Get the value of emp_id from the correct cell
                    DataGridViewRow row = dgvEmployeeList.Rows[e.RowIndex];
                    emp_id = row.Cells[empIdColumnIndex].Value.ToString();

                    // Pass emp_id to the EmployeeRegistrationForm
                    EmployeeRegistrationForm EmpRegForm = new EmployeeRegistrationForm(emp_id);
                    EmpRegForm._btnParameters = _btnEdit;
                    EmpRegForm.Show();
                    this.Close();
                }
            }
        }

        // add employee button event
        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            EmployeeRegistrationForm EmpRegForm = new EmployeeRegistrationForm(null);
            EmpRegForm._btnParameters = _btnAdd;
            EmpRegForm.Show();
            this.Close();

        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar==(char)Keys.Enter)
            {
                DataView dataView = new DataView(dataTableCompleteData);
                string searchText = txtSearch.Text.Trim();

                // Reset search and display all data if the search text is empty
                if (string.IsNullOrEmpty(searchText))
                {
                    ResetSearch();
                    return;
                }

                // Attempt to parse the search text as a date
                if (DateTime.TryParse(searchText, out DateTime searchDate))
                {
                    // Filter the DataView to show rows where the 'Date Of Birth' column matches the search date
                    dataView.RowFilter = string.Format("[Date Of Birth] = #{0:yyyy/MM/dd}#", searchDate);
                }
                else
                {
                    // Filter the DataView based on multiple columns
                    string filterExpression = BuildFilterExpression(searchText);
                    dataView.RowFilter = filterExpression;
                }

                dataTableSearchData = dataView.ToTable();
                Debug.WriteLine("Filtered rows count: " + dataTableSearchData.Rows.Count);

                // Update total pages based on filtered data
                setTotalPages(dataTableSearchData.Rows.Count);
                // Update pagination buttons
                updatePaginBtn();
                // Show data for the current page
                showData();
                if (dataTableSearchData.Rows.Count == 0)
                {
                    MessageBox.Show("No Data Found!");

                    return;
                }

            }
        }
    }
}
