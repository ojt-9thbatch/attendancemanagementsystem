﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Attendance_Management_System
{
    public partial class Profile : MenuBar
    {
        // declare a sql connection
        SqlConnection conn;

        // declared state to control the click event of edit button
        string state = "";

        // declared tempID variable because emp_id is not available now.
        string tempID = Login.employeeId;

        /* 
         * 
         * declared a connection method to connect with DB
         * server name = [Your Server Name] ;
         * database name = AttendanceDB ;
         * 
         */
        private void connection()
        {
            conn = new SqlConnection(@"data source = TMPC-057 ; database = AttendanceDB ;
            Integrated Security = true");
        }

        public Profile()
        {
            InitializeComponent();
            btnStaffProfile.BackColor = Color.FromArgb(151, 242, 0);
            btnMyAccount.BackColor = Color.FromArgb(151, 242, 0);

            // initialize placeholder text in jp certificate textbox
            txtJPLevelName.ForeColor = SystemColors.GrayText;
            txtJPLevelName.Text = "Japanese Level";
            txtJPLevelName.Leave += new System.EventHandler(TxtJPLevelName_Leave);
            txtJPLevelName.Enter += new System.EventHandler(TxtJPLevelName_Enter);

            // initialize placeholder text in IT certificate textbox
            txtCertificateName.ForeColor = SystemColors.GrayText;
            txtCertificateName.Text = "Certificate";
            txtCertificateName.Leave += new System.EventHandler(TxtCertificateName_Leave);
            txtCertificateName.Enter += new System.EventHandler(TxtCertificateName_Enter);

            // initialize placeholder text in work experience textbox
            txtWorkPosition.ForeColor = SystemColors.GrayText;
            txtWorkPosition.Text = "Experience";
            txtWorkPosition.Leave += new System.EventHandler(TxtWorkPosition_Leave);
            txtWorkPosition.Enter += new System.EventHandler(TxtWorkPosition_Enter);

            // initialize placeholder text in company name textbox
            txtCompanyName.ForeColor = SystemColors.GrayText;
            txtCompanyName.Text = "Company Name";
            txtCompanyName.Leave += new System.EventHandler(TxtCompanyName_Leave);
            txtCompanyName.Enter += new System.EventHandler(TxtCompanyName_Enter);

            // initialize placeholder text in company location textbox
            txtCompanyLocation.ForeColor = SystemColors.GrayText;
            txtCompanyLocation.Text = "Location";
            txtCompanyLocation.Leave += new System.EventHandler(TxtCompanyLocation_Leave);
            txtCompanyLocation.Enter += new System.EventHandler(TxtCompanyLocation_Enter);
        }

        // when user enters textbox, change placeholder text to blank and font color to default
        private void TxtCompanyLocation_Enter(object sender, EventArgs e)
        {
            if (txtCompanyLocation.Text.Equals("Location"))
            {
                txtCompanyLocation.Text = "";
                txtCompanyLocation.ForeColor = SystemColors.WindowText;
            }
        }
        // when user leaves textbox, show placeholder text in textbox and change forecolor
        private void TxtCompanyLocation_Leave(object sender, EventArgs e)
        {
            if (txtCompanyLocation.Text.Length == 0)
            {
                txtCompanyLocation.Text = "Location";
                txtCompanyLocation.ForeColor = SystemColors.GrayText;
            }
        }

        // when user enters textbox, change placeholder text to blank and font color to default
        private void TxtCompanyName_Enter(object sender, EventArgs e)
        {
            if (txtCompanyName.Text.Equals("Company Name"))
            {
                txtCompanyName.Text = "";
                txtCompanyName.ForeColor = SystemColors.WindowText;
            }
        }
        // when user leaves textbox, show placeholder text in textbox and change forecolor
        private void TxtCompanyName_Leave(object sender, EventArgs e)
        {
            if (txtCompanyName.Text.Length == 0)
            {
                txtCompanyName.Text = "Company Name";
                txtCompanyName.ForeColor = SystemColors.GrayText;
            }
        }

        // when user enters textbox, change placeholder text to blank and font color to default
        private void TxtWorkPosition_Enter(object sender, EventArgs e)
        {
            if (txtWorkPosition.Text.Equals("Experience"))
            {
                txtWorkPosition.Text = "";
                txtWorkPosition.ForeColor = SystemColors.WindowText;
            }
        }
        // when user leaves textbox, show placeholder text in textbox and change forecolor
        private void TxtWorkPosition_Leave(object sender, EventArgs e)
        {
            if (txtWorkPosition.Text.Length == 0)
            {
                txtWorkPosition.Text = "Experience";
                txtWorkPosition.ForeColor = SystemColors.GrayText;
            }
        }

        // when user enters textbox, change placeholder text to blank and font color to default
        private void TxtCertificateName_Enter(object sender, EventArgs e)
        {
            if (txtCertificateName.Text.Equals("Certificate"))
            {
                txtCertificateName.Text = "";
                txtCertificateName.ForeColor = SystemColors.WindowText;
            }
        }
        // when user leaves textbox, show placeholder text in textbox and change forecolor
        private void TxtCertificateName_Leave(object sender, EventArgs e)
        {
            if (txtCertificateName.Text.Length == 0)
            {
                txtCertificateName.Text = "Certificate";
                txtCertificateName.ForeColor = SystemColors.GrayText;
            }
        }
        // when user enters textbox, change placeholder text to blank and font color to default
        private void TxtJPLevelName_Enter(object sender, EventArgs e)
        {
            if (txtJPLevelName.Text.Equals("Japanese Level"))
            {
                txtJPLevelName.Text = "";
                txtJPLevelName.ForeColor = SystemColors.WindowText;
            }
        }
        // when user leaves textbox, show placeholder text in textbox and change forecolor
        private void TxtJPLevelName_Leave(object sender, EventArgs e)
        {
            if (txtJPLevelName.Text.Length == 0)
            {
                txtJPLevelName.Text = "Japanese Level";
                txtJPLevelName.ForeColor = SystemColors.GrayText;
            }
        }


        private void profileForm_Load(object sender, EventArgs e)
        {
            // declared a state as display to only show data when profile form is loaded
            state = "display";

            // clearing default Label Texts
            clearDefaultTexts();

            // these methods is used to retrieve datas from DB and shows on form when profile is loading.
            getEmployeeInfo();

            getJPSkillInfo();
            getCertificationInfo();
            getExperienceInfo();

            // this method makes profile view (ReadOnly Not Editable)
            readOnlyUserProfile();
        }

        //********************** Create DataTables *****************************

        // declared a dataTable that stores data from t_emp_jpkill table

        DataTable dtJPCertificate = new DataTable();
        private void loadJPCertificate()
        {
            connection();

            dtJPCertificate.Columns.Add("JPLevelName");
            dtJPCertificate.Columns.Add("JPBandScore");
            dtJPCertificate.Columns.Add("JPCertificateYear");
            dtJPCertificate.Columns.Add("Added");
            dtJPCertificate.Columns.Add("MaxLevel", typeof(int));
            conn.Open();

            string selectQuery = @"select * from t_emp_jpkill where emp_id = '" + txtEmployeeID.Text.Trim().ToString() + "';";

            SqlCommand selectCmd = new SqlCommand(selectQuery, conn);

            SqlDataReader sdr = selectCmd.ExecuteReader();

            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    dtJPCertificate.Rows.Add(sdr[2].ToString(), sdr[3].ToString(),
                    Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM"), "false", sdr[5]);
                }

                sdr.Close();
                conn.Close();
            }
            else
            {
                MessageBox.Show("Nothing found in t_emp_jpkill table");
            }
        }

        // declared a dataTable that stores data from t_emp_certification table
        DataTable dtCertificate = new DataTable();
        private void loadCertificate()
        {
            connection();

            dtCertificate.Columns.Add("CertificateName");
            dtCertificate.Columns.Add("DateFrom");
            dtCertificate.Columns.Add("DateTo");
            dtCertificate.Columns.Add("Added");

            conn.Open();

            string selectQuery = @"select * from t_emp_certification where emp_id = '" + txtEmployeeID.Text.Trim().ToString() + "';";

            SqlCommand selectCmd = new SqlCommand(selectQuery, conn);

            SqlDataReader sdr = selectCmd.ExecuteReader();

            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    dtCertificate.Rows.Add(sdr[2].ToString(), Convert.ToDateTime(sdr.GetValue(3)).ToString("yyyy/MM"),
                    Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM"), "false");
                }

                sdr.Close();
                conn.Close();
            }
            else
            {
                MessageBox.Show("Nothing found in t_emp_certification table");
            }
        }

        // declared a dataTable that stores data from t_emp_work_experience table

        DataTable dtWorkExperience = new DataTable();
        private void loadWorkExperience()
        {
            connection();

            dtWorkExperience.Columns.Add("Position");
            dtWorkExperience.Columns.Add("WorkDateFrom");
            dtWorkExperience.Columns.Add("WorkDateTo");
            dtWorkExperience.Columns.Add("CompanyName");
            dtWorkExperience.Columns.Add("CompanyLocation");
            dtWorkExperience.Columns.Add("Added");

            conn.Open();

            string selectQuery = @"select * from t_emp_work_experience where emp_id = '" + txtEmployeeID.Text.Trim().ToString() + "';";

            SqlCommand selectCmd = new SqlCommand(selectQuery, conn);

            SqlDataReader sdr = selectCmd.ExecuteReader();

            if (sdr.HasRows)
            {
                while (sdr.Read())
                {
                    dtWorkExperience.Rows.Add(sdr[2].ToString(), Convert.ToDateTime(sdr.GetValue(3)).ToString("yyyy/MM"),
                    Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM"), sdr[5].ToString(), sdr[6].ToString(), "false");
                }

                sdr.Close();
                conn.Close();
            }
            else
            {
                MessageBox.Show("Nothing found in t_emp_work_experience table");
            }

        }

        //**********************  DataTables END *****************************

        // clear default testing texts from textboxes and labels
        // except placeholder texts from certificate and work experience textboxes
        private void clearDefaultTexts()
        {
            txtEmployeeID.Text = string.Empty;
            txtEmployeeName.Text = string.Empty;
            comboRole.Text = string.Empty;
            dtpDOB.Text = string.Empty;
            txtNRC.Text = string.Empty;
            txtFatherName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtEmail.Text = string.Empty;
            rtxtPermAddr.Text = string.Empty;
            rtxtTempAddr.Text = string.Empty;
            txtEmgContactName.Text = string.Empty;
            txtEmgContactNo.Text = string.Empty;
            txtEmgContactRelation.Text = string.Empty;
            txtEducation.Text = string.Empty;

            lblJPLevelName.Text = string.Empty;
            lblJPBandScore.Text = string.Empty;
            lblJPCertificateDate.Text = string.Empty;

            lblCertificateName.Text = string.Empty;
            lblCertificateDate.Text = string.Empty;

            lblWorkExperienceDate.Text = string.Empty;
            lblWorkPosition.Text = string.Empty;
            lblNameLocation.Text = string.Empty;

            txtJPBandScore.Text = string.Empty;
            dtpJPYear.Text = string.Empty;
            dtpCertificateDateFrom.Text = string.Empty;
            dtpCertificateDateTo.Text = string.Empty;
            dtpExperienceDateFrom.Text = string.Empty;
            dtpExperienceDateTo.Text = string.Empty;
        }

        /// <summary>
        /// 
        /// declared readOnlyUserProfile() method to display non editable user infos
        /// How it work?
        /// eg. it hides border and set readonly textbox properties true , to display as non editable label text
        /// </summary>
        private void readOnlyUserProfile()
        {
            // Set elements in personal Information group box disabled and cannot edit
            foreach (Control ctrl in gbPersonalInfo.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = true;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.None;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).Enabled = false;
                    ((ComboBox)ctrl).Visible = false;
                }
                if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)ctrl).Enabled = false;
                    ((DateTimePicker)ctrl).Visible = false;
                }
                if (ctrl is RadioButton)
                {
                    ((RadioButton)ctrl).Enabled = false;
                    ((RadioButton)ctrl).Visible = false;
                }
                if (ctrl is Button)
                {
                    ((Button)ctrl).Enabled = false;
                    ((Button)ctrl).Visible = false;
                }
            }

            // Set elements in contact Information group box disabled and cannot edit
            foreach (Control ctrl in gbContactInfo.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = true;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.None;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is RichTextBox)
                {
                    ((RichTextBox)ctrl).ReadOnly = true;
                    ((RichTextBox)ctrl).BorderStyle = BorderStyle.None;
                    ((RichTextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).BackColor = SystemColors.Window;
                    ((ComboBox)ctrl).FlatStyle = FlatStyle.Flat;
                    ((ComboBox)ctrl).Enabled = false;
                }
                if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)ctrl).Enabled = false;
                }
            }

            // Set elements in education background group box disabled and cannot edit
            foreach (Control ctrl in gbEducation.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = true;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.None;
                }

                if (ctrl is Panel)
                {
                    ((Panel)ctrl).Visible = false;

                    //panel is visible but default labels and radio button will be hided
                    jpCertificatePanel.Visible = true;
                    certificatePanel.Visible = true;
                }
            }

            foreach (Control ctrl in jpCertificatePanel.Controls)
            {
                if (ctrl is RadioButton)
                {
                    ((RadioButton)ctrl).Visible = false;
                }
            }


            // Set elements in work experience group box disabled and cannot edit
            foreach (Control ctrl in gbWorkExperience.Controls)
            {
                if (ctrl is Panel)
                {
                    workExperiencePanel.Visible = true;
                    ((Panel)ctrl).Visible = false;
                }
            }
        } // </readOnlyProfile>


        // get information and show in gbpersonalinfo groupbox from t_emp_basic_info table
        private void getEmployeeInfo()
        {
            //called connection method to connect with DB
            connection();

            try
            {
                conn.Open();

                string selectEmployeeInfo = @"Select * From t_emp_basic_info
                Where emp_id = '" + tempID + "';";

                SqlCommand cmd = new SqlCommand(selectEmployeeInfo, conn);

                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        txtEmployeeID.Text = sdr.GetString(1);// ID
                        txtEmployeeName.Text = sdr.GetString(2); // Name
                        dtpDOB.Text = Convert.ToDateTime(sdr.GetValue(5)).ToString("yyyy/MM/dd"); //DOB
                        txtNRC.Text = sdr.GetString(6); //NRC
                        txtFatherName.Text = sdr.GetString(8); // FatherName
                        txtPhone.Text = "09" + sdr.GetInt32(9).ToString(); // Phone
                        txtEmail.Text = sdr.GetString(10); // Email
                        rtxtPermAddr.Text = sdr.GetString(11); // Permanent Address
                        rtxtTempAddr.Text = sdr.GetString(12); // Temporary Address
                        txtEmgContactName.Text = sdr.GetString(13); // Emergency Contact Name
                        txtEmgContactNo.Text = "09" + sdr.GetInt32(14).ToString(); // Emergency Contact No
                        txtEmgContactRelation.Text = sdr.GetString(15); // Emergency Contact Relation
                        txtEducation.Text = sdr.GetString(16); // Education

                        /*
                         * 
                         * load image into picture box
                         * 
                         */
                        string imageName = txtEmployeeID.Text.Trim().ToString();

                        // create a local path where profile pictures are stored
                        string localPath = @"C:\Users\T2023006\Desktop\AMS Project\temp\";

                        string[] profilePic = Directory.GetFiles(localPath, $"{imageName}.*");

                        imgBox.Image = new Bitmap(profilePic[0]);

                        // Check Employee Role (1 or 2)
                        // 1 is administrator
                        // 2 is staff
                        if (Convert.ToInt16(sdr.GetValue(4)) == 1)
                        {
                            lblRollName.Text = "Administrator";
                            comboRole.Text = "Administrator";
                        }
                        else if (Convert.ToInt16(sdr.GetValue(4)) == 2)
                        {
                            lblRollName.Text = "Staff";
                            comboRole.Text = "Staff";
                        }

                        // Check Employee Gender (1 or 2)
                        // 1 is Male
                        // 2 is Female
                        if (Convert.ToInt16(sdr.GetValue(7)) == 1)
                        {
                            lblGenderName.Text = "Male";
                            rdoMale.Checked = true;
                        }
                        else if (Convert.ToInt16(sdr.GetValue(7)) == 2)
                        {
                            lblGenderName.Text = "Female";
                            rdoFemale.Checked = true;
                        }

                        // display date of birth from datetime picker on the customize label
                        lblDisplayDOB.Text = dtpDOB.Text;
                    }
                    // exit from while : close the datareader and connection
                    sdr.Close();
                    conn.Close();
                }
                else
                {
                    //MessageBox.Show("There is no data in t_emp_basic_info table");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        // get information and show in gbEducation groupbox t_emp_jpkill table
        private void getJPSkillInfo()
        {
            connection();

            try
            {
                // declared top,  to contorl the line spacing
                int top = 10;

                conn.Open();

                string selectJPSkills = @"Select * From t_emp_jpkill Where emp_id = '" + tempID + "';";

                SqlCommand cmd = new SqlCommand(selectJPSkills, conn);

                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    // clear controls in jp certificate panel
                    jpCertificatePanel.Controls.Clear();

                    // re-create require controls in jp certificae panel
                    Label lblJPLangLevel = new Label();

                    // 
                    // Create label lblJPLangLevel
                    // 
                    lblJPLangLevel.AutoSize = true;
                    lblJPLangLevel.Location = new System.Drawing.Point(3, 10); // (left , top) from panel
                    lblJPLangLevel.Size = new System.Drawing.Size(238, 27); // (width , height)
                    lblJPLangLevel.TabIndex = 5;
                    lblJPLangLevel.Text = "Japanese Language Level";

                    // add new created label to panel
                    jpCertificatePanel.Controls.Add(lblJPLangLevel);

                    while (sdr.Read())
                    {
                        // create labels and a radio button to display jp certificates
                        Label lblJPLevelName = new Label();
                        Label lblJPBandScore = new Label();
                        Label lblJPCertificateDate = new Label();
                        RadioButton rdoSetDefault = new RadioButton();

                        jpCertificatePanel.Controls.Add(lblJPLevelName);
                        jpCertificatePanel.Controls.Add(lblJPBandScore);
                        jpCertificatePanel.Controls.Add(lblJPCertificateDate);
                        jpCertificatePanel.Controls.Add(rdoSetDefault);


                        // 
                        // create label lblJPLevelName
                        //

                        lblJPLevelName.AutoSize = true;
                        lblJPLevelName.Location = new System.Drawing.Point(lblJPLangLevel.Right + 20, top);
                        lblJPLevelName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblJPLevelName.Size = new System.Drawing.Size(80, 27);
                        lblJPLevelName.TabIndex = 7;
                        lblJPLevelName.Text = sdr[2].ToString();
                        lblJPLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        //
                        // create label lblJPBandScore
                        //

                        lblJPBandScore.AutoSize = true;
                        lblJPBandScore.Location = new System.Drawing.Point(lblJPLangLevel.Right + 110, top);
                        lblJPBandScore.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblJPBandScore.Size = new System.Drawing.Size(80, 27);
                        lblJPBandScore.TabIndex = 7;
                        lblJPBandScore.Text = sdr[3].ToString();
                        lblJPBandScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        //
                        // create label lblJPCertificateDate
                        //

                        lblJPCertificateDate.AutoSize = true;
                        lblJPCertificateDate.Location = new System.Drawing.Point(lblJPLangLevel.Right + 175, top);
                        lblJPCertificateDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblJPCertificateDate.Size = new System.Drawing.Size(80, 27);
                        lblJPCertificateDate.TabIndex = 7;
                        lblJPCertificateDate.Text = Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM");
                        lblJPCertificateDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        // 
                        // create radio button rdoSetDefault
                        // 
                        rdoSetDefault.AutoSize = true;
                        rdoSetDefault.Location = new System.Drawing.Point(lblJPLangLevel.Right + 265, top);
                        rdoSetDefault.Margin = new System.Windows.Forms.Padding(0);
                        rdoSetDefault.Size = new System.Drawing.Size(158, 31);
                        rdoSetDefault.TabIndex = 10;
                        rdoSetDefault.TabStop = true;
                        rdoSetDefault.Text = "Set as Default";
                        rdoSetDefault.UseVisualStyleBackColor = true;

                        if (Convert.ToInt16(sdr[5]) == 1)
                        {
                            rdoSetDefault.Checked = true;
                        }
                        else
                        {
                            rdoSetDefault.Checked = false;
                        }

                        top += 30;
                    }
                    // close sql datareader and connection
                    sdr.Close();
                    conn.Close();
                }
                else
                {
                    //MessageBox.Show("Nothing found in t_emp_jpskill table!");
                }

                // for responsive layout

                jpCertificatePanel.AutoSize = true;
                jpCertificatePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                certificatePanel.AutoSize = true;
                certificatePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                certificatePanel.Top = jpCertificatePanel.Bottom + 10;

                gbEducation.AutoSize = true;
                gbEducation.AutoSizeMode = AutoSizeMode.GrowOnly;

                gbWorkExperience.Top = gbEducation.Bottom + 10;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // get certificate info from t_emp_certification table
        private void getCertificationInfo()
        {
            connection();

            try
            {
                int top = 10;

                conn.Open();

                string selectCertificate = @"Select * From t_emp_certification Where emp_id = '" + tempID + "';";

                SqlCommand cmd = new SqlCommand(selectCertificate, conn);

                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    certificatePanel.Controls.Clear();

                    // create label 'lblCertificate'
                    Label lblCertificate = new Label();

                    lblCertificate.AutoSize = true;
                    lblCertificate.Location = new System.Drawing.Point(3, 10);
                    lblCertificate.Size = new System.Drawing.Size(238, 27);
                    lblCertificate.TabIndex = 5;
                    lblCertificate.Text = "Certification";

                    certificatePanel.Controls.Add(lblCertificate);

                    while (sdr.Read())
                    {
                        Label lblCertificateName = new Label();
                        Label lblCertificateDate = new Label();

                        certificatePanel.Controls.Add(lblCertificateName);
                        certificatePanel.Controls.Add(lblCertificateDate);

                        // 
                        // create label lblWorkPosition
                        //

                        lblCertificateName.AutoSize = true;
                        lblCertificateName.Location = new System.Drawing.Point(lblCertificate.Right + 105, top);
                        lblCertificateName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblCertificateName.Size = new System.Drawing.Size(80, 27);
                        lblCertificateName.TabIndex = 7;
                        lblCertificateName.Text = sdr[2].ToString();
                        lblCertificateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        //
                        // create label lblWorkExperienceDate
                        //

                        lblCertificateDate.AutoSize = true;
                        lblCertificateDate.Location = new System.Drawing.Point(lblCertificate.Right + 280, top);
                        lblCertificateDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblCertificateDate.Size = new System.Drawing.Size(80, 27);
                        lblCertificateDate.TabIndex = 7;
                        lblCertificateDate.Text = Convert.ToDateTime(sdr.GetValue(3)).ToString("yyyy/MM") + " ~ " + Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM");
                        lblCertificateDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        top += 30;
                    }

                    // close sdr and conn
                    sdr.Close();
                    conn.Close();
                }
                else
                {
                   // MessageBox.Show("Nothing found in t_emp_certification table!");
                }
                // for responsive layout
                certificatePanel.Top = jpCertificatePanel.Bottom + 20;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // get work experience info from t_emp_work_experience table
        private void getExperienceInfo()
        {
            //gbWorkExperience.Top = gbEducation.Bottom + 20;

            connection();

            try
            {
                int top = 10;

                conn.Open();

                string selectWorkExperience = @"Select * From t_emp_work_experience Where emp_id = '" + tempID + "';";

                SqlCommand cmd = new SqlCommand(selectWorkExperience, conn);

                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    // if there is data in database , clear controls in work experience panel
                    workExperiencePanel.Controls.Clear();

                    Label lblWorkExperience = new Label();

                    //
                    // create label 'lblWorkExperience'
                    //
                    lblWorkExperience.AutoSize = true;
                    lblWorkExperience.Location = new System.Drawing.Point(3, 10);
                    lblWorkExperience.Size = new System.Drawing.Size(238, 27);
                    lblWorkExperience.TabIndex = 5;
                    lblWorkExperience.Text = "Work Experience";

                    workExperiencePanel.Controls.Add(lblWorkExperience);


                    while (sdr.Read())
                    {
                        Label lblWorkPosition = new Label();
                        Label lblWorkExperienceDate = new Label();
                        Label lblNameLocation = new Label();

                        workExperiencePanel.Controls.Add(lblWorkPosition);
                        workExperiencePanel.Controls.Add(lblWorkExperienceDate);
                        workExperiencePanel.Controls.Add(lblNameLocation);

                        // 
                        // create label lblWorkPosition
                        //

                        lblWorkPosition.AutoSize = true;
                        lblWorkPosition.Location = new System.Drawing.Point(lblWorkExperience.Right + 80, top);
                        lblWorkPosition.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblWorkPosition.Size = new System.Drawing.Size(80, 27);
                        lblWorkPosition.TabIndex = 7;
                        lblWorkPosition.Text = sdr[2].ToString();
                        lblWorkPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        //
                        // create label lblWorkExperienceDate
                        //

                        lblWorkExperienceDate.AutoSize = true;
                        lblWorkExperienceDate.Location = new System.Drawing.Point(lblWorkExperience.Right + 250, top);
                        lblWorkExperienceDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblWorkExperienceDate.Size = new System.Drawing.Size(80, 27);
                        lblWorkExperienceDate.TabIndex = 7;
                        lblWorkExperienceDate.Text = Convert.ToDateTime(sdr.GetValue(3)).ToString("yyyy/MM") + " ~ " + Convert.ToDateTime(sdr.GetValue(4)).ToString("yyyy/MM");
                        lblWorkExperienceDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;


                        //
                        // create label lblNameLocation
                        //

                        lblNameLocation.AutoSize = true;
                        lblNameLocation.Location = new System.Drawing.Point(lblWorkExperience.Right + 430, top);
                        lblNameLocation.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                        lblNameLocation.Size = new System.Drawing.Size(80, 27);
                        lblNameLocation.TabIndex = 7;
                        lblNameLocation.Text = sdr[5].ToString() + " ," + sdr[6].ToString();
                        lblNameLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                        top += 30;
                    }

                    // close sdr and conn
                    sdr.Close();
                    conn.Close();
                }
                else
                {
                    //MessageBox.Show("Nothing found in t_emp_work_experience table!");
                }

                // for responsive layout
                workExperiencePanel.AutoSize = true;
                workExperiencePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                gbWorkExperience.Top = gbEducation.Bottom + 10;

                btnEdit.Top = gbWorkExperience.Bottom + 10;

                containerPanel.AutoSize = true;
                containerPanel.AutoSizeMode = AutoSizeMode.GrowOnly;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // upload profile picture
        private void btnUpload_Click(object sender, EventArgs e)
        {
            // clear picture box and add new profile picture
            if (imgBox.Image != null)
            {
                imgBox.Image.Dispose();
            }

            // open file dialog
            OpenFileDialog fileDialog = new OpenFileDialog();

            // initial directory you want to see when open file dialog
            fileDialog.InitialDirectory = "C:\\Users\\T2023006\\Pictures";

            // filter to show only these types of images
            fileDialog.Filter = "Image Only(*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                // getting (Selected Image Path)
                string imagePath = fileDialog.FileName;
                
                // getting (Image Name With ID)
                string imageName = txtEmployeeID.Text.Trim().ToString();
                
                // getting (Selected Image Info)
                FileInfo imageInfo =new FileInfo(imagePath);

                // getting (Selected Image Extension)
                string imageExtension = imageInfo.Extension;

                // create a local path where profile pictures are stored
                string localPath = @"C:\Users\T2023006\Desktop\AMS Project\temp\";

                // get every images with same name with employee ID from local path
                string[] imageWithSameName = Directory.GetFiles(localPath, $"{imageName}.*");

                // check image size is less than 1 mb or not
                if (checkImageSize(imagePath,1024) == false)
                {
                    MessageBox.Show("Image size must be less than 1mb");
                }
                else
                {
                    // if image with same name exist, delete them and add new incoming image
                    if (imageWithSameName.Length > 0)
                    {
                        try
                        {
                            // deleting image with same name
                            File.Delete(imageWithSameName[0]);

                            // adding new selected image to folder
                            File.Copy(imagePath, localPath + imageName + imageExtension, true);

                            // set creation time to image
                            File.SetCreationTimeUtc(localPath + imageName + imageExtension, DateTime.Now);

                            // adding image to picture box
                            imgBox.Image = new Bitmap(imagePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        // adding new selected image to folder
                        File.Copy(imagePath, localPath + imageName + imageExtension, true);

                        // set creation time to image
                        File.SetCreationTimeUtc(localPath + imageName + imageExtension, DateTime.Now);

                        // adding image to picture box
                        imgBox.Image = new Bitmap(imagePath);
                    }
                }
            }
            else if (fileDialog.ShowDialog() == DialogResult.Cancel)
            {
                MessageBox.Show("Please attach an image");
            }
        }

        // check form and format validation
        private Boolean checkError()
        {
            /*
             * 
             * Form Validation (Is Null or Empty?)
             * 
             */
            if (string.IsNullOrEmpty(txtEmployeeName.Text))
            {
                MessageBox.Show("Please fill name"); // Err002
                txtEmployeeName.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtNRC.Text))
            {
                MessageBox.Show("Please fill NRC number"); // Err005
                txtNRC.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtFatherName.Text))
            {
                MessageBox.Show("Please fill father name"); // Err006
                txtFatherName.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtPhone.Text.Trim()))
            {
                MessageBox.Show("Please fill phone number using (09xxxxxxxx)," +
                    " start with 09 and follow by 7 or 9 digits"); // Err007
                txtPhone.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Please fill email address"); // Error009
                txtEmail.Focus();   
                return true;
            }
            if (string.IsNullOrEmpty(rtxtPermAddr.Text))
            {
                MessageBox.Show("Please fill  permanent address"); // Error011
                rtxtPermAddr.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtEmgContactName.Text))
            {
                MessageBox.Show("Please fill emergency contact name"); // Error013
                txtEmgContactName.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtEmgContactNo.Text))
            {
                MessageBox.Show("Please fill emergency contact number"); // Error014
                txtEmgContactNo.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtEmgContactRelation.Text))
            {
                MessageBox.Show("Please fill relation"); // Err015
                txtEmgContactRelation.Focus();
                return true;
            }
            if (string.IsNullOrEmpty(txtEducation.Text))
            {
                MessageBox.Show("Please fill  education"); // Err016
                txtEducation.Focus();
                return true;
            }

            /*
             * 
             * Format Validation
             * 
             */

            // Error047 : Employee name must not be integer !
            if (Regex.IsMatch(txtEmployeeName.Text, "[0-9]"))
            {
                MessageBox.Show("Name must not be integer !");
                txtEmployeeName.Focus();
                return true;
            }

            // Error047 : Father name must not be integer !
            if (Regex.IsMatch(txtFatherName.Text, "[0-9]"))
            {
                MessageBox.Show("Name must not be integer !");
                txtFatherName.Focus();
                return true;
            }

            // Error022 : Contact relation must not be integer !
            if (Regex.IsMatch(txtEmgContactRelation.Text, "[0-9]"))
            {
                MessageBox.Show("Contact relation must not be integer !");
                txtEmgContactRelation.Focus();
                return true;
            }

            // Error023 : Contact name must not be integer !
            if (Regex.IsMatch(txtEmgContactName.Text, "[0-9]"))
            {
                MessageBox.Show("Contact name must not be integer !");
                txtEmgContactName.Focus();
                return true;
            }

            // Error008 : Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits
            // (txtEmgContactNo)
            if (!(txtEmgContactNo.Text.Trim().StartsWith("09")))
            {
                MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                txtEmgContactNo.Focus();
                return true;
            }
            else
            {
                if (!(txtEmgContactNo.Text.Trim().Substring(2).Length == 7 || txtEmgContactNo.Text.Trim().Substring(2).Length == 9))
                {
                    MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                    txtEmgContactNo.Focus();
                    return true;
                }
            }


            // Error008 : Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits
            // (txtPhone)
            if (!(txtPhone.Text.Trim().StartsWith("09")))
            {
                MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                txtPhone.Focus();
                return true;
            }
            else
            {
                if (!(txtPhone.Text.Trim().Substring(2).Length == 7 || txtPhone.Text.Trim().Substring(2).Length == 9))
                {
                    MessageBox.Show("Phone number format is wrong, phone number should be using (09xxxxxxxx), start with 09 and follow by 7 or 9 digits");
                    txtPhone.Focus();
                    return true;
                }
            }

            // Error010 : Email format is invalid, format is "your email address@gmail.com"
            // (txtEmail)
            if (!(isEmailCorrect(txtEmail.Text.Trim().ToString())))
            {
                MessageBox.Show("Email format is invalid, format is \"youremailaddress@gmail.com\"");
                txtEmail.Focus();
                return true;
            }

            // Check image is empty or not
            // (pbProfileImage)
            if (imgBox.Image == null)
            {
                MessageBox.Show("Please attach image");

                return true;
            }
            return false; // if there is no error in edit process
        }



        // Check image size is less than 1MB or not ?
        public bool checkImageSize(string filepath, long maxSize)
        {
            long imageSize = new FileInfo(filepath).Length / 1024;

            if (imageSize < maxSize)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // Email Format checking
        public bool isEmailCorrect(string emailaddress)
        {
            string pattern = @"^[a-zA-Z0-9._%+-]+@gmail\.com$";

            return Regex.IsMatch(emailaddress, pattern);
        }


        // editableUserProfile make userprofile editable , updating new data  
        private void editableUserProfile()
        {

            /* default labels above combobox,datetimepicker,radio_button are disabled to stop showing default data and
             to edit and update new data */

            lblRollName.Visible = false;
            lblDisplayDOB.Visible = false;
            lblGenderName.Visible = false;
            rdoSetDefault1.Visible = false;

            // Set elements in personal Information group box enabled and visible to edit
            foreach (Control ctrl in gbPersonalInfo.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = false;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.Fixed3D;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).Enabled = true;
                    ((ComboBox)ctrl).Visible = true;
                }
                if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)ctrl).Enabled = true;
                    ((DateTimePicker)ctrl).Visible = true;
                }
                if (ctrl is RadioButton)
                {
                    ((RadioButton)ctrl).Enabled = true;
                    ((RadioButton)ctrl).Visible = true;
                }
                if (ctrl is Button)
                {
                    ((Button)ctrl).Enabled = true;
                    ((Button)ctrl).Visible = true;
                }
            }

            // Set elements in contact Information group box enabled and visible to edit
            foreach (Control ctrl in gbContactInfo.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = false;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.Fixed3D;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is RichTextBox)
                {
                    ((RichTextBox)ctrl).ReadOnly = false;
                    ((RichTextBox)ctrl).BorderStyle = BorderStyle.Fixed3D;
                    ((RichTextBox)ctrl).BackColor = SystemColors.Window;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).Enabled = true;
                    ((ComboBox)ctrl).Visible = true;
                }
                if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)ctrl).Enabled = true;
                    ((DateTimePicker)ctrl).Visible = true;
                }
            }

            // Set elements in education background group box enabled and visible to edit
            foreach (Control ctrl in gbEducation.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).ReadOnly = false;
                    ((TextBox)ctrl).BackColor = SystemColors.Window;
                    ((TextBox)ctrl).BorderStyle = BorderStyle.Fixed3D;
                }
                if (ctrl is RadioButton)
                {
                    ((RadioButton)ctrl).Enabled = true;
                    ((RadioButton)ctrl).Visible = true;

                    /* // set default radio button unvisible
                     rdoSetDefault1.Visible = false;*/
                }
                if (ctrl is Panel)
                {
                    ((Panel)ctrl).Visible = true;

                    addJPSkillPanel.Top = jpCertificatePanel.Bottom + 10;
                    certificatePanel.Top = addJPSkillPanel.Bottom + 10;
                    addCertificatePanel.Top = certificatePanel.Bottom + 10;
                }

                foreach (Control ctrl1 in jpCertificatePanel.Controls)
                {
                    if (ctrl1 is RadioButton)
                    {
                        ((RadioButton)ctrl1).Visible = true;
                    }
                }
            }

            // Set elements in work experience group box enabled and visible to edit
            foreach (Control ctrl in gbWorkExperience.Controls)
            {
                if (ctrl is Panel)
                {
                    ((Panel)ctrl).Visible = true;
                }

                workExperiencePanel.AutoSize = true;
                workExperiencePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                addExperiencePanel.Top = workExperiencePanel.Bottom + 10;

                gbWorkExperience.AutoSize = true;
                gbWorkExperience.AutoSizeMode = AutoSizeMode.GrowOnly;

                btnEdit.Top = gbWorkExperience.Bottom + 10;

                containerPanel.AutoSize = true;
                containerPanel.AutoSizeMode = AutoSizeMode.GrowOnly;
            }

            gbEducation.AutoSize = true;
            gbEducation.AutoSizeMode = AutoSizeMode.GrowOnly;

            gbWorkExperience.Top = gbEducation.Bottom + 10;
            btnEdit.Top = gbWorkExperience.Bottom + 10;

            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowOnly;

        }

        // update employee basic info after user edited profile...
        private void update_t_emp_basic_info()
        {
            connection();

            try
            {
                conn.Open();

                string updateEmployeeInfo = @"update t_emp_basic_info set emp_name = @Name,emp_role = @Role,emp_dob = @Dob,emp_gender = @Gender,emp_nrc = @Nrc, emp_father = @FatherName,emp_phone = @Phone,emp_email = @Email,emp_perm_addr = @PermAddr,emp_temp_addr = @TempAddr, emp_emg_name = @EmgName, emp_emg_contact = @EmgContactNo, emp_emg_contact_relation = @EmgRelation, emp_education = @Education, emp_photo_name = @PhotoName where emp_id = @EmpID;";

                SqlCommand cmd = new SqlCommand(updateEmployeeInfo, conn);

                cmd.Parameters.AddWithValue("@EmpID", txtEmployeeID.Text.Trim().ToString()); // ID
                cmd.Parameters.AddWithValue("@Name", txtEmployeeName.Text.Trim().ToString()); // Name

                // Check Employee Role (1 or 2)
                // Set Role = 1 if 'Administrator'
                // Set Role = 2 if 'Stuff'
                if (comboRole.Items[comboRole.SelectedIndex].ToString().Equals("Administrator"))
                {
                    cmd.Parameters.AddWithValue("@Role", 1); // Role
                }
                else if (comboRole.Items[comboRole.SelectedIndex].ToString().Equals("Staff"))
                {
                    cmd.Parameters.AddWithValue("@Role", 2); // Role
                }

                // update employee gender (1 or 2)
                // set gender = 1 if male radio button is checked
                // set gender = 2 if female radio button is checked
                if (rdoMale.Checked == true)
                {
                    cmd.Parameters.AddWithValue("@Gender", 1); // Gender
                }
                else if (rdoFemale.Checked == true)
                {
                    cmd.Parameters.AddWithValue("@Gender", 2); // Gender
                }

                cmd.Parameters.AddWithValue("@Dob", Convert.ToDateTime(dtpDOB.Text)); // Date Of Birth
                cmd.Parameters.AddWithValue("@Nrc", txtNRC.Text.Trim().ToString()); // NRC
                cmd.Parameters.AddWithValue("@FatherName", txtFatherName.Text.Trim().ToString()); // Father Name
                cmd.Parameters.AddWithValue("@Phone", int.Parse(txtPhone.Text.Substring(2))); // Phone
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim().ToString()); // Email
                cmd.Parameters.AddWithValue("@PermAddr", rtxtPermAddr.Text.Trim().ToString()); // Permenant Address
                cmd.Parameters.AddWithValue("@TempAddr", rtxtTempAddr.Text.Trim().ToString()); // Temporary Address
                cmd.Parameters.AddWithValue("@EmgName", txtEmgContactName.Text.Trim().ToString()); // Emergency Contact Name
                cmd.Parameters.AddWithValue("@EmgContactNo", int.Parse(txtEmgContactNo.Text.Substring(2))); // Emergency Contact No
                cmd.Parameters.AddWithValue("@EmgRelation", txtEmgContactRelation.Text.Trim().ToString()); // Emergency Contact Relation
                cmd.Parameters.AddWithValue("@Education", txtEducation.Text.Trim().ToString()); // Education
                cmd.Parameters.AddWithValue("@PhotoName", txtEmployeeID.Text.Trim().ToString()); // Profile Picture Name

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // btnEdit click
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (state == "display")
            {
                // this method makes profile 'editable'
                editableUserProfile();

                // load and store datas from DB to DataTable
                loadJPCertificate();
                loadCertificate();
                loadWorkExperience();

                // set state from 'display' to 'edit' when user clicked 'edit' button 2nd time. 
                state = "edit";
            }
            else if (state == "edit")
            {
                // check error exists or not
                Boolean errorExist = checkError();

                if (errorExist == true)
                {
                    return;
                }
                else
                {
                    try
                    {
                        // update t_emp_basic_info tabel with edited datas
                        update_t_emp_basic_info();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        // insert new jp certificates to t_emp_jpskill table
                        insert_t_emp_jpkill();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        // rdoCount is used to know which radio button is clicked?
                        int rdoCount = checkRadioBtn(jpCertificatePanel);

                        // update jp certificate set default radio button.
                        update_t_emp_jpskill(rdoCount);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        // insert new certification infos into t_emp_certification table
                        insert_t_emp_certification();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        // insert new work experience infos into t_emp_work_experience table
                        insert_t_emp_work_experience();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    MessageBox.Show("Profile updated successfully!");

                    Profile pf = new Profile();
                    pf.Show();
                    this.Close();
                }
            }
        } // end of btnEdit click event


        //===============================INSERT & UPDATE SESSION (When Edit Button is clicked Second Time) ======================================


        // insert new jp certificate to t_emp_jpskill table
        private void insert_t_emp_jpkill()
        {
            connection();

            string insertQuery = @"insert into t_emp_jpkill values (@EmpID,@JPLevelName,@JPBandScore,@JPCertificateDate,@JPSkillMaxLevel)";

            SqlCommand cmd = new SqlCommand(insertQuery, conn);

            conn.Open();

            foreach (DataRow r in dtJPCertificate.Rows)
            {
                if (r[3].Equals("true"))
                {
                    cmd.Parameters.AddWithValue("@EmpID", txtEmployeeID.Text.Trim().ToString());
                    cmd.Parameters.AddWithValue("@JPLevelName", r[0].ToString());
                    cmd.Parameters.AddWithValue("@JPBandScore", r[1].ToString());
                    cmd.Parameters.AddWithValue("@JPCertificateDate", Convert.ToDateTime(r[2]));
                    cmd.Parameters.AddWithValue("@JPSkillMaxLevel", Convert.ToInt16(r[4]));

                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
        }


        /// <param name="newMax"> new max level parameter </param>
        private void update_t_emp_jpskill(int newMax)
        {
            connection();

            // set all max level to '0'
            foreach (DataRow r in dtJPCertificate.Rows)
            {
                r[4] = 0;
            }

            // set selected specific row's max level to '1'
            DataRow dr = dtJPCertificate.Rows[newMax - 1];
            dr[4] = 1;

            string jpCertificateName = dr[0].ToString();

            int zero = 0;

            string setZeroQuery = @"Update t_emp_jpkill Set emp_jpskill_maxlevel = '" + zero + "' Where emp_id = '" + txtEmployeeID.Text.Trim().ToString() + "';";

            SqlCommand zeroCmd = new SqlCommand(setZeroQuery, conn);

            conn.Open();

            zeroCmd.ExecuteNonQuery();

            conn.Close();

            string updateMaxLevelQuery = @"Update t_emp_jpkill Set emp_jpskill_maxlevel = @newMax Where emp_jpskill_level_name = '" + jpCertificateName + "' AND emp_id = '"+txtEmployeeID.Text.Trim().ToString()+"';";

            SqlCommand cmd = new SqlCommand(updateMaxLevelQuery, conn);

            cmd.Parameters.AddWithValue("@newMax", 1);
            conn.Open();

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        // Insert new added certificate infos into t_emp_certification table
        private void insert_t_emp_certification()
        {
            connection();

            string insertCertification = @"INSERT INTO t_emp_certification VALUES (@EmpID , @Certificate , @CertificateFrom , @CertificateTo)";

            SqlCommand cmd = new SqlCommand(insertCertification, conn);

            conn.Open();

            foreach (DataRow dr in dtCertificate.Rows)
            {
                if (dr[3].Equals("true"))
                {
                    cmd.Parameters.AddWithValue("@EmpID", txtEmployeeID.Text.Trim().ToString());
                    cmd.Parameters.AddWithValue("@Certificate", dr[0].ToString());
                    cmd.Parameters.AddWithValue("@CertificateFrom", Convert.ToDateTime(dr[1]));
                    cmd.Parameters.AddWithValue("@CertificateTo", Convert.ToDateTime(dr[2]));

                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
        }


        // Insert new added work experience infos into t_emp_experience table
        private void insert_t_emp_work_experience()
        {
            connection();

            string insertExperience = @"INSERT INTO t_emp_work_experience VALUES (@EmpID , @Position , @ExpDateFrom , @ExpDateTo , @CompanyName , @CompanyLocation)";

            SqlCommand cmd = new SqlCommand(insertExperience, conn);

            conn.Open();

            foreach (DataRow dr in dtWorkExperience.Rows)
            {
                if (dr[5].Equals("true"))
                {
                    cmd.Parameters.AddWithValue("@EmpID", txtEmployeeID.Text.Trim().ToString());
                    cmd.Parameters.AddWithValue("@Position", dr[0].ToString());
                    cmd.Parameters.AddWithValue("@ExpDateFrom", Convert.ToDateTime(dr[1]));
                    cmd.Parameters.AddWithValue("@ExpDateTo", Convert.ToDateTime(dr[2]));
                    cmd.Parameters.AddWithValue("@CompanyName", dr[3]);
                    cmd.Parameters.AddWithValue("@CompanyLocation", dr[4]);

                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
        }

        // check radio buttton
        private int checkRadioBtn(Control container)
        {
            // checked radio button
            int rdoBtnCount = 0;

            // check which radio button is checked ?
            foreach (Control ctrl in container.Controls)
            {
                if (ctrl is RadioButton)
                {
                    RadioButton radio = ctrl as RadioButton;

                    if (!radio.Checked)
                    {
                        rdoBtnCount += 1;
                    }
                    else
                    {
                        rdoBtnCount += 1;
                        break;
                    }
                }
            } // end of forloop
            return rdoBtnCount;
        }

        // add jp certificates
        // variable 'count' is used to counter the click event of add certificate button 

        // to check user edit or not jp certificate
        int count = 0;
        private void btnAddJPCertificate_Click(object sender, EventArgs e)
        {
            Boolean sameJPCertificate = false;

            foreach (DataRow dr in dtJPCertificate.Rows)
            {
                if (txtJPLevelName.Text.Trim().ToString().Equals(dr[0]))
                {
                    sameJPCertificate = true;
                }
            }

            if (sameJPCertificate)
            {
                MessageBox.Show("Same japanese level already exits");
            }
            else
            {
                // declared variable 'top' to align the position of "jp certificate labels" & "set as default radio btn"
                int top = 10;

                // if not Error017 and Error019, textbox values will store in dataTable
                if (string.IsNullOrEmpty(txtJPLevelName.Text.Trim()) || txtJPLevelName.Text.Equals("Japanese Level"))
                {
                    MessageBox.Show("Please fill japanese level"); // Error017
                    txtJPLevelName.Focus();
                }
                else if (string.IsNullOrEmpty(txtJPBandScore.Text.Trim()))
                {
                    MessageBox.Show("Please fill japanese band score");
                    txtJPBandScore.Focus();
                }
                else if (dtpJPYear.Value > DateTime.Now)
                {
                    MessageBox.Show("Japanese certificate year must be less than or equal current date"); // Error019
                    dtpJPYear.Value = DateTime.Now;
                    dtpJPYear.Focus();
                }
                else
                {
                    // clear labels and radio buttons that shows data from database
                    jpCertificatePanel.Controls.Clear();

                    // store new certificate info to datatable 'dtJPKill'
                    dtJPCertificate.Rows.Add(txtJPLevelName.Text.Trim().ToString(), txtJPBandScore.Text.Trim().ToString(),
                    Convert.ToDateTime(dtpJPYear.Text).ToString("yyyy/MM"), "true", 0);

                    count++;

                    for (int i = 0; i < count; i++)
                    {
                        Label lblJPLangLevel = new Label();

                        // 
                        // lblJPLangLevel
                        // 
                        lblJPLangLevel.AutoSize = true;
                        lblJPLangLevel.Location = new System.Drawing.Point(3, 10);
                        lblJPLangLevel.Size = new System.Drawing.Size(238, 27);
                        lblJPLangLevel.TabIndex = 5;
                        lblJPLangLevel.Text = "Japanese Language Level";

                        jpCertificatePanel.Controls.Add(lblJPLangLevel);

                        //dtr = dataTableRow
                        foreach (DataRow dtr in dtJPCertificate.Rows)
                        {
                            Label lblJPLevelName = new Label();
                            Label lblJPBandScore = new Label();
                            Label lblJPCertificateDate = new Label();
                            RadioButton rdoSetDefault = new RadioButton();

                            jpCertificatePanel.Controls.Add(lblJPLevelName);
                            jpCertificatePanel.Controls.Add(lblJPBandScore);
                            jpCertificatePanel.Controls.Add(lblJPCertificateDate);
                            jpCertificatePanel.Controls.Add(rdoSetDefault);

                            // 
                            // create label lblJPLevelName
                            //

                            lblJPLevelName.AutoSize = true;
                            lblJPLevelName.Location = new System.Drawing.Point(lblJPLangLevel.Right + 20, top);
                            lblJPLevelName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                            lblJPLevelName.Size = new System.Drawing.Size(80, 27);
                            lblJPLevelName.TabIndex = 7;
                            lblJPLevelName.Text = dtr[0].ToString();
                            lblJPLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                            //
                            // create label lblJPBandScore
                            //

                            lblJPBandScore.AutoSize = true;
                            lblJPBandScore.Location = new System.Drawing.Point(lblJPLangLevel.Right + 130, top);
                            lblJPBandScore.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                            lblJPBandScore.Size = new System.Drawing.Size(80, 27);
                            lblJPBandScore.TabIndex = 7;
                            lblJPBandScore.Text = dtr[1].ToString();
                            lblJPBandScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                            //
                            // create label lblJPCertificateDate
                            //

                            lblJPCertificateDate.AutoSize = true;
                            lblJPCertificateDate.Location = new System.Drawing.Point(lblJPLangLevel.Right + 195, top);
                            lblJPCertificateDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                            lblJPCertificateDate.Size = new System.Drawing.Size(80, 27);
                            lblJPCertificateDate.TabIndex = 7;
                            lblJPCertificateDate.Text = dtr[2].ToString();
                            lblJPCertificateDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                            // if maxlevel is 1 , set RadioButton checked = true;

                            // 
                            // create checked radio button rdoSetDefault 
                            // 
                            rdoSetDefault.AutoSize = true;
                            rdoSetDefault.Location = new System.Drawing.Point(lblJPLangLevel.Right + 285, top);
                            rdoSetDefault.Margin = new System.Windows.Forms.Padding(0);
                            rdoSetDefault.Size = new System.Drawing.Size(158, 31);
                            rdoSetDefault.TabIndex = 10;
                            rdoSetDefault.TabStop = true;
                            rdoSetDefault.Text = "Set as Default";
                            rdoSetDefault.UseVisualStyleBackColor = true;


                            if (Convert.ToInt16(dtr[4]) == 1)
                            {
                                rdoSetDefault.Checked = true; // checked
                            }
                            else
                            {
                                rdoSetDefault.Checked = false; // unchecked
                            }

                            top += 30;
                        }

                        // after insert jp certificate, clear textbox
                        txtJPLevelName.Clear();
                        txtJPBandScore.Clear();
                        dtpJPYear.Value = DateTime.Now;
                        txtJPLevelName.Focus();

                        count--;
                    }

                    // auto size adjustment everytime when the certificate is added

                    // panel1 holds incoming certificate infos and show in gbEducation
                    addJPSkillPanel.Top = jpCertificatePanel.Bottom + 10;

                    certificatePanel.Top = addJPSkillPanel.Bottom + 10;

                    addCertificatePanel.Top = certificatePanel.Bottom + 10;

                    gbEducation.AutoSize = true;
                    gbEducation.AutoSizeMode = AutoSizeMode.GrowOnly;

                    gbWorkExperience.Top = gbEducation.Bottom + 10;

                    btnEdit.Top = gbWorkExperience.Bottom + 10;

                    containerPanel.AutoSize = true;
                    containerPanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                    this.AutoSize = true;
                    this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                }
            }
        } // end of add jp certificate btn


        // to count add certificate btn cick
        int add_Certi_Click_Count = 0;
        private void btnAddCertificate_Click(object sender, EventArgs e)
        {
            int countSameCertificate = 0;

            foreach (DataRow dr in dtCertificate.Rows)
            {
                if (txtCertificateName.Text.Trim().ToString().Equals(dr[0]))
                {
                    countSameCertificate++;
                }
            }
            // if not same certificate exits
            if (!(countSameCertificate > 0))
            {
                // if not (Error020 : certificate name must not be empty)
                if (!(string.IsNullOrEmpty(txtCertificateName.Text.Trim()) || txtCertificateName.Text.Equals("Certificate")))
                {
                    // if not (Error060 : certificate date must be less than or equal current date)
                    if (!(dtpCertificateDateFrom.Value >= dtpCertificateDateTo.Value))
                    {
                        if (!(dtpCertificateDateTo.Value > DateTime.Now))
                        {
                            add_Certi_Click_Count++;

                            for (int i = 0; i < add_Certi_Click_Count; i++)
                            {
                                // clear labels in certificate panel that shows data from database
                                certificatePanel.Controls.Clear();

                                // store new certificate info from textboxes to datatable 'dtCertificate'
                                dtCertificate.Rows.Add(txtCertificateName.Text.Trim().ToString(), Convert.ToDateTime(dtpCertificateDateFrom.Text).ToString("yyyy/MM"),
                                Convert.ToDateTime(dtpCertificateDateTo.Text).ToString("yyyy/MM"), "true");

                                // declared variable 'top' to align the position of certificate labels
                                int top = 10;

                                Label lblCertificate = new Label();

                                //
                                // create label 'lblCertificate'
                                //
                                lblCertificate.AutoSize = true;
                                lblCertificate.Location = new System.Drawing.Point(3, 10);
                                lblCertificate.Size = new System.Drawing.Size(238, 27);
                                lblCertificate.TabIndex = 5;
                                lblCertificate.Text = "Certification";

                                certificatePanel.Controls.Add(lblCertificate);

                                // create and display labels which included data from dataTable
                                foreach (DataRow dtr in dtCertificate.Rows)
                                {

                                    // MessageBox.Show(dr[0].ToString() + dr[1].ToString() + dr[2].ToString());

                                    Label lblCertificateName = new Label();
                                    Label lblCertificateDate = new Label();

                                    certificatePanel.Controls.Add(lblCertificateName);
                                    certificatePanel.Controls.Add(lblCertificateDate);

                                    //
                                    // create label lblCertificateName
                                    //
                                    lblCertificateName.AutoSize = true;
                                    lblCertificateName.Location = new System.Drawing.Point(lblCertificate.Right + 110, top);
                                    lblCertificateName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                                    lblCertificateName.Size = new System.Drawing.Size(80, 27);
                                    lblCertificateName.TabIndex = 7;
                                    lblCertificateName.Text = dtr[0].ToString();
                                    lblCertificateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                                    //
                                    // create label lblWorkExperienceDate
                                    //
                                    lblCertificateDate.AutoSize = true;
                                    lblCertificateDate.Location = new System.Drawing.Point(lblCertificate.Right + 260, top);
                                    lblCertificateDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                                    lblCertificateDate.Size = new System.Drawing.Size(80, 27);
                                    lblCertificateDate.TabIndex = 7;
                                    lblCertificateDate.Text = dtr[1].ToString() + " ~ " + dtr[2].ToString();
                                    lblCertificateDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                                    top += 30;
                                }
                            }

                            txtCertificateName.Clear();
                            txtCertificateName.Focus();

                            dtpCertificateDateFrom.Value = DateTime.Now;
                            dtpCertificateDateTo.Value = DateTime.Now;
                            // everytime user edits certificate infos , addCertificateBtnClick ++ ;
                            add_Certi_Click_Count--;

                            // auto size adjustment everytime when the certificate is added
                            // panel1 holds incoming certificate infos and show in gbEducation

                            certificatePanel.AutoSize = true;
                            certificatePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                            certificatePanel.Top = addJPSkillPanel.Bottom + 10;
                            addCertificatePanel.Top = certificatePanel.Bottom + 10;

                            gbEducation.AutoSize = true;
                            gbEducation.AutoSizeMode = AutoSizeMode.GrowOnly;

                            gbWorkExperience.Top = gbEducation.Bottom + 10;

                            btnEdit.Top = gbWorkExperience.Bottom + 10;

                            containerPanel.AutoSize = true;
                            containerPanel.AutoSizeMode = AutoSizeMode.GrowOnly;
                        }
                        else
                        {
                            MessageBox.Show("certificate date to must be less than or equal current date"); // Error060
                            dtpCertificateDateTo.Value = DateTime.Now;
                            dtpCertificateDateTo.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("certificate date from must be less than certificate date to"); // Error060
                        dtpCertificateDateFrom.Value = DateTime.Now;
                        dtpCertificateDateFrom.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Please fill certification"); // Error020
                    txtCertificateName.Focus();
                }
            }
            else
            {
                MessageBox.Show("Same Certificate Name already exits"); // Bonus Error Check :)
                txtCertificateName.Focus();
            }
        }

        // to count add experience button click

        int add_Exp_Click_Count = 0;
        private void btnAddExperience_Click(object sender, EventArgs e)
        {
            if (!(string.IsNullOrEmpty(txtWorkPosition.Text.Trim()) || txtWorkPosition.Text.Equals("Experience")))
            {
                if (!(Regex.IsMatch(txtWorkPosition.Text, "[0-9]")))
                {
                    // if not (Error031 : Please fill work experience)
                    if (!(string.IsNullOrEmpty(txtCompanyName.Text.Trim()) || txtCompanyName.Text.Equals("Company Name")))
                    {
                        // if not (Error033 : Please fill Company Name)
                        if (!(string.IsNullOrEmpty(txtCompanyLocation.Text.Trim()) || txtCompanyLocation.Text.Equals("Location")))
                        {
                            // if not (Err062 : Experience date from must be less than experience date to)
                            if (!(dtpExperienceDateFrom.Value >= dtpExperienceDateTo.Value))
                            {
                                // if not (Err064 : Experience date to must be less than or equal current date)
                                if (!(dtpExperienceDateTo.Value > DateTime.Now))
                                {
                                    add_Exp_Click_Count++;

                                    for (int i = 0; i < add_Exp_Click_Count; i++)
                                    {
                                        // clear labels in certificate panel that shows data from database
                                        workExperiencePanel.Controls.Clear();

                                        // store new work experience info from textboxes to datatable 'dtWorkExperience'
                                        dtWorkExperience.Rows.Add(txtWorkPosition.Text.Trim().ToString(), Convert.ToDateTime(dtpExperienceDateFrom.Text).ToString("yyyy/MM"),
                                        Convert.ToDateTime(dtpExperienceDateTo.Text).ToString("yyyy/MM"), txtCompanyName.Text.Trim().ToString(), txtCompanyLocation.Text.Trim().ToString(), "true");

                                        // declared variable 'top' to align the position of certificate labels
                                        int top = 10;

                                        Label lblWorkExperience = new Label();

                                        //
                                        // create label 'lblWorkExperience'
                                        //
                                        lblWorkExperience.AutoSize = true;
                                        lblWorkExperience.Location = new System.Drawing.Point(3, 10);
                                        lblWorkExperience.Size = new System.Drawing.Size(238, 27);
                                        lblWorkExperience.TabIndex = 5;
                                        lblWorkExperience.Text = "Work Experience";

                                        workExperiencePanel.Controls.Add(lblWorkExperience);

                                        // create and display labels which included data from dataTable
                                        foreach (DataRow dtr in dtWorkExperience.Rows)
                                        {
                                            Label lblWorkPosition = new Label();
                                            Label lblWorkExperienceDate = new Label();
                                            Label lblNameLocation = new Label();

                                            workExperiencePanel.Controls.Add(lblWorkPosition);
                                            workExperiencePanel.Controls.Add(lblWorkExperienceDate);
                                            workExperiencePanel.Controls.Add(lblNameLocation);

                                            //
                                            // create label lblWorkPosition
                                            //
                                            lblWorkPosition.AutoSize = true;
                                            lblWorkPosition.Location = new System.Drawing.Point(lblWorkExperience.Right + 80, top);
                                            lblWorkPosition.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                                            lblWorkPosition.Size = new System.Drawing.Size(80, 27);
                                            lblWorkPosition.TabIndex = 7;
                                            lblWorkPosition.Text = dtr[0].ToString();
                                            lblWorkPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                                            //
                                            // create label lblWorkExperienceDate
                                            //
                                            lblWorkExperienceDate.AutoSize = true;
                                            lblWorkExperienceDate.Location = new System.Drawing.Point(lblWorkExperience.Right + 250, top);
                                            lblWorkExperienceDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                                            lblWorkExperienceDate.Size = new System.Drawing.Size(80, 27);
                                            lblWorkExperienceDate.TabIndex = 7;
                                            lblWorkExperienceDate.Text = dtr[1].ToString() + " ~ " + dtr[2].ToString();
                                            lblWorkExperienceDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;


                                            // create label lblNameLocation
                                            //
                                            lblNameLocation.AutoSize = true;
                                            lblNameLocation.Location = new System.Drawing.Point(lblWorkExperience.Right + 420, top);
                                            lblNameLocation.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                                            lblNameLocation.Size = new System.Drawing.Size(80, 27);
                                            lblNameLocation.TabIndex = 7;
                                            lblNameLocation.Text = dtr[3].ToString() + " , " + dtr[4].ToString();
                                            lblNameLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                                            top += 30;
                                        }

                                        // after insert clear textboxes

                                        txtWorkPosition.Text = "Experience";
                                        txtCompanyLocation.Text = "Location";
                                        txtCompanyName.Text = "Company Name";
                                        dtpExperienceDateTo.Value = DateTime.Now;
                                        dtpExperienceDateFrom.Value = DateTime.Now;

                                        add_Exp_Click_Count--;
                                    }

                                    // auto size adjustment everytime when the certificate is added
                                    // panel1 holds incoming certificate infos and show in gbEducation


                                    workExperiencePanel.AutoSize = true;
                                    workExperiencePanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                                    addExperiencePanel.Top = workExperiencePanel.Bottom + 10;

                                    gbWorkExperience.Top = gbEducation.Bottom + 10;
                                    gbWorkExperience.AutoSize = true;
                                    gbWorkExperience.AutoSizeMode = AutoSizeMode.GrowOnly;

                                    btnEdit.Top = gbWorkExperience.Bottom + 10;

                                    containerPanel.AutoSize = true;
                                    containerPanel.AutoSizeMode = AutoSizeMode.GrowOnly;

                                }
                                else
                                {
                                    MessageBox.Show("Work experience date to must be less than or equal current date"); // Error064

                                    dtpExperienceDateTo.Value = DateTime.Now;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Work experience date from must be less than work experience date to"); // Error062

                                dtpExperienceDateFrom.Value = DateTime.Now;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please fill company location"); // Error
                            txtCompanyLocation.Clear();
                            txtCompanyLocation.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please fill company name"); // Error033
                        txtCompanyName.Clear();
                        txtCompanyName.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Experience name must not contains integer!");
                    txtWorkPosition.Clear();
                    txtWorkPosition.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please fill work experience"); // Error031
                txtWorkPosition.Clear();
                txtWorkPosition.Focus();
            }
        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            if (dtpDOB.Value > DateTime.Now)
            {
                MessageBox.Show("Date of birth must be less than current date!");
                dtpDOB.Value = DateTime.Now;
                return;
            }
        }
    }
}