﻿namespace AttendanceManagementSystem
{
    partial class EmployeeRegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pForAll = new System.Windows.Forms.Panel();
            this.gpBoxWorkExp = new System.Windows.Forms.GroupBox();
            this.pWorkExp = new System.Windows.Forms.Panel();
            this.lblWorkExp = new System.Windows.Forms.Label();
            this.pAddWorkExp = new System.Windows.Forms.Panel();
            this.lblWorkExpLocation = new System.Windows.Forms.Label();
            this.lblWorkExpCompanyName = new System.Windows.Forms.Label();
            this.txtWorkExpLocation = new System.Windows.Forms.TextBox();
            this.txtWorkExpCompanyName = new System.Windows.Forms.TextBox();
            this.btnAddExp = new System.Windows.Forms.Button();
            this.dtpWorkExpTo = new System.Windows.Forms.DateTimePicker();
            this.lblWorkExpTo = new System.Windows.Forms.Label();
            this.dtpWorkExpFrm = new System.Windows.Forms.DateTimePicker();
            this.lblWorkExpFrm = new System.Windows.Forms.Label();
            this.txtWorkExpName = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.gpBoxEduBackground = new System.Windows.Forms.GroupBox();
            this.pCertificationList = new System.Windows.Forms.Panel();
            this.lblCertificate = new System.Windows.Forms.Label();
            this.lblJpLevel = new System.Windows.Forms.Label();
            this.pAddCertification = new System.Windows.Forms.Panel();
            this.btnAddCertificate = new System.Windows.Forms.Button();
            this.dtpCertificateTo = new System.Windows.Forms.DateTimePicker();
            this.lblCertificateTo = new System.Windows.Forms.Label();
            this.dtpCertificateFrm = new System.Windows.Forms.DateTimePicker();
            this.lblCertificateFrm = new System.Windows.Forms.Label();
            this.txtCertificate = new System.Windows.Forms.TextBox();
            this.pAddEducation = new System.Windows.Forms.Panel();
            this.btnAddJpCertificate = new System.Windows.Forms.Button();
            this.dtpJpYear = new System.Windows.Forms.DateTimePicker();
            this.lblJpYear = new System.Windows.Forms.Label();
            this.txtJpScore = new System.Windows.Forms.TextBox();
            this.txtJpLevel = new System.Windows.Forms.TextBox();
            this.lblJpScore = new System.Windows.Forms.Label();
            this.pJpLevelList = new System.Windows.Forms.Panel();
            this.txtEducation = new System.Windows.Forms.TextBox();
            this.lblEducation = new System.Windows.Forms.Label();
            this.gbBoxContactInfo = new System.Windows.Forms.GroupBox();
            this.txtRelation = new System.Windows.Forms.TextBox();
            this.txtEmergencyContactNo = new System.Windows.Forms.TextBox();
            this.lblEmergencyContactNo = new System.Windows.Forms.Label();
            this.lblRelation = new System.Windows.Forms.Label();
            this.txtEmergencyContactName = new System.Windows.Forms.TextBox();
            this.lblEmergencyContactName = new System.Windows.Forms.Label();
            this.txtTemporaryAddr = new System.Windows.Forms.RichTextBox();
            this.lblTemporaryAddr = new System.Windows.Forms.Label();
            this.txtPermanentAddr = new System.Windows.Forms.RichTextBox();
            this.lblPermanentAddr = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.gbBoxPersonalInfo = new System.Windows.Forms.GroupBox();
            this.dtpDob = new System.Windows.Forms.DateTimePicker();
            this.btnUploadImg = new System.Windows.Forms.Button();
            this.imgBox = new System.Windows.Forms.PictureBox();
            this.rdoFemale = new System.Windows.Forms.RadioButton();
            this.rdoMale = new System.Windows.Forms.RadioButton();
            this.ddRole = new System.Windows.Forms.ComboBox();
            this.txtNrcNo = new System.Windows.Forms.TextBox();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.txtEmpId = new System.Windows.Forms.TextBox();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.lblNrcNo = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblEmpName = new System.Windows.Forms.Label();
            this.lblEmpId = new System.Windows.Forms.Label();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.StaffPanel.SuspendLayout();
            this.pForAll.SuspendLayout();
            this.gpBoxWorkExp.SuspendLayout();
            this.pWorkExp.SuspendLayout();
            this.pAddWorkExp.SuspendLayout();
            this.gpBoxEduBackground.SuspendLayout();
            this.pCertificationList.SuspendLayout();
            this.pAddCertification.SuspendLayout();
            this.pAddEducation.SuspendLayout();
            this.gbBoxContactInfo.SuspendLayout();
            this.gbBoxPersonalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // pForAll
            // 
            this.pForAll.Controls.Add(this.gpBoxWorkExp);
            this.pForAll.Controls.Add(this.btnRegister);
            this.pForAll.Controls.Add(this.gpBoxEduBackground);
            this.pForAll.Controls.Add(this.gbBoxContactInfo);
            this.pForAll.Controls.Add(this.gbBoxPersonalInfo);
            this.pForAll.Location = new System.Drawing.Point(3, 179);
            this.pForAll.Name = "pForAll";
            this.pForAll.Size = new System.Drawing.Size(1458, 1981);
            this.pForAll.TabIndex = 0;
            // 
            // gpBoxWorkExp
            // 
            this.gpBoxWorkExp.AutoSize = true;
            this.gpBoxWorkExp.Controls.Add(this.pWorkExp);
            this.gpBoxWorkExp.Controls.Add(this.pAddWorkExp);
            this.gpBoxWorkExp.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.gpBoxWorkExp.Location = new System.Drawing.Point(88, 1470);
            this.gpBoxWorkExp.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gpBoxWorkExp.Name = "gpBoxWorkExp";
            this.gpBoxWorkExp.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gpBoxWorkExp.Size = new System.Drawing.Size(1328, 262);
            this.gpBoxWorkExp.TabIndex = 57;
            this.gpBoxWorkExp.TabStop = false;
            this.gpBoxWorkExp.Text = "Work Experience";
            // 
            // pWorkExp
            // 
            this.pWorkExp.Controls.Add(this.lblWorkExp);
            this.pWorkExp.Font = new System.Drawing.Font("Calibri", 12F);
            this.pWorkExp.Location = new System.Drawing.Point(36, 30);
            this.pWorkExp.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.pWorkExp.Name = "pWorkExp";
            this.pWorkExp.Size = new System.Drawing.Size(1282, 66);
            this.pWorkExp.TabIndex = 49;
            // 
            // lblWorkExp
            // 
            this.lblWorkExp.AutoSize = true;
            this.lblWorkExp.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblWorkExp.Location = new System.Drawing.Point(4, 35);
            this.lblWorkExp.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblWorkExp.Name = "lblWorkExp";
            this.lblWorkExp.Size = new System.Drawing.Size(150, 24);
            this.lblWorkExp.TabIndex = 45;
            this.lblWorkExp.Text = "Work Experience";
            // 
            // pAddWorkExp
            // 
            this.pAddWorkExp.Controls.Add(this.lblWorkExpLocation);
            this.pAddWorkExp.Controls.Add(this.lblWorkExpCompanyName);
            this.pAddWorkExp.Controls.Add(this.txtWorkExpLocation);
            this.pAddWorkExp.Controls.Add(this.txtWorkExpCompanyName);
            this.pAddWorkExp.Controls.Add(this.btnAddExp);
            this.pAddWorkExp.Controls.Add(this.dtpWorkExpTo);
            this.pAddWorkExp.Controls.Add(this.lblWorkExpTo);
            this.pAddWorkExp.Controls.Add(this.dtpWorkExpFrm);
            this.pAddWorkExp.Controls.Add(this.lblWorkExpFrm);
            this.pAddWorkExp.Controls.Add(this.txtWorkExpName);
            this.pAddWorkExp.Location = new System.Drawing.Point(36, 102);
            this.pAddWorkExp.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.pAddWorkExp.Name = "pAddWorkExp";
            this.pAddWorkExp.Size = new System.Drawing.Size(1282, 129);
            this.pAddWorkExp.TabIndex = 50;
            // 
            // lblWorkExpLocation
            // 
            this.lblWorkExpLocation.AutoSize = true;
            this.lblWorkExpLocation.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblWorkExpLocation.Location = new System.Drawing.Point(560, 87);
            this.lblWorkExpLocation.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblWorkExpLocation.Name = "lblWorkExpLocation";
            this.lblWorkExpLocation.Size = new System.Drawing.Size(80, 24);
            this.lblWorkExpLocation.TabIndex = 58;
            this.lblWorkExpLocation.Text = "Location";
            // 
            // lblWorkExpCompanyName
            // 
            this.lblWorkExpCompanyName.AutoSize = true;
            this.lblWorkExpCompanyName.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblWorkExpCompanyName.Location = new System.Drawing.Point(4, 87);
            this.lblWorkExpCompanyName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblWorkExpCompanyName.Name = "lblWorkExpCompanyName";
            this.lblWorkExpCompanyName.Size = new System.Drawing.Size(143, 24);
            this.lblWorkExpCompanyName.TabIndex = 57;
            this.lblWorkExpCompanyName.Text = "Company Name";
            // 
            // txtWorkExpLocation
            // 
            this.txtWorkExpLocation.BackColor = System.Drawing.Color.White;
            this.txtWorkExpLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWorkExpLocation.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtWorkExpLocation.Location = new System.Drawing.Point(698, 85);
            this.txtWorkExpLocation.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtWorkExpLocation.Multiline = true;
            this.txtWorkExpLocation.Name = "txtWorkExpLocation";
            this.txtWorkExpLocation.Size = new System.Drawing.Size(266, 33);
            this.txtWorkExpLocation.TabIndex = 30;
            // 
            // txtWorkExpCompanyName
            // 
            this.txtWorkExpCompanyName.BackColor = System.Drawing.Color.White;
            this.txtWorkExpCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWorkExpCompanyName.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtWorkExpCompanyName.Location = new System.Drawing.Point(248, 85);
            this.txtWorkExpCompanyName.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtWorkExpCompanyName.Multiline = true;
            this.txtWorkExpCompanyName.Name = "txtWorkExpCompanyName";
            this.txtWorkExpCompanyName.Size = new System.Drawing.Size(266, 33);
            this.txtWorkExpCompanyName.TabIndex = 29;
            // 
            // btnAddExp
            // 
            this.btnAddExp.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddExp.FlatAppearance.BorderSize = 0;
            this.btnAddExp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddExp.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnAddExp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddExp.Location = new System.Drawing.Point(1095, 16);
            this.btnAddExp.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnAddExp.Name = "btnAddExp";
            this.btnAddExp.Size = new System.Drawing.Size(169, 47);
            this.btnAddExp.TabIndex = 28;
            this.btnAddExp.Text = "Add Experience";
            this.btnAddExp.UseVisualStyleBackColor = false;
            this.btnAddExp.Click += new System.EventHandler(this.btnAddExp_Click);
            // 
            // dtpWorkExpTo
            // 
            this.dtpWorkExpTo.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpWorkExpTo.Location = new System.Drawing.Point(936, 21);
            this.dtpWorkExpTo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.dtpWorkExpTo.Name = "dtpWorkExpTo";
            this.dtpWorkExpTo.Size = new System.Drawing.Size(131, 32);
            this.dtpWorkExpTo.TabIndex = 27;
            this.dtpWorkExpTo.Value = new System.DateTime(2024, 3, 18, 0, 0, 0, 0);
            // 
            // lblWorkExpTo
            // 
            this.lblWorkExpTo.AutoSize = true;
            this.lblWorkExpTo.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblWorkExpTo.Location = new System.Drawing.Point(858, 27);
            this.lblWorkExpTo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblWorkExpTo.Name = "lblWorkExpTo";
            this.lblWorkExpTo.Size = new System.Drawing.Size(29, 24);
            this.lblWorkExpTo.TabIndex = 52;
            this.lblWorkExpTo.Text = "To";
            // 
            // dtpWorkExpFrm
            // 
            this.dtpWorkExpFrm.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpWorkExpFrm.Location = new System.Drawing.Point(698, 21);
            this.dtpWorkExpFrm.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.dtpWorkExpFrm.Name = "dtpWorkExpFrm";
            this.dtpWorkExpFrm.Size = new System.Drawing.Size(131, 32);
            this.dtpWorkExpFrm.TabIndex = 26;
            this.dtpWorkExpFrm.Value = new System.DateTime(2024, 3, 6, 0, 0, 0, 0);
            // 
            // lblWorkExpFrm
            // 
            this.lblWorkExpFrm.AutoSize = true;
            this.lblWorkExpFrm.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblWorkExpFrm.Location = new System.Drawing.Point(560, 27);
            this.lblWorkExpFrm.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblWorkExpFrm.Name = "lblWorkExpFrm";
            this.lblWorkExpFrm.Size = new System.Drawing.Size(53, 24);
            this.lblWorkExpFrm.TabIndex = 50;
            this.lblWorkExpFrm.Text = "From";
            // 
            // txtWorkExpName
            // 
            this.txtWorkExpName.BackColor = System.Drawing.Color.White;
            this.txtWorkExpName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWorkExpName.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtWorkExpName.Location = new System.Drawing.Point(248, 25);
            this.txtWorkExpName.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtWorkExpName.Multiline = true;
            this.txtWorkExpName.Name = "txtWorkExpName";
            this.txtWorkExpName.Size = new System.Drawing.Size(266, 33);
            this.txtWorkExpName.TabIndex = 25;
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnRegister.FlatAppearance.BorderSize = 0;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnRegister.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRegister.Location = new System.Drawing.Point(658, 1863);
            this.btnRegister.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(169, 47);
            this.btnRegister.TabIndex = 32;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // gpBoxEduBackground
            // 
            this.gpBoxEduBackground.AutoSize = true;
            this.gpBoxEduBackground.Controls.Add(this.pCertificationList);
            this.gpBoxEduBackground.Controls.Add(this.lblJpLevel);
            this.gpBoxEduBackground.Controls.Add(this.pAddCertification);
            this.gpBoxEduBackground.Controls.Add(this.pAddEducation);
            this.gpBoxEduBackground.Controls.Add(this.pJpLevelList);
            this.gpBoxEduBackground.Controls.Add(this.txtEducation);
            this.gpBoxEduBackground.Controls.Add(this.lblEducation);
            this.gpBoxEduBackground.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold);
            this.gpBoxEduBackground.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gpBoxEduBackground.Location = new System.Drawing.Point(88, 1022);
            this.gpBoxEduBackground.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gpBoxEduBackground.Name = "gpBoxEduBackground";
            this.gpBoxEduBackground.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gpBoxEduBackground.Size = new System.Drawing.Size(1328, 419);
            this.gpBoxEduBackground.TabIndex = 9;
            this.gpBoxEduBackground.TabStop = false;
            this.gpBoxEduBackground.Text = "Education Background";
            // 
            // pCertificationList
            // 
            this.pCertificationList.AutoSize = true;
            this.pCertificationList.Controls.Add(this.lblCertificate);
            this.pCertificationList.Font = new System.Drawing.Font("Calibri", 12F);
            this.pCertificationList.Location = new System.Drawing.Point(36, 245);
            this.pCertificationList.Margin = new System.Windows.Forms.Padding(8);
            this.pCertificationList.Name = "pCertificationList";
            this.pCertificationList.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.pCertificationList.Size = new System.Drawing.Size(1282, 60);
            this.pCertificationList.TabIndex = 33;
            // 
            // lblCertificate
            // 
            this.lblCertificate.AutoSize = true;
            this.lblCertificate.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblCertificate.Location = new System.Drawing.Point(4, 10);
            this.lblCertificate.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblCertificate.Name = "lblCertificate";
            this.lblCertificate.Size = new System.Drawing.Size(111, 24);
            this.lblCertificate.TabIndex = 46;
            this.lblCertificate.Text = "Certification";
            // 
            // lblJpLevel
            // 
            this.lblJpLevel.AutoSize = true;
            this.lblJpLevel.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblJpLevel.Location = new System.Drawing.Point(40, 124);
            this.lblJpLevel.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblJpLevel.Name = "lblJpLevel";
            this.lblJpLevel.Size = new System.Drawing.Size(169, 48);
            this.lblJpLevel.TabIndex = 33;
            this.lblJpLevel.Text = "Japanese Language\r\nLevel";
            // 
            // pAddCertification
            // 
            this.pAddCertification.AutoSize = true;
            this.pAddCertification.Controls.Add(this.btnAddCertificate);
            this.pAddCertification.Controls.Add(this.dtpCertificateTo);
            this.pAddCertification.Controls.Add(this.lblCertificateTo);
            this.pAddCertification.Controls.Add(this.dtpCertificateFrm);
            this.pAddCertification.Controls.Add(this.lblCertificateFrm);
            this.pAddCertification.Controls.Add(this.txtCertificate);
            this.pAddCertification.Location = new System.Drawing.Point(36, 312);
            this.pAddCertification.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.pAddCertification.Name = "pAddCertification";
            this.pAddCertification.Size = new System.Drawing.Size(1282, 79);
            this.pAddCertification.TabIndex = 45;
            // 
            // btnAddCertificate
            // 
            this.btnAddCertificate.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddCertificate.FlatAppearance.BorderSize = 0;
            this.btnAddCertificate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCertificate.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnAddCertificate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCertificate.Location = new System.Drawing.Point(1100, 16);
            this.btnAddCertificate.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnAddCertificate.Name = "btnAddCertificate";
            this.btnAddCertificate.Size = new System.Drawing.Size(169, 47);
            this.btnAddCertificate.TabIndex = 24;
            this.btnAddCertificate.Text = "Add Certification";
            this.btnAddCertificate.UseVisualStyleBackColor = false;
            this.btnAddCertificate.Click += new System.EventHandler(this.btnAddCertificate_Click);
            // 
            // dtpCertificateTo
            // 
            this.dtpCertificateTo.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpCertificateTo.Location = new System.Drawing.Point(936, 25);
            this.dtpCertificateTo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.dtpCertificateTo.Name = "dtpCertificateTo";
            this.dtpCertificateTo.Size = new System.Drawing.Size(131, 32);
            this.dtpCertificateTo.TabIndex = 23;
            this.dtpCertificateTo.Value = new System.DateTime(2024, 3, 18, 0, 0, 0, 0);
            // 
            // lblCertificateTo
            // 
            this.lblCertificateTo.AutoSize = true;
            this.lblCertificateTo.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblCertificateTo.Location = new System.Drawing.Point(858, 31);
            this.lblCertificateTo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblCertificateTo.Name = "lblCertificateTo";
            this.lblCertificateTo.Size = new System.Drawing.Size(29, 24);
            this.lblCertificateTo.TabIndex = 47;
            this.lblCertificateTo.Text = "To";
            // 
            // dtpCertificateFrm
            // 
            this.dtpCertificateFrm.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpCertificateFrm.Location = new System.Drawing.Point(698, 25);
            this.dtpCertificateFrm.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.dtpCertificateFrm.Name = "dtpCertificateFrm";
            this.dtpCertificateFrm.Size = new System.Drawing.Size(131, 32);
            this.dtpCertificateFrm.TabIndex = 22;
            this.dtpCertificateFrm.Value = new System.DateTime(2024, 3, 6, 0, 0, 0, 0);
            // 
            // lblCertificateFrm
            // 
            this.lblCertificateFrm.AutoSize = true;
            this.lblCertificateFrm.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblCertificateFrm.Location = new System.Drawing.Point(560, 27);
            this.lblCertificateFrm.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblCertificateFrm.Name = "lblCertificateFrm";
            this.lblCertificateFrm.Size = new System.Drawing.Size(53, 24);
            this.lblCertificateFrm.TabIndex = 45;
            this.lblCertificateFrm.Text = "From";
            // 
            // txtCertificate
            // 
            this.txtCertificate.BackColor = System.Drawing.Color.White;
            this.txtCertificate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCertificate.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtCertificate.Location = new System.Drawing.Point(254, 25);
            this.txtCertificate.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtCertificate.Multiline = true;
            this.txtCertificate.Name = "txtCertificate";
            this.txtCertificate.Size = new System.Drawing.Size(266, 33);
            this.txtCertificate.TabIndex = 21;
            // 
            // pAddEducation
            // 
            this.pAddEducation.AutoSize = true;
            this.pAddEducation.Controls.Add(this.btnAddJpCertificate);
            this.pAddEducation.Controls.Add(this.dtpJpYear);
            this.pAddEducation.Controls.Add(this.lblJpYear);
            this.pAddEducation.Controls.Add(this.txtJpScore);
            this.pAddEducation.Controls.Add(this.txtJpLevel);
            this.pAddEducation.Controls.Add(this.lblJpScore);
            this.pAddEducation.Location = new System.Drawing.Point(36, 166);
            this.pAddEducation.Margin = new System.Windows.Forms.Padding(8);
            this.pAddEducation.Name = "pAddEducation";
            this.pAddEducation.Size = new System.Drawing.Size(1282, 72);
            this.pAddEducation.TabIndex = 38;
            // 
            // btnAddJpCertificate
            // 
            this.btnAddJpCertificate.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddJpCertificate.FlatAppearance.BorderSize = 0;
            this.btnAddJpCertificate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddJpCertificate.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnAddJpCertificate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddJpCertificate.Location = new System.Drawing.Point(1100, 11);
            this.btnAddJpCertificate.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnAddJpCertificate.Name = "btnAddJpCertificate";
            this.btnAddJpCertificate.Size = new System.Drawing.Size(169, 47);
            this.btnAddJpCertificate.TabIndex = 20;
            this.btnAddJpCertificate.Text = "Add Certification";
            this.btnAddJpCertificate.UseVisualStyleBackColor = false;
            this.btnAddJpCertificate.Click += new System.EventHandler(this.btnAddJpCertificate_Click);
            // 
            // dtpJpYear
            // 
            this.dtpJpYear.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpJpYear.Location = new System.Drawing.Point(936, 19);
            this.dtpJpYear.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.dtpJpYear.Name = "dtpJpYear";
            this.dtpJpYear.Size = new System.Drawing.Size(131, 32);
            this.dtpJpYear.TabIndex = 19;
            this.dtpJpYear.Value = new System.DateTime(2024, 3, 18, 0, 0, 0, 0);
            // 
            // lblJpYear
            // 
            this.lblJpYear.AutoSize = true;
            this.lblJpYear.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblJpYear.Location = new System.Drawing.Point(858, 25);
            this.lblJpYear.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblJpYear.Name = "lblJpYear";
            this.lblJpYear.Size = new System.Drawing.Size(46, 24);
            this.lblJpYear.TabIndex = 41;
            this.lblJpYear.Text = "Year";
            // 
            // txtJpScore
            // 
            this.txtJpScore.BackColor = System.Drawing.Color.White;
            this.txtJpScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJpScore.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtJpScore.Location = new System.Drawing.Point(698, 23);
            this.txtJpScore.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtJpScore.Multiline = true;
            this.txtJpScore.Name = "txtJpScore";
            this.txtJpScore.Size = new System.Drawing.Size(129, 33);
            this.txtJpScore.TabIndex = 18;
            // 
            // txtJpLevel
            // 
            this.txtJpLevel.BackColor = System.Drawing.Color.White;
            this.txtJpLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJpLevel.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtJpLevel.Location = new System.Drawing.Point(254, 23);
            this.txtJpLevel.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtJpLevel.Multiline = true;
            this.txtJpLevel.Name = "txtJpLevel";
            this.txtJpLevel.Size = new System.Drawing.Size(266, 33);
            this.txtJpLevel.TabIndex = 17;
            // 
            // lblJpScore
            // 
            this.lblJpScore.AutoSize = true;
            this.lblJpScore.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblJpScore.Location = new System.Drawing.Point(560, 25);
            this.lblJpScore.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblJpScore.Name = "lblJpScore";
            this.lblJpScore.Size = new System.Drawing.Size(106, 24);
            this.lblJpScore.TabIndex = 35;
            this.lblJpScore.Text = "Score/Band";
            // 
            // pJpLevelList
            // 
            this.pJpLevelList.AutoSize = true;
            this.pJpLevelList.Font = new System.Drawing.Font("Calibri", 12F);
            this.pJpLevelList.Location = new System.Drawing.Point(275, 103);
            this.pJpLevelList.Margin = new System.Windows.Forms.Padding(8);
            this.pJpLevelList.Name = "pJpLevelList";
            this.pJpLevelList.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.pJpLevelList.Size = new System.Drawing.Size(1043, 57);
            this.pJpLevelList.TabIndex = 32;
            // 
            // txtEducation
            // 
            this.txtEducation.BackColor = System.Drawing.Color.White;
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEducation.Location = new System.Drawing.Point(290, 45);
            this.txtEducation.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEducation.Multiline = true;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.Size = new System.Drawing.Size(266, 33);
            this.txtEducation.TabIndex = 16;
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEducation.Location = new System.Drawing.Point(43, 47);
            this.lblEducation.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(93, 24);
            this.lblEducation.TabIndex = 30;
            this.lblEducation.Text = "Education";
            // 
            // gbBoxContactInfo
            // 
            this.gbBoxContactInfo.Controls.Add(this.txtRelation);
            this.gbBoxContactInfo.Controls.Add(this.txtEmergencyContactNo);
            this.gbBoxContactInfo.Controls.Add(this.lblEmergencyContactNo);
            this.gbBoxContactInfo.Controls.Add(this.lblRelation);
            this.gbBoxContactInfo.Controls.Add(this.txtEmergencyContactName);
            this.gbBoxContactInfo.Controls.Add(this.lblEmergencyContactName);
            this.gbBoxContactInfo.Controls.Add(this.txtTemporaryAddr);
            this.gbBoxContactInfo.Controls.Add(this.lblTemporaryAddr);
            this.gbBoxContactInfo.Controls.Add(this.txtPermanentAddr);
            this.gbBoxContactInfo.Controls.Add(this.lblPermanentAddr);
            this.gbBoxContactInfo.Controls.Add(this.txtEmail);
            this.gbBoxContactInfo.Controls.Add(this.lblEmail);
            this.gbBoxContactInfo.Controls.Add(this.txtPhone);
            this.gbBoxContactInfo.Controls.Add(this.lblPhone);
            this.gbBoxContactInfo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.gbBoxContactInfo.Location = new System.Drawing.Point(88, 537);
            this.gbBoxContactInfo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gbBoxContactInfo.Name = "gbBoxContactInfo";
            this.gbBoxContactInfo.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gbBoxContactInfo.Size = new System.Drawing.Size(1328, 463);
            this.gbBoxContactInfo.TabIndex = 8;
            this.gbBoxContactInfo.TabStop = false;
            this.gbBoxContactInfo.Text = "Contact Information";
            // 
            // txtRelation
            // 
            this.txtRelation.BackColor = System.Drawing.Color.White;
            this.txtRelation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRelation.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtRelation.Location = new System.Drawing.Point(1119, 384);
            this.txtRelation.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtRelation.Multiline = true;
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.Size = new System.Drawing.Size(142, 35);
            this.txtRelation.TabIndex = 15;
            // 
            // txtEmergencyContactNo
            // 
            this.txtEmergencyContactNo.BackColor = System.Drawing.Color.White;
            this.txtEmergencyContactNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmergencyContactNo.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEmergencyContactNo.Location = new System.Drawing.Point(829, 385);
            this.txtEmergencyContactNo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEmergencyContactNo.Multiline = true;
            this.txtEmergencyContactNo.Name = "txtEmergencyContactNo";
            this.txtEmergencyContactNo.Size = new System.Drawing.Size(150, 34);
            this.txtEmergencyContactNo.TabIndex = 14;
            // 
            // lblEmergencyContactNo
            // 
            this.lblEmergencyContactNo.AutoSize = true;
            this.lblEmergencyContactNo.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEmergencyContactNo.Location = new System.Drawing.Point(593, 386);
            this.lblEmergencyContactNo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEmergencyContactNo.Name = "lblEmergencyContactNo";
            this.lblEmergencyContactNo.Size = new System.Drawing.Size(204, 24);
            this.lblEmergencyContactNo.TabIndex = 26;
            this.lblEmergencyContactNo.Text = "Emergency Contact No.";
            // 
            // lblRelation
            // 
            this.lblRelation.AutoSize = true;
            this.lblRelation.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblRelation.Location = new System.Drawing.Point(1016, 387);
            this.lblRelation.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblRelation.Name = "lblRelation";
            this.lblRelation.Size = new System.Drawing.Size(79, 24);
            this.lblRelation.TabIndex = 28;
            this.lblRelation.Text = "Relation";
            // 
            // txtEmergencyContactName
            // 
            this.txtEmergencyContactName.BackColor = System.Drawing.Color.White;
            this.txtEmergencyContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmergencyContactName.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEmergencyContactName.Location = new System.Drawing.Point(289, 384);
            this.txtEmergencyContactName.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEmergencyContactName.Multiline = true;
            this.txtEmergencyContactName.Name = "txtEmergencyContactName";
            this.txtEmergencyContactName.Size = new System.Drawing.Size(266, 33);
            this.txtEmergencyContactName.TabIndex = 13;
            // 
            // lblEmergencyContactName
            // 
            this.lblEmergencyContactName.AutoSize = true;
            this.lblEmergencyContactName.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEmergencyContactName.Location = new System.Drawing.Point(43, 387);
            this.lblEmergencyContactName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEmergencyContactName.Name = "lblEmergencyContactName";
            this.lblEmergencyContactName.Size = new System.Drawing.Size(170, 48);
            this.lblEmergencyContactName.TabIndex = 24;
            this.lblEmergencyContactName.Text = "Emergency Contact\r\nName";
            // 
            // txtTemporaryAddr
            // 
            this.txtTemporaryAddr.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtTemporaryAddr.Location = new System.Drawing.Point(289, 280);
            this.txtTemporaryAddr.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtTemporaryAddr.Name = "txtTemporaryAddr";
            this.txtTemporaryAddr.Size = new System.Drawing.Size(266, 67);
            this.txtTemporaryAddr.TabIndex = 12;
            this.txtTemporaryAddr.Text = "";
            // 
            // lblTemporaryAddr
            // 
            this.lblTemporaryAddr.AutoSize = true;
            this.lblTemporaryAddr.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblTemporaryAddr.Location = new System.Drawing.Point(43, 299);
            this.lblTemporaryAddr.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblTemporaryAddr.Name = "lblTemporaryAddr";
            this.lblTemporaryAddr.Size = new System.Drawing.Size(171, 24);
            this.lblTemporaryAddr.TabIndex = 22;
            this.lblTemporaryAddr.Text = "Temporary Address";
            // 
            // txtPermanentAddr
            // 
            this.txtPermanentAddr.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtPermanentAddr.Location = new System.Drawing.Point(289, 185);
            this.txtPermanentAddr.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtPermanentAddr.Name = "txtPermanentAddr";
            this.txtPermanentAddr.Size = new System.Drawing.Size(267, 67);
            this.txtPermanentAddr.TabIndex = 11;
            this.txtPermanentAddr.Text = "";
            // 
            // lblPermanentAddr
            // 
            this.lblPermanentAddr.AutoSize = true;
            this.lblPermanentAddr.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblPermanentAddr.Location = new System.Drawing.Point(43, 205);
            this.lblPermanentAddr.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblPermanentAddr.Name = "lblPermanentAddr";
            this.lblPermanentAddr.Size = new System.Drawing.Size(174, 24);
            this.lblPermanentAddr.TabIndex = 20;
            this.lblPermanentAddr.Text = "Permanent Address";
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.White;
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEmail.Location = new System.Drawing.Point(290, 123);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEmail.Multiline = true;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtEmail.Size = new System.Drawing.Size(266, 33);
            this.txtEmail.TabIndex = 10;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEmail.Location = new System.Drawing.Point(43, 125);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(56, 24);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email";
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.Color.White;
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtPhone.Location = new System.Drawing.Point(290, 61);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtPhone.Multiline = true;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(266, 33);
            this.txtPhone.TabIndex = 9;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblPhone.Location = new System.Drawing.Point(43, 63);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(63, 24);
            this.lblPhone.TabIndex = 0;
            this.lblPhone.Text = "Phone";
            // 
            // gbBoxPersonalInfo
            // 
            this.gbBoxPersonalInfo.BackColor = System.Drawing.Color.Transparent;
            this.gbBoxPersonalInfo.Controls.Add(this.dtpDob);
            this.gbBoxPersonalInfo.Controls.Add(this.btnUploadImg);
            this.gbBoxPersonalInfo.Controls.Add(this.imgBox);
            this.gbBoxPersonalInfo.Controls.Add(this.rdoFemale);
            this.gbBoxPersonalInfo.Controls.Add(this.rdoMale);
            this.gbBoxPersonalInfo.Controls.Add(this.ddRole);
            this.gbBoxPersonalInfo.Controls.Add(this.txtNrcNo);
            this.gbBoxPersonalInfo.Controls.Add(this.txtFatherName);
            this.gbBoxPersonalInfo.Controls.Add(this.txtEmpName);
            this.gbBoxPersonalInfo.Controls.Add(this.txtEmpId);
            this.gbBoxPersonalInfo.Controls.Add(this.lblFatherName);
            this.gbBoxPersonalInfo.Controls.Add(this.lblNrcNo);
            this.gbBoxPersonalInfo.Controls.Add(this.lblGender);
            this.gbBoxPersonalInfo.Controls.Add(this.lblDOB);
            this.gbBoxPersonalInfo.Controls.Add(this.lblRole);
            this.gbBoxPersonalInfo.Controls.Add(this.lblEmpName);
            this.gbBoxPersonalInfo.Controls.Add(this.lblEmpId);
            this.gbBoxPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbBoxPersonalInfo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.gbBoxPersonalInfo.Location = new System.Drawing.Point(88, 18);
            this.gbBoxPersonalInfo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gbBoxPersonalInfo.Name = "gbBoxPersonalInfo";
            this.gbBoxPersonalInfo.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.gbBoxPersonalInfo.Size = new System.Drawing.Size(1328, 493);
            this.gbBoxPersonalInfo.TabIndex = 6;
            this.gbBoxPersonalInfo.TabStop = false;
            this.gbBoxPersonalInfo.Text = "Personal Information";
            // 
            // dtpDob
            // 
            this.dtpDob.Font = new System.Drawing.Font("Calibri", 12F);
            this.dtpDob.Location = new System.Drawing.Point(290, 229);
            this.dtpDob.Name = "dtpDob";
            this.dtpDob.Size = new System.Drawing.Size(266, 32);
            this.dtpDob.TabIndex = 4;
            this.dtpDob.Value = new System.DateTime(2024, 3, 6, 11, 29, 29, 0);
            // 
            // btnUploadImg
            // 
            this.btnUploadImg.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnUploadImg.FlatAppearance.BorderSize = 0;
            this.btnUploadImg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUploadImg.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnUploadImg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUploadImg.Location = new System.Drawing.Point(628, 234);
            this.btnUploadImg.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnUploadImg.Name = "btnUploadImg";
            this.btnUploadImg.Size = new System.Drawing.Size(170, 47);
            this.btnUploadImg.TabIndex = 31;
            this.btnUploadImg.Text = "Upload";
            this.btnUploadImg.UseVisualStyleBackColor = false;
            this.btnUploadImg.Click += new System.EventHandler(this.btnUploadImg_Click);
            // 
            // imgBox
            // 
            this.imgBox.BackColor = System.Drawing.Color.White;
            this.imgBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgBox.Location = new System.Drawing.Point(619, 47);
            this.imgBox.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.imgBox.Name = "imgBox";
            this.imgBox.Size = new System.Drawing.Size(186, 177);
            this.imgBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgBox.TabIndex = 16;
            this.imgBox.TabStop = false;
            // 
            // rdoFemale
            // 
            this.rdoFemale.AutoSize = true;
            this.rdoFemale.Font = new System.Drawing.Font("Calibri", 12F);
            this.rdoFemale.Location = new System.Drawing.Point(448, 357);
            this.rdoFemale.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdoFemale.Name = "rdoFemale";
            this.rdoFemale.Size = new System.Drawing.Size(111, 28);
            this.rdoFemale.TabIndex = 7;
            this.rdoFemale.TabStop = true;
            this.rdoFemale.Text = "    Female";
            this.rdoFemale.UseVisualStyleBackColor = true;
            // 
            // rdoMale
            // 
            this.rdoMale.AutoSize = true;
            this.rdoMale.Font = new System.Drawing.Font("Calibri", 12F);
            this.rdoMale.Location = new System.Drawing.Point(290, 357);
            this.rdoMale.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdoMale.Name = "rdoMale";
            this.rdoMale.Size = new System.Drawing.Size(93, 28);
            this.rdoMale.TabIndex = 6;
            this.rdoMale.TabStop = true;
            this.rdoMale.Text = "    Male";
            this.rdoMale.UseVisualStyleBackColor = true;
            // 
            // ddRole
            // 
            this.ddRole.BackColor = System.Drawing.Color.White;
            this.ddRole.Font = new System.Drawing.Font("Calibri", 12F);
            this.ddRole.FormattingEnabled = true;
            this.ddRole.ItemHeight = 24;
            this.ddRole.Items.AddRange(new object[] {
            "Administrator",
            "Staff"});
            this.ddRole.Location = new System.Drawing.Point(289, 171);
            this.ddRole.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ddRole.Name = "ddRole";
            this.ddRole.Size = new System.Drawing.Size(267, 32);
            this.ddRole.TabIndex = 3;
            // 
            // txtNrcNo
            // 
            this.txtNrcNo.BackColor = System.Drawing.Color.White;
            this.txtNrcNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNrcNo.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtNrcNo.Location = new System.Drawing.Point(290, 293);
            this.txtNrcNo.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtNrcNo.Multiline = true;
            this.txtNrcNo.Name = "txtNrcNo";
            this.txtNrcNo.Size = new System.Drawing.Size(266, 33);
            this.txtNrcNo.TabIndex = 5;
            // 
            // txtFatherName
            // 
            this.txtFatherName.BackColor = System.Drawing.Color.White;
            this.txtFatherName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFatherName.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtFatherName.Location = new System.Drawing.Point(290, 421);
            this.txtFatherName.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtFatherName.Multiline = true;
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(266, 33);
            this.txtFatherName.TabIndex = 8;
            // 
            // txtEmpName
            // 
            this.txtEmpName.BackColor = System.Drawing.Color.White;
            this.txtEmpName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmpName.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEmpName.Location = new System.Drawing.Point(289, 107);
            this.txtEmpName.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEmpName.Multiline = true;
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(266, 33);
            this.txtEmpName.TabIndex = 2;
            // 
            // txtEmpId
            // 
            this.txtEmpId.BackColor = System.Drawing.Color.White;
            this.txtEmpId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmpId.Font = new System.Drawing.Font("Calibri", 12F);
            this.txtEmpId.Location = new System.Drawing.Point(289, 47);
            this.txtEmpId.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.txtEmpId.Multiline = true;
            this.txtEmpId.Name = "txtEmpId";
            this.txtEmpId.Size = new System.Drawing.Size(266, 33);
            this.txtEmpId.TabIndex = 1;
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblFatherName.Location = new System.Drawing.Point(43, 423);
            this.lblFatherName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(117, 24);
            this.lblFatherName.TabIndex = 7;
            this.lblFatherName.Text = "Father Name";
            // 
            // lblNrcNo
            // 
            this.lblNrcNo.AutoSize = true;
            this.lblNrcNo.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblNrcNo.Location = new System.Drawing.Point(43, 295);
            this.lblNrcNo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblNrcNo.Name = "lblNrcNo";
            this.lblNrcNo.Size = new System.Drawing.Size(79, 24);
            this.lblNrcNo.TabIndex = 6;
            this.lblNrcNo.Text = "NRC No.";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblGender.Location = new System.Drawing.Point(43, 359);
            this.lblGender.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(72, 24);
            this.lblGender.TabIndex = 5;
            this.lblGender.Text = "Gender";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblDOB.Location = new System.Drawing.Point(43, 235);
            this.lblDOB.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(117, 24);
            this.lblDOB.TabIndex = 4;
            this.lblDOB.Text = "Date of birth";
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblRole.Location = new System.Drawing.Point(43, 174);
            this.lblRole.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(47, 24);
            this.lblRole.TabIndex = 3;
            this.lblRole.Text = "Role";
            // 
            // lblEmpName
            // 
            this.lblEmpName.AutoSize = true;
            this.lblEmpName.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEmpName.Location = new System.Drawing.Point(43, 109);
            this.lblEmpName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEmpName.Name = "lblEmpName";
            this.lblEmpName.Size = new System.Drawing.Size(59, 24);
            this.lblEmpName.TabIndex = 2;
            this.lblEmpName.Text = "Name";
            // 
            // lblEmpId
            // 
            this.lblEmpId.AutoSize = true;
            this.lblEmpId.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblEmpId.Location = new System.Drawing.Point(43, 49);
            this.lblEmpId.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblEmpId.Name = "lblEmpId";
            this.lblEmpId.Size = new System.Drawing.Size(114, 24);
            this.lblEmpId.TabIndex = 1;
            this.lblEmpId.Text = "Employee ID";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // EmployeeRegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1607, 1055);
            this.Controls.Add(this.pForAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.MinimizeBox = false;
            this.Name = "EmployeeRegistrationForm";
            this.Text = "Employee Management Form";
            this.Load += new System.EventHandler(this.EmployeeRegistrationForm_Load);
            this.Controls.SetChildIndex(this.StaffPanel, 0);
            this.Controls.SetChildIndex(this.pForAll, 0);
            this.StaffPanel.ResumeLayout(false);
            this.StaffPanel.PerformLayout();
            this.pForAll.ResumeLayout(false);
            this.pForAll.PerformLayout();
            this.gpBoxWorkExp.ResumeLayout(false);
            this.pWorkExp.ResumeLayout(false);
            this.pWorkExp.PerformLayout();
            this.pAddWorkExp.ResumeLayout(false);
            this.pAddWorkExp.PerformLayout();
            this.gpBoxEduBackground.ResumeLayout(false);
            this.gpBoxEduBackground.PerformLayout();
            this.pCertificationList.ResumeLayout(false);
            this.pCertificationList.PerformLayout();
            this.pAddCertification.ResumeLayout(false);
            this.pAddCertification.PerformLayout();
            this.pAddEducation.ResumeLayout(false);
            this.pAddEducation.PerformLayout();
            this.gbBoxContactInfo.ResumeLayout(false);
            this.gbBoxContactInfo.PerformLayout();
            this.gbBoxPersonalInfo.ResumeLayout(false);
            this.gbBoxPersonalInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pForAll;
        private System.Windows.Forms.GroupBox gbBoxContactInfo;
        private System.Windows.Forms.RichTextBox txtTemporaryAddr;
        private System.Windows.Forms.Label lblTemporaryAddr;
        private System.Windows.Forms.RichTextBox txtPermanentAddr;
        private System.Windows.Forms.Label lblPermanentAddr;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhone;
 //       private System.Windows.Forms.Panel panelStaffButton;
 //       private System.Windows.Forms.Button button4;
 //       private System.Windows.Forms.Button button2;
 //       private System.Windows.Forms.Button button3;
 //       private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox gbBoxPersonalInfo;
        private System.Windows.Forms.Button btnUploadImg;
        private System.Windows.Forms.PictureBox imgBox;
        private System.Windows.Forms.RadioButton rdoFemale;
        private System.Windows.Forms.RadioButton rdoMale;
        private System.Windows.Forms.ComboBox ddRole;
        private System.Windows.Forms.TextBox txtNrcNo;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.TextBox txtEmpId;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.Label lblNrcNo;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblEmpName;
        private System.Windows.Forms.Label lblEmpId;
 //       private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmergencyContactName;
        private System.Windows.Forms.Label lblEmergencyContactName;
        private System.Windows.Forms.TextBox txtEmergencyContactNo;
        private System.Windows.Forms.Label lblEmergencyContactNo;
        private System.Windows.Forms.TextBox txtRelation;
        private System.Windows.Forms.Label lblRelation;
        private System.Windows.Forms.GroupBox gpBoxEduBackground;
        private System.Windows.Forms.TextBox txtEducation;
        private System.Windows.Forms.Label lblEducation;
        private System.Windows.Forms.Panel pJpLevelList;
        private System.Windows.Forms.Panel pAddEducation;
        private System.Windows.Forms.Label lblJpScore;
        private System.Windows.Forms.TextBox txtJpLevel;
        private System.Windows.Forms.Label lblJpYear;
        private System.Windows.Forms.TextBox txtJpScore;
        private System.Windows.Forms.Button btnAddJpCertificate;
        private System.Windows.Forms.DateTimePicker dtpJpYear;
        private System.Windows.Forms.Panel pAddCertification;
        private System.Windows.Forms.TextBox txtCertificate;
        private System.Windows.Forms.Button btnAddCertificate;
        private System.Windows.Forms.DateTimePicker dtpCertificateTo;
        private System.Windows.Forms.Label lblCertificateTo;
        private System.Windows.Forms.DateTimePicker dtpCertificateFrm;
        private System.Windows.Forms.Label lblCertificateFrm;
        private System.Windows.Forms.Button btnRegister;
 //       private System.Windows.Forms.Button button7;
        //private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DateTimePicker dtpDob;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Label lblJpLevel;
        private System.Windows.Forms.Panel pCertificationList;
        private System.Windows.Forms.GroupBox gpBoxWorkExp;
        private System.Windows.Forms.Panel pWorkExp;
        private System.Windows.Forms.Label lblWorkExp;
        private System.Windows.Forms.Panel pAddWorkExp;
        private System.Windows.Forms.Label lblWorkExpLocation;
        private System.Windows.Forms.Label lblWorkExpCompanyName;
        private System.Windows.Forms.TextBox txtWorkExpLocation;
        private System.Windows.Forms.TextBox txtWorkExpCompanyName;
        private System.Windows.Forms.Button btnAddExp;
        private System.Windows.Forms.DateTimePicker dtpWorkExpTo;
        private System.Windows.Forms.Label lblWorkExpTo;
        private System.Windows.Forms.DateTimePicker dtpWorkExpFrm;
        private System.Windows.Forms.Label lblWorkExpFrm;
        private System.Windows.Forms.TextBox txtWorkExpName;
        private System.Windows.Forms.Label lblCertificate;
    }
}

